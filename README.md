# Jordio Backend Server
Jordio is an application that handles order processing on Bol.com.
It can automatically retrieve orders from the bol.com API and create invoices, packing-slips and shipping labels.
  
Currently it only supports shipping with PostNL.

## Installation

1. Run `composer install`
2. Rename `.env.example` to `.env` and check for any information that needs to be changed depending on installation.  
   For example: set the BOL_DEMO_MODE and POSTNL_SANDBOX to false if you're installing this on a production server.
3. Migrate the database: `php artisan migrate`
4. Check if everything is working as intended: `php artisan orders:sync`
5. Create a recurring task on the server that runs our scheduler every minute.  
   ```bash
   cd /volume1/web/backend.nl.jordio && php artisan schedule:run >> /dev/null 2>&1
   ```
   
Everything should now be up and running.


## Contact
Send me an email on: [jordiholleman@gmail.com](jordiholleman@gmail.com)
