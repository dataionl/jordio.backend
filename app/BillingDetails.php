<?php

namespace App;

use App\Events\ShipmentDetailsUpdated;
use App\Events\BillingDetailsUpdated;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BillingDetails extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected $dispatchesEvents = [
        'updated' => BillingDetailsUpdated::class,
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'orderId');
    }

    public function invoiceFields()
    {
        return ['salutation', 'firstName', 'surName', 'streetName',
            'houseNumber', 'houseNumberExtension', 'extraAddressInformation',
            'zipCode', 'city', 'countryCode', 'company',
            'vatNumber', 'kvkNumber', 'orderReference'];
    }

    public function getSpreadsheetHeaders()
    {
        $headers = [
            'invoice_salutation',
            'invoice_firstName',
            'invoice_surName',
            'invoice_company',
            'invoice_streetName',
            'invoice_houseNumber',
            'invoice_houseNumberExtension',
            'invoice_extraAddressInformation',
            'invoice_zipCode',
            'invoice_city',
            'invoice_countryCode',
            'invoice_vatNumber',
            'invoice_kvkNumber',
            'invoice_orderReference',
            'invoice_email'
        ];
        return $headers;
    }
}
