<?php

namespace App;

use App\Events\BolShipmentCreated;
use App\Events\BolShipmentUpdated;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BolShipment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function order()
    {
        return $this->hasOne(Order::class, 'orderId', 'orderId');
    }

    public function orderItem()
    {
        return $this->hasOne(OrderItem::class, 'orderItemId', 'orderItemId');
    }

    public function shipment()
    {
        return $this->belongsTo(Shipment::class, 'shipment_id', 'id');
    }

    protected $dispatchesEvents = [
        'created' => BolShipmentCreated::class,
        'updated' => BolShipmentUpdated::class,
    ];
}
