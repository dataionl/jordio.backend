<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BolShippingMethod extends Model
{
    use HasFactory;

    public $primaryKey = 'transporter_code';
    public $incrementing = false;
    protected $keyType = "string";
    protected $casts = [
        'transporter_code' => 'string',
        'track_and_trace' => 'boolean',
        'default' => 'boolean',
    ];
    public static $snakeAttributes = false;

    protected $guarded = [];
}
