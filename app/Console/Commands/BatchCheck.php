<?php

namespace App\Console\Commands;

use App\OrderBatch;
use App\Utils\TaskUtils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class BatchCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batch:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if batches being processed are completed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $batch = OrderBatch::where('status', 'PROCESSING')->get();
        if($batch->count() > 0) {
            Artisan::call('queue:work --queue=batch_check --max-time=58');
        }
    }
}
