<?php

namespace App\Console\Commands;

use App\Http\BolRetailerApi\BolClient;
use App\OrderItem;
use App\Utils\OrderUtils;
use App\Utils\TaskUtils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Picqer\BolRetailerV8\Exception\ConnectException;
use Picqer\BolRetailerV8\Exception\Exception;
use Picqer\BolRetailerV8\Exception\RateLimitException;
use Picqer\BolRetailerV8\Exception\ResponseException;
use Picqer\BolRetailerV8\Exception\UnauthorizedException;
use Picqer\BolRetailerV8\Model\OrderItemCancellation;

class CancelOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel orders with cancellation request';

    /**
     * @var BolClient
     */
    private $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new BolClient();
        $this->client->setDemoMode(env('BOL_API_DEMO_MODE'));
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = Carbon::now();
        $reducedOrders = $this->getReducedOrders();
        $bolOrders = $this->getOrdersDetails($reducedOrders);
        $cancelledOrderItems = $this->handleCancellationRequests($bolOrders);

        $duration = $now->diffAsCarbonInterval(Carbon::now());
        $this->info("Cancellations duration: " . $duration . ". OrderItems cancelled: " . count($cancelledOrderItems));
    }

    private function getReducedOrders(): array
    {
        $reducedOrders = [];
        $page = 1;
        $continue = true;
        while ($continue) {
            try {
                $orders = $this->client->getOrders($page);
            } catch (ConnectException | RateLimitException | ResponseException | UnauthorizedException | Exception $e) {
                $this->error($e->getMessage());
            }
            if (!empty($orders)) {
                $reducedOrders = array_merge($reducedOrders, $orders);
            }
            $page++;
            if (empty($orders) || $this->client->isDemoMode()) {
                $continue = false;
            }
        }

        return $reducedOrders;
    }

    private function getOrdersDetails(array $reducedOrders): array
    {
        $orders = [];
        foreach ($reducedOrders as $o) {
            try {
                $order = $this->client->getOrder($o->orderId);
            } catch (ConnectException | RateLimitException | ResponseException | UnauthorizedException | Exception $e) {
                $this->error($e->getMessage());
            }
            //check if the order is found
            if (!empty($order)) {
                $orders[] = $order;
            }
        }

        return $orders;
    }

    private function handleCancellationRequests(array $bolOrders)
    {
        $cancelledOrderItems = [];
        foreach ($bolOrders as $bolOrder) {
            foreach ($bolOrder->orderItems as $bolOrderItem) {
                if ($bolOrderItem->cancellationRequest) {
                    $orderItem = OrderItem::find($bolOrderItem->orderItemId);
                    if ($orderItem) {
                        $cancellation = OrderItemCancellation::constructFromArray([
                            'orderItemId' => $bolOrderItem->orderItemId,
                            'reasonCode' => "REQUESTED_BY_CUSTOMER"
                        ]);
                        try {
                            $processStatus = $this->client->cancelOrderItem([$cancellation]);

                            $cancelledOrderItems[] = $orderItem;
                            $orderItem->delete();
                            $this->line("OrderItem cancelled: " . $orderItem->orderItemId);
                        } catch (ConnectException | RateLimitException | ResponseException | UnauthorizedException | Exception $e) {
                            $this->error($e->getMessage());
                        }
                    } else {
                        $this->line("OrderItem with id " . $bolOrderItem->orderItemId . " was not found in table order_items.");
                    }
                }
            }
        }

        return $cancelledOrderItems;
    }
}
