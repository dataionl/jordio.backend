<?php

namespace App\Console\Commands;

use App\Http\BolRetailerApi\BolClient;
use App\Jobs\GetOfferExport;
use App\Jobs\UpdateOfferFulfilment;
use App\Offer;
use App\Utils\TaskUtils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Picqer\BolRetailerV8\Exception\ConnectException;
use Picqer\BolRetailerV8\Exception\Exception;
use Picqer\BolRetailerV8\Exception\RateLimitException;
use Picqer\BolRetailerV8\Exception\ResponseException;
use Picqer\BolRetailerV8\Exception\UnauthorizedException;

class OffersFulfilmentWeekend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offers:fulfilment:weekend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set 24uurs-17 offer fulfiment to 1-2d';

    /**
     * @var BolClient
     */
    private $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new BolClient();
        $this->client->setDemoMode(env('BOL_API_DEMO_MODE'));
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $offers = Offer::all();
        $offers->each(function(Offer $offer) {
            UpdateOfferFulfilment::dispatch($offer, '24uurs-17', '1-2d');
        });
    }

}
