<?php

namespace App\Console\Commands;

use App\Http\BolRetailerApi\BolClient;
use App\Jobs\GetOfferExport;
use App\Utils\TaskUtils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Picqer\BolRetailerV8\Exception\ConnectException;
use Picqer\BolRetailerV8\Exception\Exception;
use Picqer\BolRetailerV8\Exception\RateLimitException;
use Picqer\BolRetailerV8\Exception\ResponseException;
use Picqer\BolRetailerV8\Exception\UnauthorizedException;

class OffersSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offers:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync offers from Bol.com API';

    /**
     * @var BolClient
     */
    private $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new BolClient();
        $this->client->setDemoMode(env('BOL_API_DEMO_MODE'));
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $processStatus = $this->client->postOfferExport('CSV');
            GetOfferExport::dispatch($processStatus);
        } catch (ConnectException | RateLimitException | ResponseException | UnauthorizedException | Exception $e) {
            $this->error($e->getMessage());
        }
    }

}
