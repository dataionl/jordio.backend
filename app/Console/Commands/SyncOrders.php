<?php

namespace App\Console\Commands;

use App\Utils\TaskUtils;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SyncOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync orders from Bol.com API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = Carbon::now();
        $taskUtils = new TaskUtils();
        if(env('BOL_API_DEMO_MODE')) {
            $orders = $taskUtils->syncOrders();
        } else {
            $orders = $taskUtils->syncOrders(false, "ALL");
        }

        $orders->each(function($order){
            $this->line("Order created: ".$order->orderId);
        });
        $duration = $now->diffAsCarbonInterval(Carbon::now());
        $this->info("Sync duration: ".$duration.". Orders created: ".$orders->count());
    }
}
