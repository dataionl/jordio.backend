<?php

namespace App\Console;

use App\Http\Controllers\OrderController;
use App\Utils\TaskUtils;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('orders:sync')
            ->everyThreeMinutes();

        $schedule->command('queue:work --queue=bol_shipments --stop-when-empty')
            ->everyMinute()->withoutOverlapping()->runInBackground();
        $schedule->command('batch:check')
            ->everyMinute()->withoutOverlapping()->runInBackground();

        $schedule->command('orders:cancel')
            ->everyFifteenMinutes()->withoutOverlapping()->runInBackground();

        $schedule->command('queue:work --queue=emails --max-jobs=10 --stop-when-empty')
            ->everyMinute()->withoutOverlapping()->runInBackground();
        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
