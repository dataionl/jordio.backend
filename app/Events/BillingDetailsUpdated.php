<?php

namespace App\Events;

use App\BillingDetails;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BillingDetailsUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $billingDetails;
    public $order;
    public $orderQuery;

    /**
     * Create a new event instance.
     *
     * @param BillingDetails $billingDetails
     */
    public function __construct(BillingDetails $billingDetails)
    {
        $this->billingDetails = $billingDetails;
        $this->order = $billingDetails->order();
        $this->orderQuery = $billingDetails->order();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
