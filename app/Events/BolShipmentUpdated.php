<?php

namespace App\Events;

use App\BolShipment;
use App\Order;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BolShipmentUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $bolShipment;

    /**
     * Create a new event instance.
     *
     * @param BolShipment $bolShipment
     */
    public function __construct(BolShipment $bolShipment)
    {
        $this->bolShipment = $bolShipment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
