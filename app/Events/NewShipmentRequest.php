<?php

namespace App\Events;

use App\Order;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewShipmentRequest
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $order;
    public $orderQuery;
    public $shipment_product_code;

    /**
     * Create a new event instance.
     *
     * @param Order $order
     * @param null $shipment_product_code
     */
    public function __construct(Order $order, $shipment_product_code = null)
    {
        $this->order = $order;
        $this->orderQuery = $order->shipmentDetails->order(); //because FileUtils expects a Builder
        $this->shipment_product_code = $shipment_product_code;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
