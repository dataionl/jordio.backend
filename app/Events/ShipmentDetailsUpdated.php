<?php

namespace App\Events;

use App\Order;
use App\ShipmentDetails;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ShipmentDetailsUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $shipmentDetails;
    public $order;
    public $orderQuery;

    /**
     * Create a new event instance.
     *
     * @param ShipmentDetails $shipmentDetails
     */
    public function __construct(ShipmentDetails $shipmentDetails)
    {
        $this->shipmentDetails = $shipmentDetails;
        $this->order = $shipmentDetails->order;
        $this->orderQuery = $shipmentDetails->order();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
