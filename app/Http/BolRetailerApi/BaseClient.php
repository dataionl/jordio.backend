<?php


namespace App\Http\BolRetailerApi;


use App\Token;
use GuzzleHttp\Exception\BadResponseException;
use Picqer\BolRetailerV8\Client;
use Picqer\BolRetailerV8\Exception\ConnectException;
use Picqer\BolRetailerV8\Exception\Exception;
use Picqer\BolRetailerV8\Exception\RateLimitException;
use Picqer\BolRetailerV8\Exception\ResponseException;
use Picqer\BolRetailerV8\Exception\ServerException;
use Picqer\BolRetailerV8\Exception\UnauthorizedException;
use Picqer\BolRetailerV8\Model\AbstractModel;
use Psr\Http\Message\ResponseInterface;

class BaseClient extends Client
{
    protected const API_CONTENT_TYPE_JSON = 'application/vnd.retailer.v8+json';

    public $rateLimits = [
        'limit' => null,
        'remaining' => null,
        'reset' => null,
    ];

    protected $maxRetries = 10;
    protected $retries = 0;

    /**
     * @param string $method HTTP Method
     * @param string $url Url
     * @param array $options Request options to apply
     * @param array $responseTypes Expected response type per HTTP status code
     * @return AbstractModel|string|null Model or array representing response
     * @throws ConnectException when an error occurred in the HTTP connection.
     * @throws UnauthorizedException when the request was unauthorized.
     * @throws ResponseException when no suitable responseType could be applied.
     * @throws RateLimitException when the throttling limit has been reached for the API user.
     * @throws Exception when something unexpected went wrong.
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(string $method, string $url, array $options, array $responseTypes)
    {
        if (!$this->isAuthenticated()) {
            $this->authenticate(env('BOL_CLIENT_ID'), env('BOL_CLIENT_SECRET'));
//            throw new UnauthorizedException('No or expired token, please authenticate first');
        }

        $url = $this->getEndpoint($url);

        $httpOptions = [];
        $httpOptions['headers'] = [
            'Accept' => $options['produces'] ?? static::API_CONTENT_TYPE_JSON,
            'Authorization' => sprintf('Bearer %s', $this->token['access_token']),
        ];

        // encode the body if a model is supplied for it
        if (isset($options['body']) && $options['body'] instanceof AbstractModel) {
            $httpOptions['headers']['Content-Type'] = static::API_CONTENT_TYPE_JSON;
            $httpOptions['body'] = json_encode($options['body']->toArray(true));
        }

        // pass through query parameters without null values
        if (isset($options['query'])) {
            $httpOptions['query'] = array_filter($options['query'], function ($value) {
                return $value !== null;
            });
        }

        $response = $this->rawRequest($method, $url, $httpOptions);
        $statusCode = $response->getStatusCode();

        if (!array_key_exists($statusCode, $responseTypes)) {
            throw new ResponseException("No model specified for '{$url}' with status '{$statusCode}'");
        }

        $responseType = $responseTypes[$statusCode];

        // return null if responseType is null (e.g. 404)
        if ($responseType === 'null') {
            return null;
        }

        // return raw body if response type is string
        if ($responseType == 'string') {
            return (string)$response->getBody();
        }

        // create new instance of model and fill it with the response data
        $data = $this->jsonDecodeBody($response);
        return $responseType::constructFromArray($data);
    }

    /**
     * @return bool
     * @throws ConnectException
     * @throws Exception
     * @throws RateLimitException
     * @throws ResponseException
     * @throws ServerException
     * @throws UnauthorizedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function isAuthenticated(): bool
    {
        $token = Token::latest()->first();
        if (!$token || $token->expires_at < time()) {
            $this->authenticate(env('BOL_CLIENT_ID'), env('BOL_CLIENT_SECRET'));
            $token = $this->token;
        } else {
            $token = json_decode($token, true);
            if (json_last_error() != JSON_ERROR_NONE) {
                throw new ResponseException('Body contains invalid JSON');
            }
        }
        $this->validateToken($token);
        $this->token = $token;
        if (!$token || !is_array($this->token)) {
            return false;
        }
        return $this->token['expires_at'] > time();
    }

    /**
     * @param string $clientId
     * @param string $clientSecret
     * @return bool
     * @throws ConnectException
     * @throws Exception
     * @throws RateLimitException
     * @throws ResponseException
     * @throws ServerException
     * @throws UnauthorizedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function authenticate(string $clientId, string $clientSecret): void
    {
        $credentials = base64_encode(sprintf('%s:%s', $clientId, $clientSecret));
        $response = $this->rawRequest('POST', static::API_TOKEN_URI, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => sprintf('Basic %s', $credentials)
            ],
            'query' => [
                'grant_type' => 'client_credentials'
            ]
        ]);
        $token = $this->jsonDecodeBody($response);
        $this->validateToken($token);
        $token['expires_at'] = time() + $token['expires_in'] ?? 0;
        $this->saveToken($token);
        $this->token = $token;
    }

    /**
     * @param $token Token data from HTTP response.
     * @return mixed
     */
    protected function saveToken($token)
    {
        $token = Token::create($token);
        return $token;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @return ResponseInterface
     * @throws ConnectException
     * @throws Exception
     * @throws RateLimitException
     * @throws ResponseException
     * @throws ServerException
     * @throws UnauthorizedException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function rawRequest(string $method, string $url, array $options = []): ResponseInterface
    {
        $this->sleepIfLimited();
        try {
            $response = $this->http->request($method, $url, $options);
            $this->setRateLimits($response);
            $this->sleepToPreventHittingLimits($response);
        } catch (GuzzleConnectException $connectException) {
            throw new ConnectException(
                $connectException->getMessage(),
                $connectException->getCode(),
                $connectException
            );
        } catch (BadResponseException $badResponseException) {
            $response = $badResponseException->getResponse();
            $data = [];
            try {
                $data = $this->jsonDecodeBody($response);
            } catch (ResponseException $responseException) {
            }
            $statusCode = $response->getStatusCode();
            $message = $data['detail'] ??
                $data['error_description'] ??
                $statusCode . ' ' . $response->getReasonPhrase();
            if ($statusCode == 401) {
                throw new UnauthorizedException($message, $statusCode);
            }
            if ($statusCode == 429) {
                $message = "Too many requests, retry in " . $response->getHeader('Retry-After')[0] . " seconds";
                throw new RateLimitException($message, $statusCode);
            } elseif (in_array($statusCode, [500, 502, 503, 504, 507])) {
                if ($statusCode == 504 && $this->retries <= $this->maxRetries) {
                    sleep(3);
                    $this->retries++;
                    $response = $this->rawRequest($method, $url, $options);
                } else {
                    throw new ServerException($message, $statusCode);
                }
            } elseif ($statusCode != 404) {
                if (array_key_exists('violations', $data)) {
                    $violations = json_encode($data['violations']);
                    $message = $message . ". " . $violations;
                }
                throw new ResponseException($message, $statusCode);
            }
        } catch (GuzzleException $guzzleException) {
            throw new Exception(
                "Unexpected Guzzle exception: " . $guzzleException->getMessage(),
                0,
                $guzzleException
            );
        }

        return $response;
    }

    public function setRateLimits($response)
    {
        if ($response->getHeader('X-RateLimit-Limit')) {
            $this->rateLimits = [
                'limit' => intval($response->getHeader('X-RateLimit-Limit')[0]),
                'remaining' => intval($response->getHeader('X-RateLimit-Remaining')[0]),
                'reset' => intval($response->getHeader('X-RateLimit-Reset')[0])
            ];
        }
    }

    public function sleepIfLimited()
    {
        if ($this->rateLimits['remaining'] === 0) {
            sleep($this->rateLimits['reset']);
        }
    }

    public function sleepToPreventHittingLimits($response)
    {
        if($response->getHeader('X-RateLimit-Remaining') && $response->getHeader('X-RateLimit-Reset')) {
            $remaining = intval($response->getHeader('X-RateLimit-Remaining')[0]);
            $reset = intval($response->getHeader('X-RateLimit-Reset')[0]);

            if($remaining === 0 && $reset <= 1) {
                sleep($reset);
                $this->rateLimits['remaining'] = 1;
            }
        }
    }

    /**
     * @return bool
     */
    public function isDemoMode(): bool
    {
        return $this->isDemoMode;
    }
}
