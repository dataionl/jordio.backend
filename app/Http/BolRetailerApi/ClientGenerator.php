<?php


namespace App\Http\BolRetailerApi;


use Picqer\BolRetailerV8\OpenApi\SwaggerSpecs;

class ClientGenerator extends \Picqer\BolRetailerV8\OpenApi\ClientGenerator
{
    public function __construct()
    {
        $retailer = (new SwaggerSpecs())->load(__DIR__ . '/retailerV8.json')
            ->merge((new SwaggerSpecs())->load(__DIR__ . '/sharedV8.json'));

        $this->specs = $retailer->getSpecs();
    }
}
