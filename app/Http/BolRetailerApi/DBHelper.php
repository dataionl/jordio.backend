<?php


namespace App\Http\BolRetailerApi;


use App\BillingDetails;
use App\Order;
use App\OrderItem;
use App\ReducedOrder;
use App\ShipmentDetails;
use App\Utils\OrderUtils;
use App\Utils\ShipmentUtils;
use Carbon\Carbon;

class DBHelper
{
    public static function storeOrdersWithOrderItems($orders, $orderItems)
    {
        self::storeOrderItemsUpsert($orders);
        self::storeOrderItemsUpsert($orderItems);
    }

    public static function storeOrdersUpsert($orders)
    {
        $orderCollection = collect($orders);

        Order::withTrashed()->upsert($orderCollection->map(function($o) {

            $fulfilmentMethods = [];
            foreach($o->orderItems as $orderItem) {
                $fulfilmentMethods[] = ((object)$orderItem->fulfilment)->method;
            }
            if(array_unique($fulfilmentMethods) === array('FBB')) {
                $o->status = "LVB";
            } else {
                $o->status = "OPEN";
            }

            return [
                'orderId' => $o->orderId,
                'status' => $o->status,
                'pickUpPoint' => $o->pickupPoint,
                'orderPlacedDateTime' => Carbon::parse($o->orderPlacedDateTime),
                'totalOrderPrice' => $o->totalOrderPrice
            ];
        })->toArray(), ['orderId'],['pickUpPoint', 'orderPlacedDateTime', 'totalOrderPrice']);

        ShipmentDetails::upsert($orderCollection->map(function($o) {
            $s = $o->shipmentDetails;
            return [
                'orderId' => $o->orderId,
                'pickUpPointName' => $s->pickupPointName,
                'salutation' => $s->salutation,
                'firstName' => $s->firstName,
                'surName' => $s->surname,
                'streetName' => $s->streetName,
                'houseNumber' => $s->houseNumber,
                'houseNumberExtension' => $s->houseNumberExtension,
                'extraAddressInformation' => $s->extraAddressInformation,
                'zipCode' => $s->zipCode,
                'city' => $s->city,
                'countryCode' => $s->countryCode,
                'email' => $s->email,
                'company' => $s->company,
                'deliveryPhoneNumber' => $s->deliveryPhoneNumber,
                'language' => $s->language,
            ];
        })->toArray(), ['orderId'], []);

        BillingDetails::upsert($orderCollection->map(function($o) {
            $b = $o->billingDetails;
            if($b) {
                return [
                    'orderId' => $o->orderId,
                    'salutation' => $b->salutation,
                    'firstName' => $b->firstName,
                    'surName' => $b->surname,
                    'streetName' => $b->streetName,
                    'houseNumber' => $b->houseNumber,
                    'houseNumberExtension' => $b->houseNumberExtension,
                    'extraAddressInformation' => $b->extraAddressInformation,
                    'zipCode' => $b->zipCode,
                    'city' => $b->city,
                    'countryCode' => $b->countryCode,
                    'email' => $b->email,
                    'company' => $b->company,
                    'vatNumber' => $b->vatNumber,
                    'kvkNumber' => $b->kvkNumber,
                    'orderReference' => $b->orderReference,
                ];
            } else {
                return [
                    'orderId' => $o->orderId,
                    'salutation' => "",
                    'firstName' => "",
                    'surName' => "",
                    'streetName' => "",
                    'houseNumber' => "",
                    'houseNumberExtension' => "",
                    'extraAddressInformation' => "",
                    'zipCode' => "",
                    'city' => "",
                    'countryCode' => "",
                    'email' => "",
                    'company' => "",
                    'vatNumber' => "",
                    'kvkNumber' => "",
                    'orderReference' => "",
                ];
            }

        })->toArray(), ['orderId'], []);
    }

    public static function storeOrderItemsUpsert($orderItems)
    {
        $orderItemsCollection = collect($orderItems);
        OrderItem::upsert($orderItemsCollection->map(function($item) {
            if(is_null($item->offer)) {
                $item->offer = (object) ['offerId' => "", 'reference' => ""];
            }
            return [
                'orderItemId' => $item->orderItemId,
                'orderId' => $item->orderId,
                'cancellationRequest' => $item->cancellationRequest,
                'fulfilment' => json_encode($item->fulfilment),
                'offer' => json_encode($item->offer),
                'product' => json_encode($item->product),
                'quantity' => $item->quantity,
                'unitPrice' => $item->unitPrice,
                'commission' => $item->commission,
                'additionalServices' => json_encode($item->additionalServices)
            ];
        })->toArray(), ['orderId', 'orderItemId'], ['orderId', 'orderItemId', 'cancellationRequest', 'fulfilment', 'offer', 'product', 'quantity', 'unitPrice', 'commission', 'additionalServices']);
    }

    public static function storeOrders($orders)
    {
        foreach ($orders as $o) {
            $totalOrderPrice = OrderUtils::getTotalOrderPrice($o);
            $result = Order::withTrashed()->updateOrCreate(
                ['orderId' => $o->orderId],
                [
                    'pickUpPoint' => $o->pickUpPoint,
                    'orderPlacedDateTime' => Carbon::parse($o->orderPlacedDateTime),
                    'totalOrderPrice' => $totalOrderPrice
                ]
            );
            $shipmentDetailsArray = (array) $o->shipmentDetails;
            $shipmentDetailsArray['orderId'] = $o->orderId;
            $shipmentDetails = ShipmentDetails::updateOrCreate(
                ['orderId' => $o->orderId],
                (array) $shipmentDetailsArray
            );

            $billingDetailsArray = (array) $o->billingDetails;
            $billingDetailsArray['orderId'] = $o->orderId;
            $billingDetails = BillingDetails::updateOrCreate(
                ['orderId' => $o->orderId],
                (array) $billingDetailsArray
            );

            $orderItemsArray = (array) $o->orderItems;
            foreach($orderItemsArray as $orderItem) {
                $orderItem->orderId = $o->orderId;
                $orderItem->fulfilment = json_encode($orderItem->fulfilment);
                $orderItem->offer = json_encode($orderItem->offer);
                $orderItem->product = json_encode($orderItem->product);
                $orderItem->additionalServices = json_encode($orderItem->additionalServices);
                $item = OrderItem::withTrashed()->updateOrCreate(
                    ['orderItemId' => $orderItem->orderItemId],
                    json_decode(json_encode($orderItem),true)
                );
            }
        }
    }

    public static function updateOrderStatus($order, $status)
    {
        $order->status = $status;
        $order->save();
    }
}
