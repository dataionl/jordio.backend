<?php


namespace App\Http\BolRetailerApi;


use Picqer\BolRetailerV8\OpenApi\SwaggerSpecs;

class ModelGenerator extends \Picqer\BolRetailerV8\OpenApi\ModelGenerator
{
    public function __construct()
    {
        $retailer = (new SwaggerSpecs())->load(__DIR__ . '/retailerV8.json')
            ->merge((new SwaggerSpecs())->load(__DIR__ . '/sharedV8.json'));

        $this->specs = $retailer->getSpecs();
    }
}
