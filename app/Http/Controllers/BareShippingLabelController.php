<?php

namespace App\Http\Controllers;

use App\Utils\RecursiveArrayToObject;
use App\Utils\ShipmentUtils;
use Exception;
use Firstred\PostNL\Entity\AbstractEntity;
use Firstred\PostNL\Entity\Address;
use Firstred\PostNL\Entity\Dimension;
use Firstred\PostNL\Entity\Shipment;
use Firstred\PostNL\Exception\InvalidArgumentException;
use Firstred\PostNL\PostNL;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class BareShippingLabelController extends Controller
{
    /**
     * @var PostNL
     */
    private $postnl;

    public function __construct()
    {
        $this->postnl = (new ShipmentUtils())->postnl;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return StreamedResponse
     * @throws Exception|\Psr\Cache\InvalidArgumentException
     */
    public function create(Request $request): StreamedResponse
    {
        $request = RecursiveArrayToObject::purePreservingIntKeys($request->all());
        $shipment = Shipment::create([
            'Addresses' => $this->createAddresses($request->receiver, $request->sender),
            'Barcode' => $this->postnl->generateBarcodeByCountryCode($request->receiver->countryCode),
            'Dimension' => (new Dimension('2000'))->setCurrentService('Shipping'),
            'ProductCodeDelivery' => $request->productCodeDelivery,
            'Reference' => $request->reference,
            'DeliveryDate' => new \DateTime('tomorrow'),
        ])->setCurrentService('Shipping');

        $label = $this->postnl->generateLabel(
            $shipment,
            'GraphicFile|PDF',
            true
        );
        $pdf = base64_decode($label->getResponseShipments()[0]->getLabels()[0]->getContent());
        return response()->streamDownload(function() use ($pdf) {
            echo $pdf;
        }, 'DataIO_Retourlabel',
            [
                "Content-Type" => 'application/pdf'
            ]
        );
    }

    /**
     * @param $receiver
     * @param $sender
     * @return array
     * @throws InvalidArgumentException
     */
    private function createAddresses($receiver, $sender): array
    {
        $addresses = [];
        $addresses[]  = $this->createAddress($receiver);
        if($sender) {
            $addresses[] = $this->createAddress($sender, '02');
        }

        return $addresses;
    }

    /**
     * @param $address
     * @param string $addressType
     * @return AbstractEntity
     * @throws InvalidArgumentException
     */
    private function createAddress($address, $addressType = '01'): AbstractEntity
    {
        return Address::create([
            'AddressType' => $addressType,
            'City' => $address->city,
            'CompanyName' => $address->company,
            'Countrycode' => $address->countryCode,
            'FirstName' => $address->firstName,
            'HouseNr' => $address->houseNumber,
            'HouseNrExt' => $address->houseNumberExtension,
            'Name' => $address->surName,
            'Street' => $address->streetName,
            'Zipcode' => $address->zipCode,
        ])->setCurrentService('Shipping');
    }
}
