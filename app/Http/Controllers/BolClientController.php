<?php

namespace App\Http\Controllers;

use App\Http\BolRetailerApi\ClientGenerator;
use App\Http\BolRetailerApi\ModelGenerator;
use App\Order;
use App\Utils\PDFUtils;

class BolClientController extends Controller
{

    public function generateClient()
    {
        ClientGenerator::run();
    }

    public function generateOriginalClient()
    {
        \Picqer\BolRetailerV8\OpenApi\ClientGenerator::run();
    }

    public function generateModels()
    {
        ModelGenerator::run();
    }

    public function generateOriginalModels()
    {
        \Picqer\BolRetailerV8\OpenApi\ModelGenerator::run();
    }
}
