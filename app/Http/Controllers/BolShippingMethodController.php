<?php

namespace App\Http\Controllers;

use App\BolShippingMethod;
use App\Order;
use Illuminate\Http\Request;

class BolShippingMethodController extends Controller
{
    public function all()
    {
        $shipping_methods = BolShippingMethod::all();

        return response()->json($shipping_methods);
    }

    public function get($id)
    {
        $shipping_method = BolShippingMethod::findOrFail($id);
        return response()->json($shipping_method);
    }

    public function update(Request $request, $id)
    {
        $request = (object)$request->all();
        $request_shipping_method = (object)$request->shippingMethod;
        $shipping_method = BolShippingMethod::findOrFail($id);

        if ($request_shipping_method->default) {
            //set al other shippingMethods to default=false
            BolShippingMethod::where('default', true)
                ->update(['default' => false]);
            //set the request_shipping_method to default=true
            $shipping_method->update((array)$request_shipping_method);
        } else {
            $shipping_method->update([
                'transporter_code' => $request_shipping_method->transporter_code,
                'transporter_name' => $request_shipping_method->transporter_name,
                'track_and_trace' => $request_shipping_method->track_and_trace,
            ]);
        }

        return response()->json($shipping_method);
    }
}
