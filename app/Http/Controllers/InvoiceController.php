<?php

namespace App\Http\Controllers;

use App\Order;
use App\Utils\FileUtils;
use App\Utils\OrderUtils;
use App\Utils\PDFUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Jurosh\PDFMerge\PDFMerger;

class InvoiceController extends Controller
{
    private $pdfClient;

    public function __construct()
    {
        $this->pdfClient = new PDFUtils();
    }

    public function index($id)
    {
        $order = Order::with('shipmentDetails', 'billingDetails', 'orderItems', 'orderFiles')->findOrFail($id);
        $invoice = $order->orderFiles()->where('category', 'invoices')->latest()->first();

        //no file found in database
        if (is_null($invoice)){
            $invoice = $this->generateNewInvoice($order, $id);
        }

        //no file found in database
        if(Storage::disk('orders')->missing($invoice->filePath)){
            $invoice->delete();
            $invoice = $this->generateNewInvoice($order, $id);
        }
        $file = Storage::disk('orders')->path($invoice->filePath);
        return response()->file($file);
    }

    public function get(Request $request)
    {
        $orderUtils = new OrderUtils();
        $orders = $orderUtils->getOrdersFromRequest($request);
        if(is_null($orders)) {
            return response()->json(null, 204);
        }
        $orders = $orderUtils->sortOrders($orders);

        $pdfMerger = new PDFMerger();
        foreach($orders as $order) {
            $invoice = $this->index($order->orderId);
            $pdfMerger->addPDF($invoice->getFile()->getPathname());
        }
        $pdf = $pdfMerger->merge('string');
        return response()->streamDownload(function() use ($pdf) {
            echo $pdf;
        }, 'DataIO_Facturen',
            [
                "Content-Type" => 'application/pdf'
            ]
        );
    }

    private function generateNewInvoice($orderQuery, $id)
    {
        $fileUtils = new FileUtils();
        $fileUtils->generateInvoices($orderQuery->where('orderId', $id));
        return $orderQuery->orderFiles()->where('category', 'invoices')->latest()->first();
    }
}
