<?php

namespace App\Http\Controllers;

use App\Http\BolRetailerApi\BolClient;
use App\Offer;
use App\Utils\RecursiveArrayToObject;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Picqer\BolRetailerV8\Exception\ConnectException;
use Picqer\BolRetailerV8\Exception\Exception;
use Picqer\BolRetailerV8\Exception\RateLimitException;
use Picqer\BolRetailerV8\Exception\ResponseException;
use Picqer\BolRetailerV8\Exception\UnauthorizedException;
use Picqer\BolRetailerV8\Model\AbstractModel;
use Picqer\BolRetailerV8\Model\Fulfilment;
use Picqer\BolRetailerV8\Model\UpdateOfferRequest;

class OfferController extends Controller
{
    private $client = BolClient::class;

    public function __construct()
    {
        $this->client = new BolClient();
        $this->client->setDemoMode(env('BOL_API_DEMO_MODE'));
    }

    public function get($offerId)
    {
        return Offer::findOrFail($offerId);
    }

    public function all()
    {
        return Offer::with('shippingMethod')->get();
    }

    public function syncOffers()
    {
        $raw_offers = $this->getOfferExport();
        $offers = [];
        foreach($raw_offers as $offer) {
            $offer = (object) $offer;
            $offers[] = $this->client->getOffer($offer->offerId);
        }
        $this->storeOffers($offers);
        return $this->all();
    }

    private function getOfferExport()
    {
        $create_export = $this->client->postOfferExport('CSV');
        $process_status = $this->client->getProcessStatus($create_export->processStatusId);
        $success = $process_status->status === "SUCCESS";
        $retries = 0;
        while (!$success && $retries <= 10) {
            sleep(5);
            $process_status = $this->client->getProcessStatus($create_export->processStatusId);
            $success = $process_status->status === "SUCCESS";
            $retries++;
        }
        $offer_export = $this->client->getOfferExport($process_status->entityId);
        $offers = $this->csvToJson($offer_export);
        return $offers;
    }

    public function updateOffer(Request $request, $id)
    {
        $request = RecursiveArrayToObject::purePreservingIntKeys($request->all());
        $offer = Offer::findOrFail($id);

        $offer->reference = $request->offer->reference;
        $offer->fulfilment = $request->offer->fulfilment;
        $offer->shipping_method_id = $request->offer->shipping_method_id;
        $offer->shipment_product_code = $request->offer->shipment_product_code;
        $offer->jewelcase = $request->offer->jewelcase;
        $offer->save();

        if($request->syncOfferToBol) {
            $update_offer_request = $this->syncOfferToBol($offer);
        }

        return response()->json(['request' => $request->offer, 'offer' => $offer]);
    }

    /**
     * @param Offer $offer
     * @return JsonResponse|AbstractModel
     */
    private function syncOfferToBol(Offer $offer)
    {
        try {
            $bolOffer = $this->client->getOffer($offer->offerId);
        } catch (ConnectException | RateLimitException | ResponseException | UnauthorizedException | Exception $e) {
            return response()->json($e);
        }

        if(empty($bolOffer)) {
            return response()->json('Offer data is empty', 404);
        }

        $offer = RecursiveArrayToObject::pure($offer->jsonSerialize());
        $update_offer_request = UpdateOfferRequest::constructFromArray([
            'reference' => $offer->reference,
            'onHoldByRetailer' => $offer->onHoldByRetailer,
            'unknownProductTitle' => $offer->unknownProductTitle,
            'fulfilment' => Fulfilment::constructFromArray([
                'method' => $offer->fulfilment->method,
                'deliveryCode' => $offer->fulfilment->deliveryCode,
            ])
        ]);

        try {
            $this->client->putOffer($offer->offerId, $update_offer_request);
        } catch (ConnectException | RateLimitException | ResponseException | UnauthorizedException | Exception $e) {
            return response( )->json($e, 500);
        }

        return $update_offer_request;
    }

    public function updateOfferFulfilmentDeliveryCodes(Request $request)
    {
        $request = ((object)$request->all());
        $deliveryCode = $request->deliveryCode;
        $request_offers = $request->offers;

        if(!$deliveryCode) {
            return response()->json(["Ongeldige deliveryCode"]);
        }
        $offer_ids = array_column($request_offers, 'offerId');

        $offers = Offer::whereIn('offerId', $offer_ids)->get();

        $updatedOffers = [];
        foreach($offers as $offer) {
            $bolOffer = $this->client->getOffer($offer->offerId);
            if(!empty($bolOffer)) {
                $fulfilment_method = (((object)$bolOffer->fulfilment)->method);
                if($fulfilment_method !== "FBB") {
                    $fulfilment = new Fulfilment();
                    $fulfilment->method = $fulfilment_method;
                    $fulfilment->deliveryCode = $deliveryCode;
                    $fulfilment->pickUpPoints = null;

                    $update_offer_request = new UpdateOfferRequest();
                    $update_offer_request->fulfilment = $fulfilment;
                    $update_offer_request->onHoldByRetailer = boolval($bolOffer->onHoldByRetailer);
                    $update_offer_request->reference = $bolOffer->reference;
                    $update_offer_request->unknownProductTitle = $bolOffer->unknownProductTitle;

                    $response = $this->client->putOffer($offer->offerId, $update_offer_request);

                    //update offer in DB;
                    $offer_fulfilment = (object)$offer->fulfilment;
                    $offer_fulfilment->deliveryCode = $deliveryCode;
                    $offer->fulfilment = $offer_fulfilment;
                    $offer->onHoldByRetailer = $bolOffer->onHoldByRetailer;

                    $offer->save();
                    $updatedOffers[] = $offer;
                }
            }
        }
        return response()->json($updatedOffers);
    }

    public function updateOfferShippingMethods(Request $request)
    {
        $request = ((object)$request->all());
        $shipping_method_id = property_exists($request, 'shipping_method_id') ? $request->shipping_method_id : null;
        $request_offers = $request->offers;
        $offer_ids = array_column($request_offers, 'offerId');
        $offers = Offer::whereIn('offerId', $offer_ids);

        // We need to use the each method,
        // since mass assignment (e.g. upsert) will not dispatch events
        $offers->each(
            function($offer) use ($shipping_method_id) {
                $offer->shipping_method_id = $shipping_method_id;
                $offer->save();
            }
        );
        return response()->json($offers->get());
    }

    private function csvToJson($csv)
    {
        $rows = explode("\n", trim($csv));
        $data = array_slice($rows, 1);
        $keys = array_fill(0, count($data), $rows[0]);
        $json = array_map(function ($row, $key) {
            return array_combine(str_getcsv($key), str_getcsv($row));
        }, $data, $keys);

        return $json;
    }

    private function storeOffers($offers)
    {
        Offer::withTrashed()->upsert(collect($offers)->map(function ($offer) {
            return [
                'offerId' => $offer->offerId,
                'ean' => $offer->ean,
                'reference' => $offer->reference,
                'onHoldByRetailer' => $offer->onHoldByRetailer,
                'unknownProductTitle' => $offer->unknownProductTitle,
                'pricing' => json_encode($offer->pricing),
                'stock' => json_encode($offer->stock),
                'fulfilment' => json_encode($offer->fulfilment),
                'store' => json_encode($offer->store),
                'condition' => json_encode($offer->condition),
                'notPublishableReasons' => json_encode($offer->notPublishableReasons),
            ];
        })->toArray(), ['offerId']);
    }
}
