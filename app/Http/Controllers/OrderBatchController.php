<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderBatch;
use App\Utils\OrderUtils;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderBatchController extends Controller
{
    public function index()
    {
        return OrderBatch::orderBy('created_at', 'DESC')
            ->paginate(50)
            ->all();
    }

    public function get($id)
    {
        $batch = OrderBatch::findOrFail($id);
        return $batch;
    }

    public function getOrdersInBatch($id)
    {
        $batch = $this->get($id);
        $orders = $batch->orders();
        $orders = OrderUtils::getOrders($orders);
        $orders->orderBy('orderPlacedDateTime', 'DESC')
            ->with(['orderItems' => function($query) {
                $query->withTrashed();
            }, 'shipmentDetails', 'billingDetails', 'bolShipments', 'shipments'])
            ->withTrashed();

        return $orders->get();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function newBatch(Request $request)
    {
        $orders = Order::whereIn('orderId', $request->all());

        $order_ids = collect($orders->get())->map(function ($o) {
            return $o->orderId;
        });

        $batch = OrderBatch::create([
            'order_ids' => $order_ids,
            'status' => "OPEN",
        ]);
        $orders->update(['batch_id' => $batch->id]);
        return response()->json($batch);
    }

    /**
     * @return JsonResponse
     */
    public function getAllIds(): JsonResponse
    {
        $batches = OrderBatch::all('id', 'created_at');
        return response()->json($batches);
    }
}
