<?php

namespace App\Http\Controllers;

use App\BillingDetails;
use App\Events\NewShipmentRequest;
use App\Http\BolRetailerApi\BolClient;
use App\Http\BolRetailerApi\DBHelper;
use App\Mail\CoronaVaccin;
use App\Mail\OrderShipped;
use App\Order;
use App\OrderBatch;
use App\OrderItem;
use App\Shipment;
use App\ShipmentDetails;
use App\Utils\OrderUtils;
use App\Utils\RecursiveArrayToObject;
use App\Utils\ShipmentUtils;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Picqer\BolRetailerV8\Model\OrderItemCancellation;

class OrderController extends Controller
{
    private $client;

    public function __construct()
    {
        $this->client = new BolClient();
        $this->client->setDemoMode(env('BOL_API_DEMO_MODE'));
    }

    public function index()
    {

    }

    public function all()
    {
        $orders = OrderUtils::getOrders();
        $orders->where('status', 'OPEN')
            ->doesntHave('orderBatch')
            ->orderBy('orderPlacedDateTime', 'DESC')
            ->with('orderItems', 'shipmentDetails', 'billingDetails', 'shipments');

        //Remove orders from result where all orderItems have a different fulfilmentMethod
        if (request('fulfilmentMethod')) {
            foreach($orders->get() as $o) {
                $fulfilmentMethods = [];
                foreach($o->orderItems as $orderItem) {
                    $fulfilmentMethods[] = ((object)$orderItem->fulfilment)->method;
                }
                if(!in_array(request('fulfilmentMethod'), $fulfilmentMethods)) {
                    $orders->where('orderId', '!=',  $o->orderId);
                }
            }
        }

        return $orders->get();
    }

    public function refresh()
    {
        $this->updateAllOrders();
        if (request('cancellations') === "true") {
            $cancels = $this->handleCancellationRequests();
        }

        $orders = OrderUtils::getOrders();
        $orders->where('status', 'OPEN')
            ->doesntHave('orderBatch')
            ->orderBy('orderPlacedDateTime', 'DESC')
            ->with('orderItems', 'shipmentDetails', 'billingDetails', 'shipments');
        return $orders->get();
    }

    public function get($id)
    {
        $order = OrderUtils::getOrder($id);
        return response()->json($order);
    }

    private function updateAllOrders()
    {
        $reducedOrders = [];
        $page = 1;
        $empty = false;
        while (!$empty) {
            $orders = $this->client->getOrders($page);
            $reducedOrders = array_merge($reducedOrders, $orders);
            $page++;
            if (empty($orders) || $this->client->isDemoMode()) {
                $empty = true;
            }
        }
        $orders = [];
        $orderItems = [];
        foreach ($reducedOrders as $o) {
            $order = $this->client->getOrder($o->orderId);
            //check if the order is found
            if(!empty($order)) {
                $order->totalOrderPrice = OrderUtils::getTotalOrderPrice($order);
                $orders[] = $order;
                foreach($order->orderItems as $orderItem) {
                    $orderItem->orderId = $order->orderId;
                    $orderItems[] = $orderItem;
                }
            }
        }

        $order_chunks = array_chunk($orders, 100);
        foreach($order_chunks as $chunk) {
            DBHelper::storeOrdersUpsert($chunk);
            if (count($order_chunks) > 1) {
                sleep(3); //so we don't exceed PostNL API Limits
            }
        }
        DBHelper::storeOrderItemsUpsert($orderItems);

        return $orders;
    }

    public function getOrderItemsWithCancelRequest()
    {
        return OrderItem::whereHas('order', function ($order) {
            $order->where('status', "OPEN");
        })->where('cancellationRequest', true)->get();
    }

    public function productEan($ean)
    {
        return Order::whereJsonContains('orderItems', ['product' => ['ean' => $ean]])
            ->get();
    }

    /**
     * Cancels all OrderItems with a cancellationRequest that belong to an open Order
     * Delete the Order if the Order has all OrderItems cancelled
     */
    protected function handleCancellationRequests()
    {
        //Get all OrderItems with a cancellationRequest belonging to an open Order
        $openCancellations = $this->getOrderItemsWithCancelRequest();

        //Create an OrderItemCancellation for each OrderItem with a cancellationRequest
        foreach ($openCancellations as $orderItem) {
            if ($orderItem->cancellationRequest) {
                //Create the CancellationRequest
                $cancellation = OrderItemCancellation::constructFromArray([
                    'orderItemId' => $orderItem->orderItemId,
                    'reasonCode' => "REQUESTED_BY_CUSTOMER"
                ]);
                $processStatus = $this->client->cancelOrderItem([$cancellation]);

                //(soft)Delete the OrderItem and check if the Order has lost all it's OrderItems
                //If so; set the Order status to CANCELLED
                $orderItem->delete();
                if (is_null($orderItem->order->orderItems->first())) {
                    DBHelper::updateOrderStatus($orderItem->order, "CANCELLED");
                    $orderItem->order->delete();
                } else {
                    $this->updateTotalOrderPrice($orderItem->order);
                }
            }
        }
        return $openCancellations;
    }

    private function updateTotalOrderPrice(Order $order)
    {
        $totalOrderPrice = OrderUtils::getTotalOrderPrice($order);
        $order->totalOrderPrice = $totalOrderPrice;
        $order->save();
    }

    public function updateOrderDetails(Request $request, $id)
    {
        $request = RecursiveArrayToObject::purePreservingIntKeys($request->all());
        $request_order = $request->order;
        $order = Order::findOrFail($id);

        //ShipmentDetails update
        $values = $request_order->shipmentDetails;
        $shipmentDetails = $order->shipmentDetails;
        unset($values->created_at, $values->updated_at);
        foreach($values as $column => $value) {
            if($shipmentDetails->$column !== $value) {
                $shipmentDetails->$column = $value;
            }
        }
        if($shipmentDetails->isDirty()) {
            if($request->regenerateOrderFiles) {
                $shipmentDetails->save();
            } else {
                $shipmentDetails->saveQuietly();
            }
        }

        //BillingDetails update
        $values = $request_order->billingDetails;
        $billingDetails = $order->billingDetails;
        unset($values->created_at, $values->updated_at);
        foreach($values as $column => $value) {
            if(!$billingDetails->$column !== $value) {
                $billingDetails->$column = $value;
            }
        }
        if($billingDetails->isDirty()) {
            if($request->regenerateOrderFiles) {
                $billingDetails->save();
            } else {
                $billingDetails->saveQuietly();
            }
        }

        //Shipment update
        $request_shipment = $request_order->last_shipment;
        $shipment = Shipment::findOrFail($request_shipment->id);
        $shipment->transporter_code = $request_shipment->transporter_code;
        if($shipment->isDirty()) {
            $shipment->save();
        }

        return response()->json($order);
    }

    /**
     * @param Request $request
     * @param $orderId
     * @return JsonResponse
     * @throws \Exception
     */
    public function newShipment(Request $request, $orderId)
    {
        $request = RecursiveArrayToObject::purePreservingIntKeys($request->all());
        $order = Order::withAll()->findOrFail($orderId);
        NewShipmentRequest::dispatch($order, $request->shipment_product_code);

        $order = $order->refresh();
        return response()->json($order->shipments->last());
    }

    /**
     * @param Order $order
     * @return JsonResponse
     */
    public function delete(Order $order): JsonResponse
    {
        $order->forceDelete();

        return response()->json([
            'order' => $order,
            'deleted' => true,
        ]);
    }

    /**
     * @param OrderItem $orderItem
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteOrderItem(Request $request, OrderItem $orderItem): JsonResponse
    {
        $request = RecursiveArrayToObject::purePreservingIntKeys($request->all());

        if($request->cancel) {
            $bolOrder = $this->client->getOrder($orderItem->orderId);
            if(!empty($bolOrder)) {
                $cancellation = OrderItemCancellation::constructFromArray([
                    'orderItemId' => $orderItem->orderItemId,
                    'reasonCode' => "REQUESTED_BY_CUSTOMER"
                ]);
                $processStatus = $this->client->cancelOrderItem([$cancellation]);
            }
        }

        $orderItem->delete();

        $responseOrder = Order::withAll()->with(['orderItems' => function($query) {
            $query->withTrashed();
        }])->withTrashed()->find($orderItem->orderId);

        return response()->json([
            'order' => $responseOrder,
            'deleted' => true,
            'cancelled' => $request->cancel,
        ]);
    }

    /**
     * @param Order $order
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function changeOrderBatch(Order $order, Request $request): JsonResponse
    {
        $request = RecursiveArrayToObject::purePreservingIntKeys($request->all());
        $orderBatch = is_null($request->batch_id) ? null : OrderBatch::findOrFail($request->batch_id);
        $order->batch_id = $request->batch_id;;
        $order->save();

        $responseOrder = Order::withAll()->with(['orderItems' => function($query) {
            $query->withTrashed();
        }])->withTrashed()->find($order->orderId);

        return response()->json([
            'order' => $responseOrder,
            'batch' => $orderBatch,
        ]);
    }
}
