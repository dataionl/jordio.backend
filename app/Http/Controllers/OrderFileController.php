<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderFile;
use App\Utils\FileUtils;
use App\Utils\RecursiveArrayToObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class OrderFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Retrieve the specified resource by Id.
     *
     * @param string $id
     * @return BinaryFileResponse
     */
    public function get(string $id): BinaryFileResponse
    {
        $orderFile = OrderFile::findOrFail($id);
        return response()->file(Storage::disk('orders')->path($orderFile->filePath));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function regenerate(Request $request)
    {
        $req = RecursiveArrayToObject::purePreservingIntKeys($request->all());
        $category = $req->category;
        $order = Order::findOrFail($req->order->orderId);
        $orderQuery = $order->shipmentDetails->order();

        $fileUtils = new FileUtils();

        switch ($category) {
            case "packing_slips":
                $fileUtils->generatePackingSlips($orderQuery);
                break;
            case "invoices":
                $fileUtils->generateInvoices($orderQuery);
                break;
            case "shipping_labels":
                $fileUtils->generatePostNLShipmentsAndLabels($orderQuery);
                break;
            default:
                return response()->json("file category not found", 404);
        }

        $newFile = $orderQuery->latest()->first()->orderFiles()
            ->where('category', $category)->latest()->first();

        return response()->json($newFile);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderFile  $orderFile
     * @return \Illuminate\Http\Response
     */
    public function show(OrderFile $orderFile)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderFile  $orderFile
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderFile $orderFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderFile  $orderFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderFile $orderFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderFile  $orderFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderFile $orderFile)
    {
        //
    }
}
