<?php

namespace App\Http\Controllers;

use App\Order;
use App\Utils\FileUtils;
use App\Utils\OrderUtils;
use App\Utils\PDFUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Jurosh\PDFMerge\PDFMerger;

class PackingSlipController extends Controller
{
    private $pdfClient;

    public function __construct()
    {
        $this->pdfClient = new PDFUtils();
    }

    public function index($id)
    {
        $order = Order::with('shipmentDetails', 'billingDetails', 'orderItems', 'orderFiles')->findOrFail($id);
        $packing_slip = $order->orderFiles()->where('category', 'packing_slips')->latest()->first();

        //no file found in database
        if (is_null($packing_slip)){
            $packing_slip = $this->generateNewPackingSlip($order, $id);
        }

        //no file found in storage disk
        if(Storage::disk('orders')->missing($packing_slip->filePath)){
            $packing_slip->delete();
            $packing_slip = $this->generateNewPackingSlip($order, $id);
        }
        $file = Storage::disk('orders')->path($packing_slip->filePath);
        return response()->file($file);
    }

    public function get(Request $request)
    {
        $orderUtils = new OrderUtils();
        $orders = $orderUtils->getOrdersFromRequest($request);
        if(is_null($orders)) {
            return response()->json(null, 204);
        }
        $orders = $orderUtils->sortOrders($orders);

        $pdfMerger = new PDFMerger();
        foreach($orders as $order) {
            $packing_slip = $this->index($order->orderId);
            $pdfMerger->addPDF($packing_slip->getFile()->getPathname());
        }
        $pdf = $pdfMerger->merge('string');
        return response()->streamDownload(function() use ($pdf) {
            echo $pdf;
        }, 'DataIO_Pakbonnen',
            [
                "Content-Type" => 'application/pdf'
            ]
        );
    }

    private function generateNewPackingSlip($orderQuery, $id)
    {
        $fileUtils = new FileUtils();
        $fileUtils->generatePackingSlips($orderQuery->where('orderId', $id));
        return $orderQuery->orderFiles()->where('category', 'packing_slips')->latest()->first();
    }
}
