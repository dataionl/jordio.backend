<?php

namespace App\Http\Controllers;

use App\Order;
use App\ShipmentDetails;
use App\Utils\RecursiveArrayToObject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function search_orders(Request $request): \Illuminate\Http\JsonResponse
    {
        $request = RecursiveArrayToObject::purePreservingIntKeys($request->all());
        $input = $request->input;
        $options = $request->options;

        $orders = Order::query();
        if($request->input) {
            $orders = $orders->where('orderId', $input);
        }
        $orders = $this->filterFromOptions($orders, $options);
//        return response()->json($orders->toSql());
        $orders = $orders->with('shipmentDetails', 'billingDetails', 'orderItems')
            ->orderBy('orderPlacedDateTime', 'desc')
            ->paginate(1000);

        if (count($orders) === 0) {
            return response()->json([
                'message' => "No resources found that match the requested query."
            ], 404);
        }

        return response()->json([
            'orders' => $orders,
        ]);
    }

    /**
     * @param $orders
     * @param $options
     * @return mixed
     */
    private function filterFromOptions($orders, $options)
    {
        foreach ($options->categories as $categoryName => $categoryFields) {
            foreach ($categoryFields->fields as $key => $data) {
                if ($data->value) {
                    if($categoryName === "order") {
                        switch ($key) {
                            case 'orderId':
                                $orders = $orders->where($key, $data->input);
                                break;
                            case 'totalOrderPrice':
                                $orders = $orders->where($key, $data->selector, $data->input);
                                break;
                            case 'orderPlacedDateTime':
                                if($data->input->start) {
                                    $dateTime = Carbon::parse($data->input->start)->tz('Europe/Amsterdam')->toIso8601String();
                                    $orders = $orders->where($key, '>=', $dateTime);
                                }
                                if($data->input->end) {
                                    $dateTime = Carbon::parse($data->input->end)->tz('Europe/Amsterdam')->toIso8601String();
                                    $orders = $orders->where($key, '<=', $dateTime);
                                }
                                break;
                            default:
                                return $orders->where($key, 'like', '%' . $data->input . '%');
                        }
                    } else {
                        $orders = $orders->whereHas($categoryName, function ($query) use ($key, $data) {
                            switch ($key) {
                                case "unitPrice":
                                case "quantity":
                                    return $query->where($key, $data->selector, $data->input);
                                case "transporter_code":
                                    return $query->where($key, '=', $data->input);
                                case "title":
                                case "ean":
                                    return $query->whereJsonContains('product', [$key => $data->input]);
                                case "reference":
                                case "offerId":
                                    return $query->whereJsonContains('offer', [$key => $data->input]);
                                case "latestDeliveryDate":
                                    return $query->whereJsonContains('fulfilment', [$key => $data->input]);
                                default:
                                    return $query->where($key, 'like', '%' . $data->input . '%');
                            }
                        });
                    }
                }
            }
        }

        return $orders;
    }
}
