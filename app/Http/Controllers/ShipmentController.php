<?php

namespace App\Http\Controllers;

use App\BolShipment;
use App\BolShippingMethod;
use App\Exceptions\Exception;
use App\Http\BolRetailerApi\BolClient;
use App\Http\BolRetailerApi\DBHelper;
use App\Jobs\CheckIfBatchCompleted;
use App\Jobs\SendOrderShippedEmail;
use App\Offer;
use App\Order;
use App\OrderBatch;
use App\ShippingMethodBreakpoint;
use App\Utils\FileUtils;
use App\Utils\OrderUtils;
use App\Utils\ShipmentUtils;
use Carbon\Carbon;
use Firstred\PostNL\Entity\Address;
use Firstred\PostNL\Entity\Dimension;
use Firstred\PostNL\Entity\Label;
use Firstred\PostNL\Entity\Shipment as PostNLShipment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Jurosh\PDFMerge\PDFMerger;
use Picqer\BolRetailerV8\Model\OrderItemCancellation;
use Picqer\BolRetailerV8\Model\ShipmentRequest;
use Picqer\BolRetailerV8\Model\TransportInstruction;

class ShipmentController extends Controller
{
    private $client = BolClient::class;
    private $shipmentUtils = ShipmentUtils::class;

    public function __construct()
    {
        $this->client = new BolClient();
        $this->client->setDemoMode(env('BOL_API_DEMO_MODE'));
        $this->shipmentUtils = new ShipmentUtils();
    }

    public function all()
    {
        $orders = OrderUtils::getOrders()
            ->where('status', 'SHIPPED')
            ->with('shipmentDetails', 'orderItems');

        return $orders->get();
    }

    public function shipOrdersByOrderId(Request $request)
    {
        $request_orders = $request->all(); //Get all Orders
        $orders_ids = array_column($request_orders, 'orderId');
        $orders = Order::with('shipmentDetails', 'billingDetails', 'orderItems')->find($orders_ids);
        $shipments = $this->shipOrders($orders);
        return response()->json($shipments);
    }

    public function shipOrdersByBatchId(Request $request, $id)
    {
        $request_orders = $request->all();
        $orders_ids = array_column($request_orders, 'orderId');

        $batch = OrderBatch::findOrFail($id);
        $orders = $batch->orders();
        $orders = $orders->whereIn('orderId', $orders_ids)
            ->with('shipmentDetails', 'billingDetails', 'orderItems')
            ->get();
        $this->createBolShipments($orders);

        CheckIfBatchCompleted::dispatch($batch);
        $batch->status = "PROCESSING";
        $batch->save();

        return response()->json($batch);
    }

    private function createBolShipments($orders)
    {
        $orders->each(function($order) {
            $order->orderItems->each(function($orderItem) use ($order) {
                $shipment = $order->shipments->last();
                $bolShipment = BolShipment::updateOrCreate([
                    'shipment_id' => $shipment->id,
                    'orderId' => $order->orderId,
                    'orderItemId' => $orderItem->orderItemId
                ], []);
            });
        });
    }

    private function shipOrders($orders)
    {
        $now = Carbon::now('Europe/Amsterdam');
        $shipments = (object) [
            'success' => [],
            'skipped' => [],
            'cancelled' => [],
            'orders_shipped' => [],
        ];
        $this->shipmentUtils->setShippingMethodFields();

        //Ship all Orders
        foreach($orders as $order) {
            $bolOrder = $this->client->getOrder($order->orderId);

            if(!empty($bolOrder)) {

                //Set the shipping method to be used in Bol.com
                if(!$order->shipmentDetails->shipping_method_id){
                    $shipping_method = $this->shipmentUtils->getOrderShippingMethod($order);
                    $order->shipmentDetails->shipping_method_id = $shipping_method->transporter_code;
                    $order->shipmentDetails->save();
                }
                $bolShippingMethod = BolShippingMethod::findOrFail($order->shipmentDetails->shipping_method_id);

                foreach ($order->orderItems as $index => $orderItem) {
                    $shipmentRequest = new ShipmentRequest();

                    // Check if an order has a cancellation request before shipping
                    if($bolOrder->orderItems[$index]->cancellationRequest) {
                        $shipments->cancelled[] = ['orderItem' => $orderItem, 'order' => $order];
                        $this->cancelOrderItemBeforeShipping($orderItem);
                    } else {
                        $shipmentRequest->setOrderItemIds([$orderItem->orderItemId]);
                        $shipmentRequest->transport = TransportInstruction::constructFromArray([
                            "transporterCode" => $bolShippingMethod->transporter_code,
                        ]);
                        if($bolShippingMethod->track_and_trace) {
                            if(is_null($order->shipmentDetails->barcode)) {
                                //make sure the shipment has a barcode when required.
                                $postnl = $this->shipmentUtils->PostNL_Client(env('POSTNL_API_SANDBOX'));
                                $order->shipmentDetails->barcode = $postnl->generateBarcodeByCountryCode($order->shipmentDetails->countryCode);
                                $order->shipmentDetails->save();
                            }
                            $shipmentRequest->transport->trackAndTrace = $order->shipmentDetails->barcode;
                        }
                        $shipmentRequest->shipmentReference = $this->shipmentUtils->getBolShipmentReference($order, $now);
                        $shipments->success[] = ['orderItem' => $orderItem, 'order' => $order];
                        $shipments->shipmentRequests[] = $shipmentRequest;

                        $shipment = $this->client->shipOrderItem($shipmentRequest);
                        $order->status = "SHIPPED";
                        $order->save();
                        $shipments->orders_shipped[$order->orderId] = $order;
                    }
                }
            }
        }

        foreach($shipments->orders_shipped as $order) {
            SendOrderShippedEmail::dispatch($order);
        }

        return $shipments;
    }

    public function shipOrdersManually(Request $request)
    {
        $request_transports = $request->all();
        $now = Carbon::now('Europe/Amsterdam');

        $orders_ids = array_column($request_transports, 'orderId');
        $orders = Order::with('orderItems')->where('status', 'SKIPPED')->find($orders_ids);

        $shipments = (object) [
            'success' => [],
            'skipped' => [],
            'cancelled' => [],
            'shipmentRequests' => [],
        ];

        //Ship all Orders
        foreach($orders as $order) {
            $bolOrder = $this->client->getOrder($order->orderId);
            $transportOrderIndex = array_search($order->orderId, array_column($request_transports, 'orderId'));
            $transportInfo = (object) $request_transports[$transportOrderIndex];

            // @todo: rework this when we're using PostNL API
            if(!empty($bolOrder)) {
                $shipmentRequest = new ShipmentRequest();
                $shipmentRequest->shipmentReference = $order->orderId. " | ".$now;
                foreach ($order->orderItems as $index => $orderItem) {

                    // Check if an order has a cancellation request before shipping
                    if($bolOrder->orderItems[$index]->cancellationRequest) {
                        $shipments->cancelled[] = ['orderItem' => $orderItem, 'order' => $order];
                        $this->cancelOrderItemBeforeShipping($orderItem);
                    } else {
                        $shipmentRequest->setOrderItemIds([$orderItem->orderItemId]);
                        $shipmentRequest->transport = TransportInstruction::constructFromArray([
                            "transporterCode" => $transportInfo->transporterCode,
                            "trackAndTrace" => $transportInfo->trackAndTrace,
                        ]);
                        $shipments->shipmentRequests[] = $shipmentRequest;
                        $shipments->success[] = ['orderItem' => $orderItem, 'order' => $order];
                        $shipment = $this->client->shipOrderItem($shipmentRequest);
                    }
                }
            }
            //Change the Order status to "SHIPPED"
            if ($order->status !== "CANCELLED") {
                $order->status = "SHIPPED";
                $order->save();
            }
        }

        return response()->json($shipments);
    }

    public function getPostNLShippingLabelByOrderId($id)
    {
        $order = Order::with('shipmentDetails', 'billingDetails', 'orderItems', 'orderFiles')->findOrFail($id);
        $shipping_label = $order->orderFiles()->where('category', 'shipping_labels')->latest()->first();

        //no file found in database
        if (is_null($shipping_label)){
            $shipping_label = $this->generateNewPostNLShippingLabel($order, $id);
        }

        //no file found in database
        if(Storage::disk('orders')->missing($shipping_label->filePath)){
            $shipping_label->delete();
            $shipping_label = $this->generateNewPostNLShippingLabel($order, $id);
        }

        $file = Storage::disk('orders')->path($shipping_label->filePath);
        return response()->file($file);
    }

    public function getPostNLShippingLabels(Request $request)
    {
        $orderUtils = new OrderUtils();
        $orders = $orderUtils->getOrdersFromRequest($request);
        if (is_null($orders)) {
            return response()->json(null, 204);
        }
        $orders = $this->shipmentUtils->filterOrdersByRegionAndShippingMethod(
            $orders,
            'BE',
            'TNT'
        );

        if($orders->count() === 0) {
            return response()->json(null, 204);
        }

        $orders = $orderUtils->sortOrders($orders);

        $pdfMerger = new PDFMerger();
        foreach ($orders as $order) {
            $shipping_label = $this->getPostNLShippingLabelByOrderId($order->orderId);
            $pdfMerger->addPDF($shipping_label->getFile()->getPathname(), 'all', 'horizontal');
        }
        $pdf = $pdfMerger->merge('string');
        return response()->streamDownload(function() use ($pdf) {
            echo $pdf;
        }, 'DataIO_Verzendlabels',
            [
                "Content-Type" => 'application/pdf'
            ]
        );
    }

    private function generateNewPostNLShippingLabel($orderQuery, $id)
    {
        $fileUtils = new FileUtils();
        $fileUtils->generatePostNLShipmentsAndLabels($orderQuery->where('orderId', $id));
        return $orderQuery->orderFiles()->where('category', 'shipping_labels')->latest()->first();
    }

    public function createPostNLShipmentsAndLabels(Request $request)
    {
        $orders = $this->getOrdersFromRequest($request);
        if(is_null($orders)) {
            return response()->json(null, 204);
        }
        $orders = $this->shipmentUtils->filterOrdersByRegionAndShippingMethod(
                $orders,
                'BE',
                'TNT'
            );
        $orders = $orders->with('shipmentDetails', 'billingDetails', 'orderItems')->get();
        if(count($orders)<=0) {
            return response()->json(null, 204);
        }
        $responseLabels = $this->requestPostNLLabels($orders);
        $pdf = $this->generatePostNLLabelsFile($responseLabels);

        return response()->file($pdf);
    }

    public function createPostNLReturnLabel($orderId)
    {
        $order = Order::findOrFail($orderId);
        $order->productCodeDelivery = $this->shipmentUtils->getPostNLProductCode($order);
        $postnl = $this->shipmentUtils->setPostNL_Client(env('POSTNL_API_SANDBOX'));
        $shipment = $this->shipmentUtils->createPostNLReturnShipmentForOrder($order);

        $label = $postnl->generateLabel(
            $shipment,
            'GraphicFile|PDF',
            true
        );
        $pdf = base64_decode($label->getResponseShipments()[0]->getLabels()[0]->getContent());
        return response()->streamDownload(function() use ($pdf) {
            echo $pdf;
        }, 'DataIO_Retourlabel',
            [
                "Content-Type" => 'application/pdf'
            ]
        );
    }

    private function requestPostNLLabels($orders)
    {
        $orders = $this->shipmentUtils->prepareOrdersForPostNLShipment($orders);
        $postnl = $this->shipmentUtils->setPostNL_Client(env('POSTNL_API_SANDBOX'));

        $chunks = array_chunk($orders, 100, true);
        $all_labels = [];

        foreach($chunks as $chunk_index => $chunk) {
            $shipments = [];
            foreach ($chunk as $index => $order) {
                $shipment = $this->shipmentUtils->createPostNLShipmentForOrder($order);
                $shipments[] = $shipment;
            }

            $labels = $postnl->generateLabels(
                $shipments,
                'GraphicFile|PDF', // Printertype (only PDFs can be merged -- no need to use the Merged types)
                true, // Confirm immediately
                false,
                Label::FORMAT_A6 // Format -- this merges multiple A6 labels onto an A4
            );
            ksort($labels);
            //a check when PostNL can't create a label. We Return an error here.
            foreach($labels as $label_index => $label) {
                if(is_null($label)){
                    $error_order = $orders[$chunk_index * 100 + $label_index];
                    return response()->json([
                        'message' => 'Label for order '. $error_order->orderId . ' could not be made.',
                        'error' => $error_order
                    ])->setStatusCode(409);
                }
            }
            $all_labels = array_merge($all_labels, $labels);
        }

        return $all_labels;
    }

    private function generatePostNLLabelsFile($labels)
    {
        $now = Carbon::now('Europe/Amsterdam')->format('y-m-d\TH_i_s');
        $path = storage_path('app/public/shipping_labels');
        $fileName = "postnl_shipping_labels ".$now.".pdf";
        Storage::disk('public')->makeDirectory('shipping_labels');

        $labels_in_chunks = array_chunk($labels, 100, true);
        foreach($labels_in_chunks as $chunk) {
            $tempfiles = [];
            $pdfMerger = new PDFMerger;
            if(Storage::disk('public')->exists('shipping_labels/'.$fileName)) {
                $pdfMerger->addPDF($path.'/'.$fileName, 'all', 'horizontal');
            }

            foreach ($chunk as $label) {
                $pdf = base64_decode($label->getResponseShipments()[0]->getLabels()[0]->getContent());
                $tmpfname = tempnam("", "LABEL");
                rename($tmpfname, $tmpfname .= '.pdf');
                $handle = fopen($tmpfname,'w');
                fwrite($handle, $pdf);

                $tempfiles[] = (object)['handle' => $handle, 'tempname' => $tmpfname];

                $pdfMerger->addPDF($tmpfname, 'all', 'horizontal');
            }

            $pdfMerger->merge('file', $path.'/'.$fileName);

            foreach($tempfiles as $file) {
                fclose($file->handle);
                unlink($file->tempname);
            }
        }

        $file = Storage::disk('public')->path('shipping_labels/'.$fileName);
        return $file;
    }

    private function cancelOrderItemBeforeShipping($orderItem)
    {
        //Create the CancellationRequest
        $cancellation = OrderItemCancellation::constructFromArray([
            'orderItemId' => $orderItem->orderItemId,
            'reasonCode' => "REQUESTED_BY_CUSTOMER"
        ]);
        $processStatus = $this->client->cancelOrderItem([$cancellation]);

        //(soft)Delete the OrderItem and check if the Order has lost all it's OrderItems
        //If so; set the Order status to CANCELLED
        $orderItem->delete();
        if (is_null($orderItem->order->orderItems->first())) {
            DBHelper::updateOrderStatus($orderItem->order, "CANCELLED");
            $orderItem->order->delete();
        } else {
            $this->updateTotalOrderPrice($orderItem->order);
        }
    }

    private function updateTotalOrderPrice(Order $order)
    {
        $totalOrderPrice = OrderUtils::getTotalOrderPrice($order);
        $order->totalOrderPrice = $totalOrderPrice;
        $order->save();
    }

    private function getOrdersFromRequest($request)
    {
        $request_orders = $request->all();
        $orders_ids = array_column($request_orders, 'orderId');
        if(count($orders_ids) === 1) {
            $orders = Order::whereIn('orderId', $orders_ids);
        } elseif (count($orders_ids) > 1) {
            $orders = Order::whereIn('orderId', $orders_ids);
        } else {
            $orders = null;
        }

        return $orders;
    }
}
