<?php

namespace App\Http\Controllers;

use App\OrderBatch;
use App\ShippingMethodBreakpoint;
use App\Order;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShippingMethodBreakpointController extends Controller
{
    public function all()
    {
        $shippingMethodBreakpoints = ShippingMethodBreakpoint
            ::with('shippingMethod')
            ->orderBy('totalOrderPrice', 'desc')
            ->get()->toArray();

        return response()->json($shippingMethodBreakpoints);
    }

    public function get($id)
    {
        $shippingMethodBreakpoint = ShippingMethodBreakpoint::with('shippingMethod')->findOrFail($id);
        return response()->json($shippingMethodBreakpoint);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function new(Request $request)
    {
        $request = (object)$request->all();
        $shippingMethodBreakpoint = ShippingMethodBreakpoint::create([
            'totalOrderPrice' => $request->totalOrderPrice,
            'shipping_method_id' => $request->shipping_method_id,
        ]);

        return response()->json($shippingMethodBreakpoint);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request = (object)$request->all();
        $shippingMethodBreakpoint = ShippingMethodBreakpoint::with('shippingMethod')->findOrFail($id);
        $shippingMethodBreakpoint->totalOrderPrice = $request->totalOrderPrice;
        $shippingMethodBreakpoint->shipping_method_id = $request->shipping_method_id;
        $shippingMethodBreakpoint->save();

        return response()->json(
            ShippingMethodBreakpoint::with('shippingMethod')
                ->findOrFail($shippingMethodBreakpoint->id)
        );
    }

    /**
     * @return JsonResponse
     */
    public function delete($id)
    {
        $shippingMethodBreakpoint = ShippingMethodBreakpoint::findOrFail($id)->delete();

        return response()->json($shippingMethodBreakpoint);
    }

}
