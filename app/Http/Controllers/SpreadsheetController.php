<?php


namespace App\Http\Controllers;


use App\Order;
use App\Utils\OfferUtils;
use App\Utils\OrderUtils;
use App\Utils\SpreadsheetUtils;
use Carbon\Carbon;
use Faker\Core\Number;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class SpreadsheetController extends Controller
{
    private $spreadsheetUtils;

    public function __construct()
    {
        $this->spreadsheetUtils = new SpreadsheetUtils();
    }

    public function get(Request $request)
    {
        //Create a Spreadsheet instance
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('bol.com bestellingen');

        //Set the Spreadsheet Headers
        $headerNames = Order::first()->getSpreadsheetHeaderNames(7);
        $lastHeaderIndex = Coordinate::stringFromColumnIndex($headerNames[array_key_last($headerNames)]->colIndex);
        foreach($headerNames as $key => $data) {
            $sheet->setCellValueByColumnAndRow($data->colIndex, 1, $data->displayName);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($data->colIndex))->setWidth($data->width);

        }
        $sheet = $this->spreadsheetUtils->setHeaderStyles($sheet, $lastHeaderIndex);

        //Get all Orders and sort them
        $request_orders = $request->all();
        $orders_ids = array_column($request_orders, 'orderId');
        $orders = Order::with('shipmentDetails', 'billingDetails', 'orderItems')
            ->orderBy('orderPlacedDateTime', 'DESC')
            ->find($orders_ids);
        $orders = $this->spreadsheetUtils->prepareOrdersForSpreadsheet($orders);

        //Set Order data per row
        $row = 2;
        foreach ($orders as $orderIndex => $order) {
            $this->spreadsheetUtils->setOrderRowStyles($sheet, $order, $row, $lastHeaderIndex);
            $numOrderItems = count($order->orderItems);

            $currentHeaderColumnIndex = Coordinate::columnIndexFromString("A");
            foreach($headerNames as $key => $name) {
                $headerKey = $key;
                if (strpos($key, "orderItem") === 0) {
                    $headerKey = substr($key, 0, -1);
                    $orderItemIndex = (int) (substr($key, -1)-1);
                    if ($orderItemIndex < $numOrderItems) {
                        $headerData = (object) $order->getSpreadsheetDataForHeader($headerKey, $orderItemIndex, $orderIndex, $headerNames);
                        $sheet->setCellValueByColumnAndRow($currentHeaderColumnIndex, $row, $headerData->data);
                        if (property_exists($headerData, 'format')) {
                            $sheet->getStyleByColumnAndRow($currentHeaderColumnIndex, $row)
                                ->getNumberFormat()
                                ->setFormatCode($headerData->format);
                        }
                    }
                } else {
                    $headerData = (object) $order->getSpreadsheetDataForHeader($headerKey, 0, $orderIndex, $headerNames);
                    $sheet->setCellValueByColumnAndRow($currentHeaderColumnIndex, $row, $headerData->data);
                    if (property_exists($headerData, 'format')) {
                        $sheet->getStyleByColumnAndRow($currentHeaderColumnIndex, $row)
                            ->getNumberFormat()
                            ->setFormatCode($headerData->format);
                    }
                }

                $currentHeaderColumnIndex++;
            }
            $row++;
        }

        $spreadsheet = $this->spreadsheetUtils->createPostNLSheets($orders,$spreadsheet);

        //Save the Spreadsheet
        $writer = new Xlsx($spreadsheet);
        $now = Carbon::now('Europe/Amsterdam')->format('y-m-d\TH_i_s');
        $fileName = "export ".$now.".xlsx";

        ob_start();
        $writer->save('php://output');
        $content = ob_get_contents();
        ob_end_clean();
        if (count($orders) <= 1) {
            return response()->streamDownload(function() use ($content) {
                echo $content;
            }, $fileName);
        }
        Storage::disk('public')->put('exports/'.$fileName,$content);

        return response()->file(Storage::disk('public')->path('exports/'.$fileName));
    }

}
