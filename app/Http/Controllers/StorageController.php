<?php

namespace App\Http\Controllers;

use App\BillingDetails;
use App\Http\BolRetailerApi\BolClient;
use App\Http\BolRetailerApi\DBHelper;
use App\Order;
use App\OrderItem;
use App\ShipmentDetails;
use App\Utils\OrderUtils;
use App\Utils\ShipmentUtils;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Picqer\BolRetailerV8\Model\OrderItemCancellation;

class StorageController extends Controller
{
    public function get(Request $request)
    {
        $request = (object) $request->all();
        $file = Storage::disk('public')->path($request->filePath);

        return response()->file($file);
    }

    public function exports()
    {
        $files = Storage::disk('public')->files('exports');
        return response()->json($files);
    }

    public function shipping_labels()
    {
        $files = Storage::disk('public')->files('shipping_labels');
        return response()->json($files);
    }
}
