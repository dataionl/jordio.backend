<?php

namespace App\Jobs;

use App\BolShipment;
use App\Exceptions\Exception;
use App\Http\BolRetailerApi\BolClient;
use App\Mail\OrderShipped;
use App\Order;
use App\OrderBatch;
use App\OrderItem;
use App\Utils\ShipmentUtils;
use Carbon\Carbon;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Picqer\BolRetailerV8\Exception\ConnectException;
use Picqer\BolRetailerV8\Exception\RateLimitException;
use Picqer\BolRetailerV8\Exception\ResponseException;
use Picqer\BolRetailerV8\Exception\UnauthorizedException;
use Picqer\BolRetailerV8\Model\OrderItemCancellation;
use Picqer\BolRetailerV8\Model\ShipmentRequest;
use Picqer\BolRetailerV8\Model\TransportInstruction;
use Throwable;

class CheckIfBatchCompleted implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var OrderBatch
     */
    private $orderBatch;

    /**
     * Determine the time at which the job should timeout.
     *
     * @return DateTime
     */
    public function retryUntil(): DateTime
    {
        return now()->addHours(3);
    }

    /**
     * The number of seconds to wait before retrying the job.
     *
     * @var int
     */
    public $backoff = 30;

    public $completed = true;

    /**
     * Create a new job instance.
     *
     * @param OrderBatch $orderBatch
     */
    public function __construct(OrderBatch $orderBatch)
    {
        $this->onQueue('batch_check');
        $this->orderBatch = $orderBatch;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {

        if($this->orderBatch->status === "PROCESSING") {
            $this->orderBatch->orders->each(function($order){
                if($order->bolShipments->count() > 0) {
                    $order->bolShipments->each(function($bolShipment){
                        if($bolShipment->status === "QUEUED") {
                            $this->completed = false;
                        }
                    });
                }
            });
        }

        if($this->completed) {
            $this->orderBatch->status = "COMPLETED";
            $this->orderBatch->save();
        } else {
            $this->release($this->backoff);
        }
    }

    /**
     * Handle a job failure.
     *
     * @param Throwable $exception
     * @return void
     */
    public function failed(Throwable $exception)
    {
        dump($exception->getMessage());
    }
}
