<?php

namespace App\Jobs;

use App\Http\BolRetailerApi\BolClient;
use App\Offer;
use App\Utils\CsvToJson;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use Picqer\BolRetailerV8\Exception\ConnectException;
use Picqer\BolRetailerV8\Exception\Exception;
use Picqer\BolRetailerV8\Exception\RateLimitException;
use Picqer\BolRetailerV8\Exception\ResponseException;
use Picqer\BolRetailerV8\Exception\UnauthorizedException;
use Picqer\BolRetailerV8\Model\ProcessStatus;

class GetOfferExport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Determine the time at which the job should timeout.
     *
     * @return DateTime
     */
    public function retryUntil(): DateTime
    {
        return now()->addHours(3);
    }

    /**
     * The number of seconds to wait before retrying the job.
     *
     * @var int
     */
    public $backoff = 5;

    /**
     * @var ProcessStatus
     */
    protected $processStatus;

    /**
     * @var BolClient
     */
    private $client;

    /**
     * Create a new job instance.
     *
     * @param ProcessStatus $processStatus
     */
    public function __construct(ProcessStatus $processStatus)
    {
        $this->onQueue('process_status_checks');
        $this->processStatus = $processStatus;
    }

    private function setBolClient()
    {
        $this->client = new BolClient();
        $this->client->setDemoMode(env('BOL_API_DEMO_MODE'));
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws ConnectException
     * @throws Exception
     * @throws RateLimitException
     * @throws ResponseException
     * @throws UnauthorizedException
     */
    public function handle()
    {
        $this->setBolClient();
        $this->processStatus = $this->client->getProcessStatus($this->processStatus->processStatusId);

        if ($this->isSuccess($this->processStatus)) {
            $offer_export = $this->client->getOfferExport($this->processStatus->entityId);
            $json_offers = CsvToJson::convert($offer_export);
            $offers = $this->getAllOfferDetails($json_offers);
            $this->storeOffers($offers);
        }
    }

    /**
     * @param ProcessStatus $processStatus
     * @return bool
     */
    private function isSuccess(ProcessStatus $processStatus): bool
    {
        if ($processStatus->status === "SUCCESS") {
            $this->processStatus = $processStatus;
            return true;
        } else {
            $this->release($this->backoff);
        }
        return false;
    }

    private function getAllOfferDetails($json_offers): array
    {
        $offers = [];
        foreach ($json_offers as $offer) {
            $offer = (object)$offer;
            $offers[] = $this->client->getOffer($offer->offerId);
        }
        return $offers;
    }

    /**
     * @param $offers
     */
    private function storeOffers($offers)
    {
        Offer::withTrashed()->upsert(collect($offers)->map(function ($offer) {
            return [
                'offerId' => $offer->offerId,
                'ean' => $offer->ean,
                'reference' => $offer->reference,
                'onHoldByRetailer' => $offer->onHoldByRetailer,
                'unknownProductTitle' => $offer->unknownProductTitle,
                'pricing' => json_encode($offer->pricing),
                'stock' => json_encode($offer->stock),
                'fulfilment' => json_encode($offer->fulfilment),
                'store' => json_encode($offer->store),
                'condition' => json_encode($offer->condition),
                'notPublishableReasons' => json_encode($offer->notPublishableReasons),
            ];
        })->toArray(), ['offerId']);
    }
}
