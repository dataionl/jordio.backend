<?php

namespace App\Jobs;

use App\BolShipment;
use App\Exceptions\Exception;
use App\Http\BolRetailerApi\BolClient;
use App\Mail\OrderShipped;
use App\Order;
use App\OrderItem;
use App\Utils\ShipmentUtils;
use Carbon\Carbon;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Picqer\BolRetailerV8\Exception\ConnectException;
use Picqer\BolRetailerV8\Exception\RateLimitException;
use Picqer\BolRetailerV8\Exception\ResponseException;
use Picqer\BolRetailerV8\Exception\UnauthorizedException;
use Picqer\BolRetailerV8\Model\OrderItemCancellation;
use Picqer\BolRetailerV8\Model\ShipmentRequest;
use Picqer\BolRetailerV8\Model\TransportInstruction;
use Throwable;

class ProcessBolShipment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var BolShipment
     */
    protected $bolShipment;

    /**
     * @var BolClient
     */
    private $client = BolClient::class;

    /**
     * @var mixed
     */
    private $order;

    /**
     * Determine the time at which the job should timeout.
     *
     * @return DateTime
     */
    public function retryUntil(): DateTime
    {
        return now()->addHours(3);
    }

    /**
     * The number of seconds to wait before retrying the job.
     *
     * @var int
     */
    public $backoff = [5, 10, 20, 60, 60, 60, 60, 60, 60, 600];

    /**
     * Create a new job instance.
     *
     * @param BolShipment $bolShipment
     */
    public function __construct(BolShipment $bolShipment)
    {
        $this->onQueue('bol_shipments');
        $this->bolShipment = $bolShipment;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws ConnectException
     * @throws RateLimitException
     * @throws ResponseException
     * @throws UnauthorizedException
     * @throws \Picqer\BolRetailerV8\Exception\Exception
     */
    public function handle()
    {
        if(is_null($this->bolShipment->order)){
            $exception = new Exception('Order not found: '. $this->bolShipment->orderId);
            $this->fail($exception);
        }
        if(is_null($this->bolShipment->orderItem)) {
            $exception = new Exception('OrderItem not found: '. $this->bolShipment->orderItemId);
            $this->fail($exception);
        }

        $this->order = $this->bolShipment->order;
        $this->client = new BolClient();
        $this->client->setDemoMode(env('BOL_API_DEMO_MODE'));

        try {
            $bolOrder = $this->client->getOrder($this->bolShipment->orderItem->orderId);
        } catch (RateLimitException $e) {
            sleep(1);
            throw $e;
        } catch (ConnectException | ResponseException | UnauthorizedException | \Picqer\BolRetailerV8\Exception\Exception $e) {
            $this->failed($e);
            throw $e;
        }

        if (!empty($bolOrder)) {
            if(!$this->acceptCancellationRequest($bolOrder)) {
                $orderItem = $this->bolShipment->orderItem;
                if(((object)$orderItem->fulfilment)->method === "FBB") {
                    $this->bolShipment->status = "LVB";
                    $this->bolShipment->save();
                } else {
                    $shipmentRequest = new ShipmentRequest();
                    $shipmentRequest->setOrderItemIds([$orderItem->orderItemId]);
                    $shipmentRequest->transport = TransportInstruction::constructFromArray([
                        "transporterCode" => $this->bolShipment->shipment->transporter_code
                    ]);
                    $shipmentRequest->shipmentReference = null;
                    if($this->bolShipment->shipment->transporter->track_and_trace) {
                        $shipmentRequest->transport->trackAndTrace = $this->bolShipment->shipment->barcode;
                        $shipmentRequest->shipmentReference = $this->bolShipment->shipment->barcode;
                    } elseif($this->bolShipment->shipment->barcode) {
                        $shipmentRequest->shipmentReference = $this->bolShipment->shipment->barcode;
                    }
                    try {
                        $shipment = $this->client->shipOrderItem($shipmentRequest);
                        $this->bolShipment->status = "SHIPPED";
                        $this->bolShipment->save();
                    } catch (RateLimitException $e) {
                        sleep(1);
                        throw $e;
                    } catch (ConnectException | ResponseException | UnauthorizedException | \Picqer\BolRetailerV8\Exception\Exception $e) {
                        throw $e;
                    }
                }

            }
        }
    }

    /**
     * Handle a job failure.
     *
     * @param Throwable $exception
     * @return void
     */
    public function failed(Throwable $exception)
    {
        dump($this->bolShipment->orderId . " : " . $exception->getMessage());
    }

    private function acceptCancellationRequest(\Picqer\BolRetailerV8\Model\Order $bolOrder)
    {
        foreach($bolOrder->orderItems as $bolOrderItem) {
            if(
                $bolOrderItem->orderItemId === $this->bolShipment->orderItem->orderItemId &&
                $bolOrderItem->cancellationRequest
            ) {
                $cancellation = OrderItemCancellation::constructFromArray([
                    'orderItemId' => $bolOrderItem->orderItemId,
                    'reasonCode' => "REQUESTED_BY_CUSTOMER"
                ]);
                $processStatus = $this->client->cancelOrderItem([$cancellation]);
                $this->bolShipment->orderItem->delete();
                return true;
            }
        }
        return false;
    }
}
