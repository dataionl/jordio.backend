<?php

namespace App\Jobs;

use App\Http\BolRetailerApi\BolClient;
use App\Offer;
use App\Utils\CsvToJson;
use App\Utils\RecursiveArrayToObject;
use Carbon\Carbon;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use Picqer\BolRetailerV8\Exception\ConnectException;
use Picqer\BolRetailerV8\Exception\Exception;
use Picqer\BolRetailerV8\Exception\RateLimitException;
use Picqer\BolRetailerV8\Exception\ResponseException;
use Picqer\BolRetailerV8\Exception\UnauthorizedException;
use Picqer\BolRetailerV8\Model\Fulfilment;
use Picqer\BolRetailerV8\Model\ProcessStatus;
use Picqer\BolRetailerV8\Model\RetailerOffer;
use Picqer\BolRetailerV8\Model\UpdateOfferRequest;

class UpdateOfferFulfilment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Determine the time at which the job should timeout.
     *
     * @return DateTime
     */
    public function retryUntil(): DateTime
    {
        return now()->addHours(3);
    }

    /**
     * The number of seconds to wait before retrying the job.
     *
     * @var int
     */
    public $backoff = 10;

    /**
     * @var BolClient
     */
    private $client;

    /**
     * @var Offer
     */
    protected $offer;

    /**
     * @var string
     */
    protected $fromDeliveryCode;

    /**
     * @var string
     */
    protected $toDeliveryCode;

    /**
     * Create a new job instance.
     * @param Offer $offer
     * @param $fromDeliveryCode
     * @param $toDeliveryCode
     */
    public function __construct(Offer $offer, $fromDeliveryCode, $toDeliveryCode)
    {
        $this->onQueue('offers');
        $this->offer = $offer;
        $this->fromDeliveryCode = $fromDeliveryCode;
        $this->toDeliveryCode = $toDeliveryCode;
    }

    private function setBolClient()
    {
        $this->client = new BolClient();
        $this->client->setDemoMode(env('BOL_API_DEMO_MODE'));
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $this->setBolClient();
        $bolOffer = $this->client->getOffer($this->offer->offerId);

        if(!empty($bolOffer)) {
            $bolOffer = RecursiveArrayToObject::purePreservingIntKeys($bolOffer);
            if(
                $bolOffer->fulfilment->method === "FBR" &&
                $bolOffer->fulfilment->deliveryCode === $this->fromDeliveryCode
            ) {
                $fulfilment = $this->newFulfilment($bolOffer, $this->toDeliveryCode);
                $updateOfferRequest = $this->newUpdateOfferRequest($fulfilment, $bolOffer);
                $processStatus = $this->client->putOffer($this->offer->offerId, $updateOfferRequest);
                $this->saveOffer($fulfilment, $bolOffer);

                if($this->fromDeliveryCode === "24uurs-17") {
                    $delayUntil = Carbon::now()->next('Saturday')->addHours(12);
                    UpdateOfferFulfilment::dispatch($this->offer, $this->toDeliveryCode, '24uurs-12')
                        ->delay($delayUntil);
                } elseif ($this->fromDeliveryCode === "1-2d") {
                    $delayUntil = Carbon::today()->addHours(17);
                    UpdateOfferFulfilment::dispatch($this->offer, $this->toDeliveryCode, '24uurs-17')
                        ->delay($delayUntil);
                }
            }
        }
    }

    /**
     * @param $offer
     * @param $deliveryCode
     * @return Fulfilment
     */
    private function newFulfilment($offer, $deliveryCode): Fulfilment
    {
        $fulfilment = new Fulfilment();
        $fulfilment->method = $offer->fulfilment->method;
        $fulfilment->deliveryCode = $deliveryCode;

        return $fulfilment;
    }

    /**
     * @param $fulfilment
     * @param $bolOffer
     * @return UpdateOfferRequest
     */
    private function newUpdateOfferRequest($fulfilment, $bolOffer): UpdateOfferRequest
    {
        $updateOfferRequest = new UpdateOfferRequest();
        $updateOfferRequest->fulfilment = $fulfilment;
        $updateOfferRequest->onHoldByRetailer = boolval($bolOffer->onHoldByRetailer);
        $updateOfferRequest->reference = $bolOffer->reference;
        $updateOfferRequest->unknownProductTitle = $bolOffer->unknownProductTitle;

        return $updateOfferRequest;
    }

    private function saveOffer($fulfilment, $bolOffer)
    {
        $this->offer->fulfilment = $fulfilment;
        $this->offer->onHoldByRetailer = $bolOffer->onHoldByRetailer;

        $this->offer->save();
    }

}
