<?php

namespace App\Listeners;

use App\Events\BillingDetailsUpdated;
use App\Events\OrderCreated;
use App\Events\OrderItemDeleted;
use App\Events\ShipmentDetailsUpdated;
use App\Jobs\ProcessBolShipment;
use App\Utils\FileUtils;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AllOrderItemsCancelled
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param $event
     * @return void
     */
    public function handle($event)
    {
        $order = $event->order->first();
        if(is_null($order->orderItems->first())) {
            $order->status = "CANCELLED";
            $order->delete();
        }
    }
}
