<?php

namespace App\Listeners;

use App\Events\BillingDetailsUpdated;
use App\Events\OrderCreated;
use App\Events\OrderItemDeleted;
use App\Events\ShipmentDetailsUpdated;
use App\Jobs\ProcessBolShipment;
use App\Jobs\SendOrderShippedEmail;
use App\Utils\FileUtils;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class IsAllOrderItemsShipped
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param $event
     * @return void
     */
    public function handle($event)
    {
        $bolShipment = $event->bolShipment;
        $order = $bolShipment->order;

        $bolShipmentsShipped = $order->bolShipments()->where('status', 'SHIPPED')->get();
        if($bolShipmentsShipped->count() >= $order->orderItems->count()) {
            $order->status = "SHIPPED";
            $order->saveQuietly();

            SendOrderShippedEmail::dispatch($order);
        }
    }
}
