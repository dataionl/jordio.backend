<?php

namespace App\Listeners;

use App\Events\BillingDetailsUpdated;
use App\Events\OrderCreated;
use App\Events\OrderItemDeleted;
use App\Events\ShipmentDetailsUpdated;
use App\Jobs\ProcessBolShipment;
use App\Utils\FileUtils;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class QueueBolShipmentJob
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param $event
     * @return void
     */
    public function handle($event)
    {
        ProcessBolShipment::dispatch($event->bolShipment);
    }
}
