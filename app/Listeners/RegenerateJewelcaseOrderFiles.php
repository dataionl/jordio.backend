<?php

namespace App\Listeners;

use App\Events\BillingDetailsUpdated;
use App\Events\OrderItemDeleted;
use App\Events\ShipmentDetailsUpdated;
use App\Order;
use App\ShipmentDetails;
use App\Utils\FileUtils;
use App\Utils\ShipmentUtils;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RegenerateJewelcaseOrderFiles
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param $event
     * @return void
     */
    public function handle($event)
    {
        $offer = $event->offer;
        if($offer->wasChanged(['jewelcase'])) {
            $orders = $this->getOpenOrdersByOfferId($offer->offerId);
            $fileUtils = new FileUtils();

            $orders->get()->each(function($order) use ($fileUtils) {
                $orderQuery = $order->shipmentDetails->order();
                $fileUtils->generatePackingSlips($orderQuery);
                $fileUtils->generateInvoices($orderQuery);
            });
        }
    }

    private function getOpenOrdersByOfferId($offerId)
    {
        return Order::where('status', 'OPEN')
            ->whereHas('orderItems', function ($query) use ($offerId) {
                return $query->whereJsonContains('offer', ['offerId' => $offerId]);
            });
    }
}
