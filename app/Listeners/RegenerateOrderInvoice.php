<?php

namespace App\Listeners;

use App\Events\BillingDetailsUpdated;
use App\Events\OrderCreated;
use App\Events\OrderItemDeleted;
use App\Events\ShipmentDetailsUpdated;
use App\Utils\FileUtils;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RegenerateOrderInvoice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param $event
     * @return void
     */
    public function handle($event)
    {
        switch (get_class($event)) {
            case ShipmentDetailsUpdated::class:
                $shipmentDetails = $event->shipmentDetails;
                if ($shipmentDetails->wasChanged($shipmentDetails->invoiceFields())) {
                    $this->generateInvoice($event->orderQuery);
                }
                break;
            case BillingDetailsUpdated::class:
                $billingDetails = $event->billingDetails;
                if ($billingDetails->wasChanged($billingDetails->invoiceFields())) {
                    $this->generateInvoice($event->orderQuery);
                }
                break;
            case OrderCreated::class:
                $this->generateInvoice($event->orderQuery);
                break;
            case OrderItemDeleted::class:
                $this->generateInvoice($event->orderQuery);
        }
    }

    private function generateInvoice($orderQuery)
    {
        (new FileUtils())->generateInvoices($orderQuery);
    }
}
