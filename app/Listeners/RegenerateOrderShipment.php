<?php

namespace App\Listeners;

use App\Shipment;
use App\Utils\ShipmentUtils;
use Firstred\PostNL\Exception\CifDownException;
use Firstred\PostNL\Exception\CifException;
use Firstred\PostNL\Exception\HttpClientException;
use Firstred\PostNL\Exception\InvalidBarcodeException;
use Firstred\PostNL\Exception\InvalidConfigurationException;
use Firstred\PostNL\Exception\ResponseException;
use Firstred\PostNL\PostNL;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RegenerateOrderShipment
{
    /**
     * @var ShipmentUtils
     */
    private $shipmentUtils;

    /**
     * @var int|null
     */
    private $BE_parcel_breakpoint_value;
    /**
     * @var mixed
     */
    private $transporter_code;

    /**
     * @var int
     */
    private $shipment_product_code;

    /**
     * @var string|null
     */
    private $barcode;

    /**
     * @var PostNL
     */
    private $postnl;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param $event
     * @return void
     * @throws CifDownException
     * @throws CifException
     * @throws HttpClientException
     * @throws InvalidBarcodeException
     * @throws InvalidConfigurationException
     * @throws ResponseException
     */
    public function handle($event)
    {
        $this->shipmentUtils = new ShipmentUtils();
        $this->shipmentUtils->setShippingMethodFields();
        $this->postnl = $this->shipmentUtils->postnl;

        //Needed for BE productcode
        $this->BE_parcel_breakpoint_value = env('BOL_API_REGION') === "BE" ? $this->shipmentUtils->get_BE_ParcelBreakpointValue() : null;

        $order = $event->order;
        $this->transporter_code = $this->getShippingMethodId($order);
        if (property_exists($event, 'shipment_product_code') && $event->shipment_product_code) {
            $this->shipment_product_code = $event->shipment_product_code;
        } else {
            $this->shipment_product_code = $this->getShipmentProductCode($order);
        }
        $this->barcode = $this->getBarcode($order);

        $this->createDBShipment($order);
    }

    private function getShippingMethodId($order)
    {
        $shipping_method = $this->shipmentUtils->getOrderShippingMethod($order);
        return $shipping_method->transporter_code;
    }

    private function getShipmentProductCode($order): int
    {
        return $this->shipmentUtils->getPostNLProductCode($order, $this->BE_parcel_breakpoint_value);
    }

    /**
     * @param $order
     * @return string|null
     * @throws CifDownException
     * @throws CifException
     * @throws HttpClientException
     * @throws InvalidBarcodeException
     * @throws InvalidConfigurationException
     * @throws ResponseException
     */
    private function getBarcode($order): ?string
    {
        if ($order->shipmentDetails->countryCode === "BE") {
            if ($this->transporter_code === "TNT") {
                $barcode = $this->postnl->generateBarcodeByCountryCode($order->shipmentDetails->countryCode);
            } elseif ($order->grandTotalPrice("FBR") >= $this->BE_parcel_breakpoint_value) {
                $barcode = $this->postnl->generateBarcodeByCountryCode($order->shipmentDetails->countryCode);
            } else {
                $barcode = null;
            }
        } else {
            $barcode = $this->postnl->generateBarcode();
        }
        return $barcode;
    }

    private function createDBShipment($order)
    {
        return Shipment::create([
            'transporter_code' => $this->transporter_code,
            'product_code' => $this->shipment_product_code,
            'barcode' => $this->barcode,
            'reference' => $order->orderId,
            'orderId' => $order->orderId,
        ]);
    }

}
