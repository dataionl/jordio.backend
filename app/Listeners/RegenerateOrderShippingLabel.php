<?php

namespace App\Listeners;

use App\Events\BillingDetailsUpdated;
use App\Events\NewShipmentRequest;
use App\Events\OrderCreated;
use App\Events\OrderItemDeleted;
use App\Events\ShipmentDetailsUpdated;
use App\Utils\FileUtils;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RegenerateOrderShippingLabel
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param $event
     * @return void
     */
    public function handle($event)
    {
        switch (get_class($event)) {
            case ShipmentDetailsUpdated::class:
                $shipmentDetails = $event->shipmentDetails;
                if ($shipmentDetails->wasChanged($shipmentDetails->shippingLabelFields())) {
                    $this->generateShippingLabel($event->orderQuery);
                }
                break;
            case NewShipmentRequest::class:
            case OrderCreated::class:
                if(!property_exists($event, 'shipment_product_code') || $event->shipment_product_code !== 6400) {
                    $this->generateShippingLabel($event->orderQuery);
                }
        }
    }

    private function generateShippingLabel($orderQuery)
    {
        (new FileUtils())->generatePostNLShipmentsAndLabels($orderQuery);
    }
}
