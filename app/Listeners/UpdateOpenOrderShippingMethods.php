<?php

namespace App\Listeners;

use App\Events\BillingDetailsUpdated;
use App\Events\OrderItemDeleted;
use App\Events\ShipmentDetailsUpdated;
use App\Order;
use App\ShipmentDetails;
use App\Utils\FileUtils;
use App\Utils\ShipmentUtils;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateOpenOrderShippingMethods
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param $event
     * @return void
     */
    public function handle($event)
    {
        $offer = $event->offer;
        $orders = $this->getOpenOrdersByOfferId($offer->offerId);
        if($offer->shipping_method_id === NULL) {
            $shipmentUtils = new ShipmentUtils();
            $shipmentUtils->setShippingMethodFields();
            $orders->get()->each(function($order) use ($shipmentUtils) {
                $shipping_method = $shipmentUtils->getOrderShippingMethod($order);
                $orderShipment = $order->shipments->last();
                $orderShipment->transporter_code = $shipping_method->transporter_code;
                $orderShipment->save();
            });
        } else {
            $this->updateOrdersShippingMethods($orders, $offer->shipping_method_id);
        }
    }

    private function getOpenOrdersByOfferId($offerId)
    {
        return Order::where('status', 'OPEN')
            ->whereHas('orderItems', function ($query) use ($offerId) {
                return $query->whereJsonContains('offer', ['offerId' => $offerId]);
            });
    }

    private function updateOrdersShippingMethods($orders, $shipping_method_id)
    {
        $orders->each(function($order) use ($shipping_method_id) {
            $orderShipment = $order->shipments->last();
            $orderShipment->transporter_code = $shipping_method_id;
            $orderShipment->save();
        });
    }
}
