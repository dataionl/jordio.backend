<?php

namespace App\Listeners;

use App\Events\BillingDetailsUpdated;
use App\Events\OrderCreated;
use App\Events\OrderItemDeleted;
use App\Events\ShipmentDetailsUpdated;
use App\Jobs\ProcessBolShipment;
use App\Utils\FileUtils;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateOrderAndShipmentStatus
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param $event
     * @return void
     */
    public function handle($event)
    {
        $order = $event->order->first();
        $orderItem = $event->orderItem;

        $bolShipment = $orderItem->bolShipment;
        if($bolShipment) {
            $bolShipment->status = "CANCELLED";
            $bolShipment->save();
        }

        if(is_null($order->orderItems->first())) {
            $order->status = "CANCELLED";
            $order->saveQuietly();
            $order->delete();
        }else if($order->bolShipments->count() > $order->orderItems->count()) {
            $order->status = "MIXED";
            $order->saveQuietly();
        }
    }
}
