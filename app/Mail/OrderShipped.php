<?php

namespace App\Mail;

use App\Http\Controllers\InvoiceController;
use App\Order;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Order
     */
    public $order;
    /**
     * @var \DateTime
     */
    public $deliveryDate;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->deliveryDate = (new Carbon((((object) $order->orderItems[0]->fulfilment)->latestDeliveryDate)))
        ->translatedFormat('l d F');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $invoice = (new InvoiceController())->index($this->order->orderId);
        return $this->from('orderdesk@dataio.shop', 'DataIO')
            ->replyTo('bol@dataio.nl')
            ->attachFromStorageDisk('orders', 'invoices/'.$invoice->getFile()->getFilename())
            ->subject('BTW-factuur van je bestelling bij DataIO via Bol ['.$this->order->orderId.']')
            ->view('orders.emails.shipped');
    }
}
