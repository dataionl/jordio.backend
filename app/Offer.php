<?php

namespace App;

use App\Events\OfferUpdated;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offer extends Model
{
    use HasFactory;
    use softDeletes;

    public $primaryKey = 'offerId';
    public $incrementing = false;
    protected $keyType = "string";
    protected $casts = [
        'orderId' => 'string',
        'pricing' => 'json',
        'stock' => 'json',
        'fulfilment' => 'json',
        'store' => 'json',
        'condition' => 'json',
        'notPublishableReasons' => 'json',
    ];
    public static $snakeAttributes = false;

    protected $guarded = [];

    protected $dispatchesEvents = [
        'updated' => OfferUpdated::class,
    ];

    public function shippingMethod()
    {
        return $this->hasOne(BolShippingMethod::class, 'transporter_code', 'shipping_method_id');
    }
}
