<?php

namespace App;

use App\Events\OrderUpdated;
use App\Utils\ShipmentUtils;
use App\Utils\SpreadsheetUtils;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use ReflectionClass;

class Order extends Model
{
    use softDeletes;
    use HasFactory;

    public $primaryKey = 'orderId';
    public $incrementing = false;
    protected $keyType = "string";
    protected $casts = ['orderId' => 'string'];
    public static $snakeAttributes = false;

    protected $guarded = [];

    protected $dispatchesEvents = [
        'updated' => OrderUpdated::class,
    ];

    protected $appends = ['grand_total_price'];

    public function getGrandTotalPriceAttribute()
    {
        return $this->grandTotalPrice();
    }

    /**
     * @Relation
     */
    public function shipmentDetails()
    {
        return $this->hasOne(ShipmentDetails::class, 'orderId', 'orderId');
    }

    /**
     * @Relation
     */
    public function billingDetails()
    {
        return $this->hasOne(BillingDetails::class, 'orderId', 'orderId');
    }

    /**
     * @Relation
     */
    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'orderId', 'orderId');
    }

    /**
     * @Relation
     */
    public function orderBatch()
    {
        return $this->belongsTo(OrderBatch::class, 'batch_id', 'id');
    }

    /**
     * @Relation
     */
    public function orderFiles()
    {
        return $this->hasMany(OrderFile::class, 'orderId', 'orderId');
    }

    /**
     * @Relation
     */
    public function bolShipments()
    {
        return $this->hasMany(BolShipment::class, 'orderId', 'orderId');
    }

    /**
     * @Relation
     */
    public function shipments()
    {
        return $this->hasMany(Shipment::class, 'orderId', 'orderId');
    }

    public function scopeWithAll($query)
    {
        $query->with($this->getSupportedRelations());
    }

    public function hasReader()
    {
        $reader_references = ['SDDR-F451', 'LRW550U-RNBNG', 'LRW550U'];
        foreach($this->orderItems as $orderItem) {
            if(in_array($orderItem->offer()->reference, $reader_references)) {
                return true;
            }
        }
        return false;
    }

    public function hasShippingMethod($default_shipping_method, $offer_ids)
    {
        foreach($this->orderItems as $orderItem) {
            $offerId = ((object)$orderItem->offer)->offerId;
            if(in_array($offerId, $offer_ids)) {
                $offer = Offer::findOrFail($offerId);
                $shippingMethod = $offer->shippingMethod;
                if ($shippingMethod !== $default_shipping_method) {
                    return $shippingMethod;
                }
            }
        }
        return false;
    }

    public function hasOfferShipmentProductCode()
    {
        foreach($this->orderItems as $orderItem) {
            $offerId = ((object)$orderItem->offer)->offerId;
            $offer = Offer::find($offerId);
            if($offer) {
                if ($offer->shipment_product_code) {
                    return $offer->shipment_product_code;
                }
            }
        }

        return false;
    }

    public function isLetterboxParcel()
    {
        $shipment = $this->bolShipments->last()->shipment;
        if($shipment->product_code === "2928" ||
            $shipment->product_code === "2929" ||
            $shipment->product_code === "6400") {
            return true;
        }

        return false;
    }

    public static function getSupportedRelations(): array
    {
        $relations = [];
        $reflectionClass = new ReflectionClass(get_called_class());

        foreach($reflectionClass->getMethods() as $method)
        {
            $doc = $method->getDocComment();

            if($doc && strpos($doc, '@Relation') !== false)
            {
                $relations[] = $method->getName();
            }
        }

        return $relations;
    }

    public function addJewelcasesToOrderItems($jewelcaseOffers)
    {
        foreach($this->orderItems as $orderItem) {
            if(in_array($orderItem->offer()->offerId, $jewelcaseOffers)) {

                $orderItem->jewelcase = true;
            }
        }
    }

    public function subTotalPrice(?string $fulfilmentMethod = "ALL")
    {
        $subtotal = 0;
        foreach ($this->orderItems as $orderItem) {
            if ($fulfilmentMethod == "ALL" || $fulfilmentMethod == ((object)$orderItem->fulfilment)->method) {
                $subtotal = $subtotal + $orderItem->unitPriceTotalExVAT();
            }
        }
        return $subtotal;
    }

    public function totalVAT(?string $fulfilmentMethod = "ALL")
    {
        $totalVAT = 0;
        foreach($this->orderItems as $orderItem) {
            if ($fulfilmentMethod == "ALL" || $fulfilmentMethod == ((object)$orderItem->fulfilment)->method) {
                $totalVAT = $totalVAT + $orderItem->totalVAT();
            }
        }
        return $totalVAT;
    }

    public function grandTotalPrice(?string $fulfilmentMethod = "ALL")
    {
        return $this->subTotalPrice($fulfilmentMethod) + $this->totalVAT($fulfilmentMethod);
    }

    public function getSpreadsheetHeaderNames($numOrderItems = 1)
    {
        $default_width = 10;
        $headers = [
            'orderId' => (object) [
                'displayName' => 'bestelnummer',
                'colIndex' => 1,
                'width' => 12,
            ],
            'shipment_barcode' => (object) [
                'displayName' => 'barcode_verzending',
                'colIndex' => 2,
                'width' => 20,
            ],
            'shipment_method' => (object) [
                'displayName' => 'methode_verzending',
                'colIndex' => 3,
                'width' => 12,
            ],
            'shipment_salutation' => (object) [
                'displayName' => 'aanhef_verzending',
                'colIndex' => 4,
                'width' => $default_width,
            ],
            'shipment_firstName' => (object) [
                'displayName' => 'voornaam_verzending',
                'colIndex' => 5,
                'width' => 20,
            ],
            'shipment_surName' => (object) [
                'displayName' => 'achternaam_verzending',
                'colIndex' => 6,
                'width' => 20,
            ],
            'shipment_company' => (object) [
                'displayName' => 'bedrijfsnaam_verzending',
                'colIndex' => 7,
                'width' => 20,
            ],
            'shipment_streetName' => (object) [
                'displayName' => 'adres_verz_straat',
                'colIndex' => 8,
                'width' => 22,
            ],
            'shipment_houseNumber' => (object) [
                'displayName' => 'adres_verz_huisnummer',
                'colIndex' => 9,
                'width' => $default_width,
            ],
            'shipment_houseNumberExtension' => (object) [
                'displayName' => 'adres_verz_huisnummer_toevoeging',
                'colIndex' => 10,
                'width' => 20,
            ],
            'shipment_extraAddressInformation' => (object) [
                'displayName' => 'adres_verz_toevoeging',
                'colIndex' => 11,
                'width' => 20,
            ],
            'shipment_zipCode' => (object) [
                'displayName' => 'postcode_verzending',
                'colIndex' => 12,
                'width' => $default_width,
            ],
            'shipment_city' => (object) [
                'displayName' => 'woonplaats_verzending',
                'colIndex' => 13,
                'width' => 16,
            ],
            'shipment_countryCode' => (object) [
                'displayName' => 'land_verzending',
                'colIndex' => 14,
                'width' => 7,
            ],
            'billing_salutation' => (object) [
                'displayName' => 'aanhef_facturatie',
                'colIndex' => 15,
                'width' => $default_width,
            ],
            'billing_firstName' => (object) [
                'displayName' => 'voornaam_facturatie',
                'colIndex' => 16,
                'width' => $default_width,
            ],
            'billing_surName' => (object) [
                'displayName' => 'achternaam_facturatie',
                'colIndex' => 17,
                'width' => $default_width,
            ],
            'billing_company' => (object) [
                'displayName' => 'bedrijfsnaam_facturatie',
                'colIndex' => 18,
                'width' => $default_width,
            ],
            'billing_streetName' => (object) [
                'displayName' => 'adres_fact_straat',
                'colIndex' => 19,
                'width' => $default_width,
            ],
            'billing_houseNumber' => (object) [
                'displayName' => 'adres_fact_huisnummer',
                'colIndex' => 20,
                'width' => $default_width,
            ],
            'billing_houseNumberExtension' => (object) [
                'displayName' => 'adres_fact_huisnummer_toevoeging',
                'colIndex' => 21,
                'width' => $default_width,
            ],
            'billing_extraAddressInformation' => (object) [
                'displayName' => 'adres_fact_toevoeging',
                'colIndex' => 22,
                'width' => $default_width,
            ],
            'billing_zipCode' => (object) [
                'displayName' => 'postcode_facturatie',
                'colIndex' => 23,
                'width' => $default_width,
            ],
            'billing_city' => (object) [
                'displayName' => 'woonplaats_facturatie',
                'colIndex' => 24,
                'width' => 16,
            ],
            'billing_countryCode' => (object) [
                'displayName' => 'land_facturatie',
                'colIndex' => 25,
                'width' => 7,
            ],
            'billing_vatNumber' => (object) [
                'displayName' => 'btw_nummer_facturatie',
                'colIndex' => 26,
                'width' => $default_width,
            ],
            'billing_kvkNumber' => (object) [
                'displayName' => 'kvk_facturatie',
                'colIndex' => 27,
                'width' => $default_width,
            ],
        ];
        $lastHeaderIndex = end($headers)->colIndex;
        $headers = array_merge($headers, $this->getOrderItemHeaderNames($numOrderItems, $lastHeaderIndex));

        $lastHeaderIndex = end($headers)->colIndex;
        $totalPriceHeaders = [
            'order_subtotal' => (object) [
                'displayName' => 'Subtotal',
                'colIndex' => $lastHeaderIndex+1,
                'width' => $default_width,
            ],
            'order_totalVAT' => (object) [
                'displayName' => 'Total_BTW',
                'colIndex' => $lastHeaderIndex+2,
                'width' => $default_width,
            ],
            'order_grandTotal' => (object) [
                'displayName' => 'Grand_Total',
                'colIndex' => $lastHeaderIndex+3,
                'width' => $default_width,
            ],
        ];
        $headers = array_merge($headers, $totalPriceHeaders);

        $moreInfoHeaders = [
            'order_placedDateTime' => (object)[
                'displayName' => 'besteldatum',
                'colIndex' => $lastHeaderIndex+4,
                'width' => $default_width,
            ],
            'billing_orderReference' => (object) [
                'displayName' => 'referentie_facturatie',
                'colIndex' => $lastHeaderIndex+5,
                'width' => $default_width,
            ],
            'billing_email' => (object)[
                'displayName' => 'email_facturatie',
                'colIndex' => $lastHeaderIndex+6,
                'width' => 20,
            ],
            'order_priceAbove40' => (object) [
                'displayName' => 'boven_40',
                'colIndex' => $lastHeaderIndex+7,
                'width' => $default_width,
            ],
            'order_index' => (object) [
                'displayName' => 'sort',
                'colIndex' => $lastHeaderIndex+8,
                'width' => $default_width,
            ],
            'order_bolIndex' => (object) [
                'displayName' => 'bol',
                'colIndex' => $lastHeaderIndex+9,
                'width' => $default_width,
            ],
        ];
        $headers = array_merge($headers, $moreInfoHeaders);

        return $headers;
    }

    public function getOrderItemHeaderNames($count = 1, $lastHeaderIndex = 1)
    {
        $default_width = 10;
        $rawHeaders = [
            'orderItem_ean' => (object) [
                'displayName' => "EAN",
                'colIndex' => 1,
                'width' => 15,
            ],
            'orderItem_jewelcase' => (object) [
                'displayName' => 'JEWELCASE',
                'colIndex' => 2,
                'width' => $default_width,
            ],
            'orderItem_reference' => (object) [
                'displayName' => 'referentie',
                'colIndex' => 3,
                'width' => 23,
            ],
            'orderItem_productTitle' => (object) [
                'displayName' => 'producttitel',
                'colIndex' => 4,
                'width' => 53,
            ],
            'orderItem_quantity' => (object) [
                'displayName' => 'aantal',
                'colIndex' => 5,
                'width' => 8,
            ],
            'orderItem_unitPriceTotal' => (object) [
                'displayName' => 'prijs',
                'colIndex' => 6,
                'width' => $default_width,
            ],
            'orderItem_unitPriceExVAT' => (object) [
                'displayName' => 'per_stuk',
                'colIndex' => 7,
                'width' => $default_width,
            ],
            'orderItem_unitPriceTotalExVAT' => (object) [
                'displayName' => 'totaal',
                'colIndex' => 8,
                'width' => $default_width,
            ],
            'orderItem_totalVAT' => (object) [
                'displayName' => 'BTW',
                'colIndex' => 9,
                'width' => $default_width,
            ],
            'orderItem_latestDeliveryDate' => (object) [
                'displayName' => 'uiterste_leverdatum',
                'colIndex' => 10,
                'width' => 3,
            ],
            'orderItem_exactDeliveryDate' => (object) [
                'displayName' => 'exacte_leverdatum',
                'colIndex' => 11,
                'width' => 3,
            ],
        ];
        $numRawHeaders = count($rawHeaders);
        $headers = [];
        for($i=1;$i<=$count;$i++) {
            foreach ($rawHeaders as $key => $data) {
                $clone = clone $data;
                $clone->colIndex = ($numRawHeaders * ($i - 1)) + $clone->colIndex + $lastHeaderIndex;
                if (!($key === "orderItem_jewelcase" && $i === 1)) { //don't include index on jewelcase header displayName
                    $clone->displayName = $clone->displayName . $i;
                }
                $headers[$key . $i] = $clone;
            }
        }

        return $headers;
    }

    public function getSpreadsheetDataForHeader($key = null, $orderItemIndex = 0, $orderIndex = 0, $headers = [])
    {
        $headers = [
            'orderId' => [
                'data' => $this->orderId,
            ],
            'shipment_barcode' => [
                'data' => $this->getShipmentBarcode(),
            ],
            'shipment_method' => [
                'data' => $this->getShipmentTransporter(),
            ],
            'shipment_salutation' => [
                'data' => $this->shipmentDetails->salutation,
            ],
            'shipment_firstName' => [
                'data' => $this->shipmentDetails->firstName,
            ],
            'shipment_surName' => [
                'data' => $this->shipmentDetails->surName,
            ],
            'shipment_company' => [
                'data' => $this->shipmentDetails->company,
            ],
            'shipment_streetName' => [
                'data' => $this->shipmentDetails->streetName,
            ],
            'shipment_houseNumber' => [
                'data' => $this->shipmentDetails->houseNumber,
            ],
            'shipment_houseNumberExtension' => [
                'data' => (new SpreadsheetUtils())->formatHouseNumberExtension($this->shipmentDetails->houseNumberExtension),
            ],
            'shipment_extraAddressInformation' => [
                'data' => $this->shipmentDetails->extraAddressInformation,
            ],
            'shipment_zipCode' => [
                'data' => $this->shipmentDetails->zipCode,
            ],
            'shipment_city' => [
                'data' => $this->shipmentDetails->city,
            ],
            'shipment_countryCode' => [
                'data' => $this->shipmentDetails->countryCode,
            ],
            'billing_salutation' => [
                'data' => $this->billingDetails->salutation,
            ],
            'billing_firstName' => [
                'data' => $this->billingDetails->firstName,
            ],
            'billing_surName' => [
                'data' => $this->billingDetails->surName,
            ],
            'billing_company' => [
                'data' => $this->billingDetails->company,
            ],
            'billing_streetName' => [
                'data' => $this->billingDetails->streetName,
            ],
            'billing_houseNumber' => [
                'data' => $this->billingDetails->houseNumber,
            ],
            'billing_houseNumberExtension' => [
                'data' => $this->billingDetails->houseNumberExtension,
            ],
            'billing_extraAddressInformation' => [
                'data' => $this->billingDetails->extraAddressInformation,
            ],
            'billing_zipCode' => [
                'data' => $this->billingDetails->zipCode,
            ],
            'billing_city' => [
                'data' => $this->billingDetails->city,
            ],
            'billing_countryCode' => [
                'data' => $this->billingDetails->countryCode,
            ],
            'billing_vatNumber' => [
                'data' => $this->billingDetails->vatNumber,
                'format' => NumberFormat::FORMAT_TEXT
            ],
            'billing_kvkNumber' => [
                'data' => $this->billingDetails->kvkNumber,
            ],
            'billing_orderReference' => [
                'data' => $this->billingDetails->orderReference,
            ],
            'billing_email' => [
                'data' => $this->billingDetails->email,
            ],
            'order_placedDateTime' => [
                'data' => $this->orderPlacedDateTime,
            ],
            'orderItem_ean' => [
                'data' => $this->orderItems[$orderItemIndex]->product()->ean,
                'format' => NumberFormat::FORMAT_NUMBER,
            ],
            'orderItem_jewelcase' => [
                'data' => $this->orderItems[$orderItemIndex]->jewelcase,
                'format' => NumberFormat::FORMAT_TEXT
            ],
            'orderItem_reference' => [
                'data' => $this->orderItems[$orderItemIndex]->offer()->reference,
            ],
            'orderItem_productTitle' => [
                'data' => $this->orderItems[$orderItemIndex]->product()->title,
            ],
            'orderItem_quantity' => [
                'data' => $this->orderItems[$orderItemIndex]->quantity,
            ],
            'orderItem_unitPriceTotal' => [
                'test' => '=SUM('.Coordinate::stringFromColumnIndex($headers['orderItem_unitPriceTotal'.($orderItemIndex+1)]->colIndex).($orderIndex+2).')',
                'data' => '=SUM('.$this->orderItems[$orderItemIndex]->unitPrice. '*' .Coordinate::stringFromColumnIndex($headers['orderItem_quantity'.($orderItemIndex+1)]->colIndex).($orderIndex+2). ')',
                'format' => NumberFormat::FORMAT_ACCOUNTING_EUR,
            ],
            'orderItem_unitPriceExVAT' => [
                'data' => '=SUM('.($this->orderItems[$orderItemIndex]->unitPrice). '/1.21)',
                'format' => NumberFormat::FORMAT_ACCOUNTING_EUR,
            ],
            'orderItem_unitPriceTotalExVAT' => [
                'data' => '=SUM('.Coordinate::stringFromColumnIndex($headers['orderItem_unitPriceExVAT'.($orderItemIndex+1)]->colIndex).($orderIndex+2). '*' .Coordinate::stringFromColumnIndex($headers['orderItem_quantity'.($orderItemIndex+1)]->colIndex).($orderIndex+2). ')',
                'format' => NumberFormat::FORMAT_ACCOUNTING_EUR,
            ],
            'orderItem_totalVAT' => [
                'data' => '=SUM('.Coordinate::stringFromColumnIndex($headers['orderItem_unitPriceTotal'.($orderItemIndex+1)]->colIndex).($orderIndex+2). '-' .Coordinate::stringFromColumnIndex($headers['orderItem_unitPriceTotalExVAT'.($orderItemIndex+1)]->colIndex).($orderIndex+2). ')',
                'format' => NumberFormat::FORMAT_ACCOUNTING_EUR,
            ],
            'orderItem_latestDeliveryDate' => [
                'data' => $this->orderItems[$orderItemIndex]->fulfilment()->latestDeliveryDate,
            ],
            'orderItem_exactDeliveryDate' => [
                'data' => $this->orderItems[$orderItemIndex]->fulfilment()->exactDeliveryDate,
            ],
            'order_subtotal' => [
                'data' => $this->subTotalPriceFormula($headers, $orderIndex),
                'format' => NumberFormat::FORMAT_ACCOUNTING_EUR,
            ],
            'order_totalVAT' => [
                'data' => $this->totalVATFormula($headers, $orderIndex),
                'format' => NumberFormat::FORMAT_ACCOUNTING_EUR,
            ],
            'order_grandTotal' => [
                'data' => $this->grandTotalPriceFormula($headers, $orderIndex),
                'format' => NumberFormat::FORMAT_ACCOUNTING_EUR,
            ],
            'order_priceAbove40' => [
                'data' => $this->grandTotalPrice() >= 40 ? 1 : "",
            ],
            'order_index' => [
                'data' => $orderIndex+1,
            ],
            'order_bolIndex' => [
                'data' => $this->bolIndex,
            ]
        ];

        return $headers[$key];
    }

    public function subTotalPriceFormula($headers, $orderIndex)
    {
        $coords = [];
        for($i=1;$i<=7;$i++) {
            $coords[] = (Coordinate::stringFromColumnIndex($headers['orderItem_unitPriceTotalExVAT'.$i]->colIndex).($orderIndex+2));
        }
        return '=SUM(' .implode('+', $coords). ')';
    }

    public function totalVATFormula($headers, $orderIndex)
    {
        $coords = [];
        for($i=1;$i<=7;$i++) {
            $coords[] = (Coordinate::stringFromColumnIndex($headers['orderItem_totalVAT'.$i]->colIndex).($orderIndex+2));
        }
        return '=SUM(' .implode('+', $coords). ')';
    }

    public function grandTotalPriceFormula($headers, $orderIndex)
    {
        return '=SUM(' .(Coordinate::stringFromColumnIndex($headers['order_subtotal']->colIndex).($orderIndex+2)). '+' .(Coordinate::stringFromColumnIndex($headers['order_totalVAT']->colIndex).($orderIndex+2)). ')';
    }

    public function getShipmentBarcode() {
        if($this->bolShipments->count() > 0) {
            return $this->bolShipments->last()->shipment->barcode;
        }
        if($this->shipments->count() > 0) {
            return $this->shipments->last()->barcode;
        }
        return "";
    }

    public function getShipmentTransporter()
    {
        if($this->bolShipments->count() > 0) {
            return $this->bolShipments->last()->shipment->transporter_code;
        }
        if($this->shipments->count() > 0) {
            return $this->shipments->last()->transporter_code;
        }
        return "";
    }
}
