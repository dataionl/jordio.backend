<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderBatch extends Model
{
    use HasFactory;
    use softDeletes;

    protected $casts = [
        'order_ids' => 'json',
    ];
    public static $snakeAttributes = false;

    protected $guarded = [];

    public function orders()
    {
        return $this->hasMany(Order::class, 'batch_id', 'id');
    }

    protected $appends = ['order_count'];

    public function getOrderCountAttribute()
    {
        return $this->orders()->withTrashed()->count();
    }
}
