<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderFile extends Model
{
    use HasFactory;
    public static $snakeAttributes = false;
    protected $guarded = [];

    public function order() {
        return $this->belongsTo(Order::class, 'orderId', 'orderId');
    }

    protected static function boot()
    {
        parent::boot();
        OrderFile::created(function($orderFile) {
            $orderFile->fileName = $orderFile->composeFileName();
            $orderFile->filePath = $orderFile->composeFilePath();
            $orderFile->save();
        });
    }

    /**
     * @return string
     */
    public function composeFileName(): string
    {
        switch ($this->category) {
            case "shipping_labels":
                $localizedCategoryName = "PostNL_Verzendlabel";
                break;
            case "invoices":
                $localizedCategoryName = "Factuur";
                break;
            case "packing_slips":
                $localizedCategoryName = "Pakbon";
                break;
            default:
                $localizedCategoryName = "file";
        }

        return "DataIO_".$localizedCategoryName."_".$this->id."-".$this->orderId;
    }

    /**
     * @return string
     */
    public function composeFilePath(): string
    {
        return '/'.$this->category.'/'.$this->fileName.'.'.$this->extension;
    }
}
