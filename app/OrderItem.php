<?php

namespace App;

use App\Events\OrderItemDeleted;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use ReflectionClass;

class OrderItem extends Model
{
    use HasFactory;
    use softDeletes;

    public $primaryKey = 'orderItemId';
    public $incrementing = false;
    protected $keyType = "string";
    protected $casts = [
        'orderId' => 'string',
        'fulfilment' => 'json',
        'offer' => 'json',
        'product' => 'json',
        'additionalServices' => 'json',
        'cancellationRequest' => 'boolean',
    ];
    public static $snakeAttributes = false;

    protected $guarded = [];

    protected $dispatchesEvents = [
        'deleted' => OrderItemDeleted::class,
    ];

    protected $appends = ['status'];

    public function getStatusAttribute()
    {
        if($this->fulfilment()->method === "FBB") {
            return "LVB";
        } else if(!is_null($this->bolShipment)) {
            return $this->bolShipment->status;
        } elseif($this->trashed()) {
            return "CANCELLED";
        }
        return "OPEN";
    }

    /**
     * @Relation
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'orderId');
    }

    /**
     * @Relation
     */
    public function bolShipment()
    {
        return $this->hasOne(BolShipment::class, 'orderItemId', 'orderItemId');
    }

    /**
     * @return bool
     */
    public function hasJewelcase(): bool
    {
        $offer = $this->offer();
        $db_offer = Offer::find($offer->offerId);
        if($db_offer) {
            if($db_offer->jewelcase) {
                return true;
            }
        }

        return false;
    }

    public function isMultiPack()
    {
        return Str::contains($this->offer()->reference, '2PACK');
    }

    public function scopeWithAll($query)
    {
        $query->with($this->getSupportedRelations());
    }

    public static function getSupportedRelations(): array
    {
        $relations = [];
        $reflectionClass = new ReflectionClass(get_called_class());

        foreach($reflectionClass->getMethods() as $method)
        {
            $doc = $method->getDocComment();

            if($doc && strpos($doc, '@Relation') !== false)
            {
                $relations[] = $method->getName();
            }
        }

        return $relations;
    }

    public function getSpreadsheetHeaders()
    {
        $headers = [
            'ean',
            'jewelcase',
            'reference',
            'productTitle',
            'quantity',
            'unitPrice',
            'per item',
            'exVAT',
            'total'
        ];
        return $headers;
    }

    public function product()
    {
        return (object) $this->product;
    }

    public function offer()
    {
        return (object) $this->offer;
    }

    public function additionalServices()
    {
        return (object) $this->additionalServices;
    }

    public function fulfilment()
    {
        return (object) $this->fulfilment;
    }

    public function unitPriceTotal()
    {
        return round($this->unitPrice * $this->quantity, 2, PHP_ROUND_HALF_UP);
    }

    public function unitPriceExVAT()
    {
        return round($this->unitPrice/1.21,2,PHP_ROUND_HALF_UP);
    }

    public function unitPriceTotalExVAT()
    {
        return $this->unitPriceExVAT() * $this->quantity;
    }

    public function totalVAT()
    {
        return $this->unitPriceTotal() - $this->unitPriceTotalExVAT();
    }
}
