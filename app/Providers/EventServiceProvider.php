<?php

namespace App\Providers;

use App\Events\BillingDetailsUpdated;
use App\Events\BolShipmentCreated;
use App\Events\BolShipmentUpdated;
use App\Events\NewShipmentRequest;
use App\Events\OfferUpdated;
use App\Events\OrderCreated;
use App\Events\OrderItemDeleted;
use App\Events\OrderShipmentNotFound;
use App\Events\OrderUpdated;
use App\Events\ShipmentDetailsUpdated;
use App\Listeners\AllOrderItemsCancelled;
use App\Listeners\IsAllOrderItemsShipped;
use App\Listeners\QueueBolShipmentJob;
use App\Listeners\RegenerateJewelcaseOrderFiles;
use App\Listeners\RegenerateOrderShipment;
use App\Listeners\RegenerateOrderInvoice;
use App\Listeners\RegenerateOrderPackingSlip;
use App\Listeners\RegenerateOrderShippingLabel;
use App\Listeners\UpdateOpenOrderShippingMethods;
use App\Listeners\UpdateOrderAndShipmentStatus;
use App\Listeners\UpdateTotalOrderPrice;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ShipmentDetailsUpdated::class => [
            RegenerateOrderInvoice::class,
            RegenerateOrderPackingSlip::class,
            RegenerateOrderShipment::class,
            RegenerateOrderShippingLabel::class,
        ],
        BillingDetailsUpdated::class => [
            RegenerateOrderInvoice::class,
            RegenerateOrderPackingSlip::class,
        ],
        OrderItemDeleted::class => [
            UpdateTotalOrderPrice::class,
            UpdateOrderAndShipmentStatus::class,
            RegenerateOrderInvoice::class,
            RegenerateOrderPackingSlip::class,
        ],
        OfferUpdated::class => [
            UpdateOpenOrderShippingMethods::class,
            RegenerateJewelcaseOrderFiles::class,
        ],
        OrderCreated::class => [
            RegenerateOrderInvoice::class,
            RegenerateOrderPackingSlip::class,
            RegenerateOrderShipment::class,
            RegenerateOrderShippingLabel::class,
        ],
        BolShipmentCreated::class => [
            QueueBolShipmentJob::class,
        ],
        BolShipmentUpdated::class => [
            IsAllOrderItemsShipped::class,
        ],
        NewShipmentRequest::class => [
            RegenerateOrderShipment::class,
            RegenerateOrderShippingLabel::class,
        ],
        OrderShipmentNotFound::class => [
            RegenerateOrderShipment::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
