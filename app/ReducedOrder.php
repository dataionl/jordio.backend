<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReducedOrder extends Model
{
    public $primaryKey = 'orderId';
    public $incrementing = false;
    protected $keyType = "string";
    protected $casts = ['orderId' => 'string'];

    protected $fillable = [
        'orderId', 'orderPlacedDateTime', 'orderItems'
    ];

    public function order()
    {
        return $this->hasOne(Order::class, 'orderId');
    }
}
