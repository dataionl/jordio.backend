<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    use HasFactory;

    protected $guarded = [];
    public static $snakeAttributes = false;
    protected $casts = [
        'return' => 'boolean',
    ];

    public function transporter()
    {
        return $this->hasOne(BolShippingMethod::class, 'transporter_code', 'transporter_code');
    }

    public function bolShipments()
    {
        return $this->hasMany(BolShipment::class, 'shipment_id', 'id');
    }
}
