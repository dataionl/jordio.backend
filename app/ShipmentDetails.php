<?php

namespace App;

use App\Events\ShipmentDetailsUpdated;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShipmentDetails extends Model
{
    use HasFactory;
    protected $guarded = [];
    public static $snakeAttributes = false;

    protected $dispatchesEvents = [
        'updated' => ShipmentDetailsUpdated::class,
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'orderId');
    }

    public function invoiceFields()
    {
        return ['salutation', 'firstName', 'surName', 'streetName',
            'houseNumber', 'houseNumberExtension', 'extraAddressInformation',
            'zipCode', 'city', 'countryCode', 'company',];
    }

    public function shippingLabelFields()
    {
        return array_merge($this->invoiceFields(), [
            'barcode', 'shipping_method_id', 'shipment_productcode',
        ]);
    }

    public function getSpreadsheetHeaders()
    {
        $headers = [
            'salutation',
            'firstName',
            'surName',
            'company',
            'streetName',
            'houseNumber',
            'houseNumberExtension',
            'extraAddressInformation',
            'zipCode',
            'city',
            'countryCode'
        ];
        return $headers;
    }
}
