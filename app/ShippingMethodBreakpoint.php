<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingMethodBreakpoint extends Model
{
    use HasFactory;
    public static $snakeAttributes = false;

    protected $guarded = [];

    public function shippingMethod()
    {
        return $this->hasOne(BolShippingMethod::class, 'transporter_code', 'shipping_method_id');
    }
}
