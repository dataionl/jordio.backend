<?php


namespace App\Utils;


class CsvToJson
{
    /**
     * Converts a CSV file/string to JSON array
     *
     * @param $csv
     * @return array
     */
    public static function convert($csv): array
    {
        $rows = explode("\n", trim($csv));
        $data = array_slice($rows, 1);
        $keys = array_fill(0, count($data), $rows[0]);
        $json = array_map(function ($row, $key) {
            return array_combine(str_getcsv($key), str_getcsv($row));
        }, $data, $keys);

        return $json;
    }
}
