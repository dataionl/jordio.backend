<?php


namespace App\Utils;


use App\Order;
use App\OrderFile;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;

class FileUtils
{
    private $shipmentUtils = ShipmentUtils::class;
    private $pdfClient;

    public function generateInvoices($orders)
    {
        $orders = $orders->with('orderItems', 'shipmentDetails', 'billingDetails')->get();
        $orderFiles = $this->getFiles($orders, 'invoices', 'pdf');
    }

    public function generatePackingSlips($orders)
    {
        $orders = $orders->with('orderItems', 'shipmentDetails', 'billingDetails')->get();
        $orderFiles = $this->getFiles($orders, 'packing_slips', 'pdf');
    }

    public function generatePostNLShipmentsAndLabels($orders)
    {
        $this->shipmentUtils = new ShipmentUtils();
        $orders = $this->shipmentUtils->filterOrdersByRegionAndShippingMethod(
            $orders,
            'BE',
            'TNT',
        );

        $orders = $orders->with('shipmentDetails', 'billingDetails', 'orderItems')->get();
        $orderFiles = $this->getFiles($orders, 'shipping_labels', 'pdf');
        return $orders;
    }

    /**
     * @param object $orders
     * @param string $category
     * @param string $extension
     * @return array files
     */
    private function getFiles(object $orders, string $category, string $extension): array
    {
        $files = [];
        foreach ($orders as $order) {
            $orderFile = OrderFile::create([
                'orderId' => $order->orderId,
                'category' => $category,
                'extension' => $extension
            ]);
            $file = $this->getFileByCategory($category, $order);
            Storage::disk('orders')->put($orderFile->filePath, $file);
            $files[] = $orderFile;
        }
        return $files;
    }

    /**
     * @param string $category
     * @param Order $order
     * @return JsonResponse|object
     */
    private function getFileByCategory(string $category, Order $order)
    {
        switch ($category) {
            case "shipping_labels":
                $this->shipmentUtils->setShippingMethodFields();
                $file = $this->shipmentUtils->getPostNLShippingLabel($order);
                break;
            case "invoices":
                $this->pdfClient = new PDFUtils();
                $file = $this->pdfClient->getInvoice($order);
                break;
            case "packing_slips":
                $this->pdfClient = new PDFUtils();
                $file = $this->pdfClient->getPackingSlip($order);
                break;
            default:
                return response()->json([
                    'message' => 'Invalid file category for order '. $order->orderId,
                    'error' => $order
                ])->setStatusCode(409);
        }
        return $file;
    }
}
