<?php


namespace App\Utils;


use App\Offer;

class OfferUtils
{

    public function getJewelcaseOffers()
    {
        $jewelcaseOffers = Offer::where('jewelcase', true)->get();
        $jewelcaseOffers = collect($jewelcaseOffers)->map(function($offer) {
            return [
                $offer->offerId
            ];
        })->toArray();
        $jewelcaseOffers = array_merge(...$jewelcaseOffers);

        return $jewelcaseOffers;
    }
}
