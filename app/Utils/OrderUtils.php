<?php


namespace App\Utils;


use App\Order;
use Carbon\Carbon;

class OrderUtils
{
    public static function getTotalOrderPrice($order)
    {
        $unitPrices = [];
        foreach($order->orderItems as $orderItem) {
            if($orderItem->fulfilment->method == "FBR") { //only count FBR in total order price
                $unitPrices[] = $orderItem->unitPrice * $orderItem->quantity;
            }
        }
        return array_sum($unitPrices);
    }

    public static function getOrders($orders = null)
    {
        $orders = $orders ?: Order::latest();
        if (request('status')) {
            $orders->where('status', request('status'));
        }
        if (request('minPrice')) {
            $orders->where('totalOrderPrice', '>=', request('minPrice'));
        }
        if (request('maxPrice')) {
            $orders->where('totalOrderPrice', '<=', request('maxPrice'));
        }
        if (request('cancellationRequest')) {
            $orders->whereHas('orderItems', function ($orderItem) {
                $orderItem->where('cancellationRequest', request('cancellationRequest'));
            });
        }
        if (request('datetimeStart')) {
            $start = Carbon::parse(request('datetimeStart'))->tz('Europe/Amsterdam')->toIso8601String();
            $orders->where('orderPlacedDateTime', '>=', $start);
        }
        if (request('datetimeEnd')) {
            $end = Carbon::parse(request('datetimeEnd'))->tz('Europe/Amsterdam')->toIso8601String();
            $orders->where('orderPlacedDateTime', '<=', $end);
        }
        return $orders;
    }

    public static function getOrder($id)
    {
        $order = Order::withTrashed()->findOrFail($id);

        if(filter_var(request('full'), FILTER_VALIDATE_BOOLEAN)) {
            $order = Order::with(['orderItems' => function($query) {
                $query->withTrashed();
            }, 'billingDetails', 'shipmentDetails', 'shipments', 'orderFiles'])
                ->withTrashed()
                ->findOrFail($id);
        } else if(request('relations') && request('relations')[0] !== null) {
            $order = Order::with(request('relations'))->withTrashed()->findOrFail($id);
        }

        return $order;
    }

    public function getOrdersFromRequest($request)
    {
        $request_orders = $request->all();
        $orders_ids = array_column($request_orders, 'orderId');
        if(count($orders_ids) >= 1) {
            $orders = Order::whereIn('orderId', $orders_ids)->get();
        } else {
            $orders = null;
        }

        return $orders;
    }

    public function sortOrders($orders)
    {
        $categorizedOrders = $this->getArrayWithOrderCategories();
        foreach($orders as $order) {
            $categorizedOrders = $this->pre_sortOrders_by_category($categorizedOrders, $order);
        }
        $orders = $this->sortOrdersByCategory($categorizedOrders);

        return $orders;
    }

    private function pre_sortOrders_by_category($order_array, $order)
    {
        if ($order->hasReader() && $order->totalOrderPrice < 100) {
            $order_array['readers'][] = $order;
        } else if ($order->totalOrderPrice >= 100) {
            $order_array['above100'][] = $order;
        } else if ($order->totalOrderPrice >= 40) {
            $order_array['above40'][] = $order;
        } else {
            $order_array['others'][] = $order;
        }
        return $order_array;
    }

    private function getArrayWithOrderCategories(): array
    {
        return $orders_array = [
            'readers' => [],
            'above100' => [],
            'above40' => [],
            'others' => [],
        ];
    }

    private function sortOrdersByCategory($orders)
    {
        //sort orders >= €100 by numOrderItems > totalOrderPrice(asc)
        if (!empty($orders['above100'])) {
            usort($orders['above100'], function ($a, $b) {
                $retval = count($b->orderItems) <=> count($a->orderItems);
                if ($retval === 0) {
                    $retval = $a->totalOrderPrice <=> $b->totalOrderPrice;
                    if ($retval === 0) {
                        $retval = $a->orderId <=> $b->orderId;
                    }
                }
                return $retval;
            });
        }
        //sort orders >= €40 by numOrderItems > totalOrderPrice(asc)
        if (!empty($orders['above40'])) {
            usort($orders['above40'], function ($a, $b) {
                $retval = count($b->orderItems) <=> count($a->orderItems);
                if ($retval === 0) {
                    $retval = $a->totalOrderPrice <=> $b->totalOrderPrice;
                    if ($retval === 0) {
                        $retval = $a->orderId <=> $b->orderId;
                    }
                }
                return $retval;
            });
        }
        //sort rest orders by numOrderItems > orderItem[0]->offer->reference > orderItem[0]->quantity
        if (!empty($orders['others'])) {
            usort($orders['others'], function ($a, $b) {
                $retval = count($b->orderItems) <=> count($a->orderItems);
                if ($retval === 0) {
                    $retval = $a->orderItems[0]->offer()->reference <=> $b->orderItems[0]->offer()->reference;
                    if ($retval === 0) {
                        $retval = $a->orderItems[0]->quantity <=> $b->orderItems[0]->quantity;
                        if ($retval === 0) {
                            $retval = $a->orderId <=> $b->orderId;
                        }
                    }
                }
                return $retval;
            });
        }
        return call_user_func_array('array_merge', $orders);
    }
}
