<?php


namespace App\Utils;

use PDF;

class PDFUtils
{
    public function getInvoice($order)
    {
        $pdf = PDF::loadView('orders.pdf.invoice', compact('order'));
        return $pdf->output('order.pdf'); //download name
    }

    public function streamInvoice($order)
    {
        $pdf = PDF::loadView('orders.pdf.invoice', compact('order'));
        return $pdf->stream('order.pdf'); //download name
    }

    public function getPackingSlip($order)
    {
        $pdf = PDF::loadView('orders.pdf.packing-slip', compact('order'));
        return $pdf->output('order.pdf'); //download name
    }

    public function streamPackingSlip($order)
    {
        $pdf = PDF::loadView('orders.pdf.packing-slip', compact('order'));
        return $pdf->stream('order.pdf'); //download name
    }
}
