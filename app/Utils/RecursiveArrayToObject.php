<?php


namespace App\Utils;


use Exception;
use stdClass;

class RecursiveArrayToObject
{
    /**
     * Recursively converts an array to an object
     *
     * @param $data
     * @return stdClass
     */
    public static function pure($data): stdClass
    {
        $obj = new stdClass;
        foreach ($data as $k => $v) {
            if (strlen($k)) {
                if (is_array($v)) {
                    $obj->{$k} = self::pure($v);
                } else {
                    $obj->{$k} = $v;
                }
            }
        }
        return $obj;
    }

    /**
     * @param $data
     * @return array|stdClass
     * @throws Exception
     */
    public static function purePreservingIntKeys($data)
    {
        $resultObj = new \stdClass;
        $resultArr = array();
        $hasIntKeys = false;
        $hasStrKeys = false;
        foreach ($data as $k => $v) {
            if (!$hasIntKeys) {
                $hasIntKeys = is_int($k);
            }
            if (!$hasStrKeys) {
                $hasStrKeys = is_string($k);
            }
            if ($hasIntKeys && $hasStrKeys) {
                $e = new Exception( 'Current level has both integer and string keys, thus it is impossible to keep array or convert to object' );
                $e->vars = array('level' => $data);
                throw $e;
            }
            if ($hasStrKeys) {
                $resultObj->{$k} = is_array($v) ? self::purePreservingIntKeys($v) : $v;
            } else {
                $resultArr[$k] = is_array($v) ? self::purePreservingIntKeys($v) : $v;
            }
        }
        return ($hasStrKeys) ? $resultObj : $resultArr;
    }
}
