<?php


namespace App\Utils;


use App\BolShippingMethod;
use App\Events\OrderShipmentNotFound;
use App\Offer;
use App\Order;
use App\Shipment;
use App\ShippingMethodBreakpoint;
use Firstred\PostNL\Entity\Address;
use Firstred\PostNL\Entity\Customer;
use Firstred\PostNL\Entity\Dimension;
use Firstred\PostNL\Entity\Shipment as PostNLShipment;
use Firstred\PostNL\Exception\InvalidArgumentException;
use Firstred\PostNL\PostNL;
use Illuminate\Database\Eloquent\Builder;

class ShipmentUtils
{

    public $default_shipping_method;
    public $offers_with_shipping_method;
    public $offer_ids;
    public $lowest_shipping_method_breakpoint;
    public $postnl;
    private $currentBarcode;

    public function __construct()
    {
        $this->postnl = $this->PostNL_Client(env('POSTNL_API_SANDBOX'));
    }

    public function setPostNL_Client(bool $sandbox = false)
    {
        $this->postnl = $this->PostNL_Client($sandbox);
        return $this->postnl;
    }

    /**
     * @param bool $sandbox
     * @return mixed
     */
    private function PostNL_API_Key(bool $sandbox)
    {
        $key = env('POSTNL_API_KEY_PRODUCTION');
        if($sandbox) {
            $key = env('POSTNL_API_KEY_SANDBOX');
        }
        return $key;
    }

    /**
     * @return Customer
     */
    private function PostNL_Customer(): Customer
    {
        return Customer::create([
            'CollectionLocation' => env('POSTNL_API_CUSTOMER_COLLECTIONLOCATION'),
            'CustomerCode' => env('POSTNL_API_CUSTOMER_CUSTOMERCODE'),
            'CustomerNumber' => env('POSTNL_API_CUSTOMER_CUSTOMERNUMBER'),
            'ContactPerson' => env('POSTNL_API_CUSTOMER_CONTACTPERSON'),
            'Address' => Address::create([
                'AddressType' => env('POSTNL_API_CUSTOMER_ADDRESS_ADDRESSTYPE'),
                'City'        => env('POSTNL_API_CUSTOMER_ADDRESS_CITY'),
                'CompanyName' => env('POSTNL_API_CUSTOMER_ADDRESS_COMPANYNAME'),
                'Countrycode' => env('POSTNL_API_CUSTOMER_ADDRESS_COUNTRYCODE'),
                'HouseNr'     => env('POSTNL_API_CUSTOMER_ADDRESS_HOUSENR'),
                'Street'      => env('POSTNL_API_CUSTOMER_ADDRESS_STREET'),
                'Zipcode'     => env('POSTNL_API_CUSTOMER_ADDRESS_ZIPCODE'),
            ]),
            'Email' => env('POSTNL_API_CUSTOMER_EMAIL'),
            'Name' => env('POSTNL_API_CUSTOMER_CONTACTPERSON'),
        ]);
    }

    /**
     * @param bool $sandbox
     * @return PostNL
     */
    public function PostNL_Client(bool $sandbox = false): PostNL
    {
        return new PostNL(
            $this->PostNL_Customer(),        // The filled Customer object
            $this->PostNL_API_Key($sandbox),          // The API key
            $sandbox,            // Sandbox = false, meaning we are now using the live environment
            PostNL::MODE_REST // We are going to use the REST API (default)
        );
    }

    public function prepareOrdersForPostNLShipment($orders)
    {
        $categorizedOrders = $this->getArrayWithOrderCategories();
        foreach($orders as $order) {
            $order->productCodeDelivery = $this->getPostNLProductCode($order);
            $categorizedOrders = $this->pre_sortOrders_by_category($categorizedOrders, $order);
        }
        $orders = $this->sortOrders($categorizedOrders);

        return $orders;
    }

    private function pre_sortOrders_by_category($order_array, $order)
    {
        if ($order->hasReader() && $order->totalOrderPrice < 100) {
            $order_array['readers'][] = $order;
        } else if ($order->totalOrderPrice >= 100) {
            $order_array['above100'][] = $order;
        } else if ($order->totalOrderPrice >= 40) {
            $order_array['above40'][] = $order;
        } else {
            $order_array['others'][] = $order;
        }
        return $order_array;
    }

    private function getArrayWithOrderCategories(): array
    {
        return $orders_array = [
            'readers' => [],
            'above100' => [],
            'above40' => [],
            'others' => [],
        ];
    }

    public function sortOrders($orders)
    {
        //sort orders >= €100 by numOrderItems > totalOrderPrice(asc)
        if (!empty($orders['above100'])) {
            usort($orders['above100'], function ($a, $b) {
                $retval = count($b->orderItems) <=> count($a->orderItems);
                if ($retval === 0) {
                    $retval = $a->totalOrderPrice <=> $b->totalOrderPrice;
                    if ($retval === 0) {
                        $retval = $a->orderId <=> $b->orderId;
                    }
                }
                return $retval;
            });
        }
        //sort orders >= €40 by numOrderItems > totalOrderPrice(asc)
        if (!empty($orders['above40'])) {
            usort($orders['above40'], function ($a, $b) {
                $retval = count($b->orderItems) <=> count($a->orderItems);
                if ($retval === 0) {
                    $retval = $a->totalOrderPrice <=> $b->totalOrderPrice;
                    if ($retval === 0) {
                        $retval = $a->orderId <=> $b->orderId;
                    }
                }
                return $retval;
            });
        }
        //sort rest orders by numOrderItems > orderItem[0]->offer->reference > orderItem[0]->quantity
        if (!empty($orders['others'])) {
            usort($orders['others'], function ($a, $b) {
                $retval = count($b->orderItems) <=> count($a->orderItems);
                if ($retval === 0) {
                    $retval = $a->orderItems[0]->offer()->reference <=> $b->orderItems[0]->offer()->reference;
                    if ($retval === 0) {
                        $retval = $a->orderItems[0]->quantity <=> $b->orderItems[0]->quantity;
                        if ($retval === 0) {
                            $retval = $a->orderId <=> $b->orderId;
                        }
                    }
                }
                return $retval;
            });
        }
        return call_user_func_array('array_merge', $orders);
    }

    /**
     * @param $order
     * @return int
     */
    public function getPostNLProductCode($order, $BE_parcel_breakpoint_value = null): int
    {
        $PARCEL_CODE_BE = 4912;
        $LETTERBOX_PARCEL_CODE_BE = 6400;
        $PARCEL_CODE_NL = 3089;
        $PARCEL_CODE_NL_READER = 3085;
        $LETTERBOX_PARCEL_CODE_NL = 2928;

        if(env('POSTNL_LETTERBOX_PARCEL_BUSINESS')) {
            $LETTERBOX_PARCEL_CODE_NL = 2929;
        }

        if ($order->shipmentDetails->countryCode === "BE") {
            if($order->shipmentDetails->shipping_method_id === "TNT") {
                $productCode = $PARCEL_CODE_BE;
            }elseif ($order->grandTotalPrice("FBR") >= $BE_parcel_breakpoint_value) {
                $productCode = $PARCEL_CODE_BE;
            } else {
                $productCode = $LETTERBOX_PARCEL_CODE_BE;
            }
        } else {
            $orderOfferProductCode = $order->hasOfferShipmentProductCode();
            if ($order->grandTotalPrice("FBR") >= 100) {
                $productCode = $PARCEL_CODE_NL;
            } else if($orderOfferProductCode) {
                $productCode = $orderOfferProductCode;
            }else {
                $productCode = $LETTERBOX_PARCEL_CODE_NL;
            }
        }
        return $productCode;
    }

    public function formatCompanyNameForPostNL($company)
    {
        if($company) {
            $forbidden_words = ['picture', 'fotograaf', 'fotografie', 'foto', 'video', 'photo', 'film', 'camera', 'studio', 'media', 'audio', 'cine', 'beeld', 'geluid', 'productie'];
            $replacement_words = ['P.', 'F.', 'F.', 'F.', 'V.', 'Ph.', 'F.', 'C.', 'St.', 'M.', 'A.', 'C.', 'B.', 'G.', 'Pr.'];
            $company = str_ireplace($forbidden_words, $replacement_words, $company);
            $company = substr($company, 0, 32);
            return $company;
        }
        return "";
    }

    public function getShippingMethod(Order $order)
    {
        //The order has its own shipping method
        if (!is_null($order->shipmentDetails->shipping_method_id)) {
            $shipping_method = $order->shipmentDetails->shippingMethod;
        }
        //The order contains a product that has a different shipping method
        elseif ($order->hasShippingMethod($this->default_shipping_method, $this->offer_ids)) {
            $shipping_method = $order->hasShippingMethod($this->default_shipping_method, $this->offer_ids);
        }
        //The order has a totalOrderPrice above a shipping method breakpoint
        //The biggest breakpoint is chosen.
        elseif ($this->lowest_shipping_method_breakpoint && $order->totalOrderPrice >= $this->lowest_shipping_method_breakpoint->totalOrderPrice) {
            $shippingMethodBreakpoint = ShippingMethodBreakpoint
                ::where('totalOrderPrice', '<=', $order->totalOrderPrice)
                ->orderBy('totalOrderPrice', 'desc')
                ->first();
            $shipping_method = $shippingMethodBreakpoint->shippingMethod;
        }
        //If all of the above are false, use the default shipping method
        else {
            $shipping_method = $this->default_shipping_method;
        }

        return $shipping_method;
    }

    /**
     * This function is used in the automatic sync task
     * @param Order $order
     * @return BolShippingMethod
     */
    public function getOrderShippingMethod(Order $order)
    {
        //Calculate total order price of all FBR items in the order
        $totalOrderPriceFBR = $order->grandTotalPrice("FBR");

        //The order has an item with a different shipping method than default.
        //Ship everything in the order with this shipping method.
        if ($order->hasShippingMethod($this->default_shipping_method, $this->offer_ids)) {
            $shipping_method = $order->hasShippingMethod($this->default_shipping_method, $this->offer_ids);
        }

        //The order has a totalOrderPrice above a shipping method breakpoint
        //The biggest breakpoint is chosen.
        elseif ($this->lowest_shipping_method_breakpoint && $totalOrderPriceFBR >= $this->lowest_shipping_method_breakpoint->totalOrderPrice) {
            $shippingMethodBreakpoint = ShippingMethodBreakpoint
                ::where('totalOrderPrice', '<=', $totalOrderPriceFBR)
                ->orderBy('totalOrderPrice', 'desc')
                ->first();
            $shipping_method = $shippingMethodBreakpoint->shippingMethod;
        }
        //If all of the above are false, use the default shipping method
        else {
            $shipping_method = $this->default_shipping_method;
        }

        return $shipping_method;
    }

    public function setShippingMethodFields()
    {
        $this->default_shipping_method = BolShippingMethod::where('default', true)->first();
        $this->offers_with_shipping_method = Offer::whereNotNull('shipping_method_id')->get();
        $this->offer_ids = array_column($this->offers_with_shipping_method->toArray(), 'offerId');
        $this->lowest_shipping_method_breakpoint = ShippingMethodBreakpoint::orderBy('totalOrderPrice', 'asc')->first();
    }

    public function getBolShipmentReference($order, $now)
    {
        if (env('BOL_API_REGION') === "NL") {
            return $order->shipmentDetails->barcode . " | " . $now;
        } elseif (env('BOL_API_REGION') === "BE" && $order->totalOrderPrice >= $this->lowest_shipping_method_breakpoint->totalOrderPrice) {
            return $order->shipmentDetails->barcode . " | " . $now;
        } else {
            return $now;
        }
    }

    public function filterOrdersByRegionAndShippingMethod($orders, $region, $transporter_code)
    {
        if (env('BOL_API_REGION') === $region) {
            $default_shipping_method = BolShippingMethod::where('default', true)->first();
            if ($default_shipping_method->transporter_code !== $transporter_code) {
                $TNT_breakpoint = ShippingMethodBreakpoint::where('shipping_method_id', $transporter_code)->orderBy('totalOrderPrice', 'asc')->first();
                if ($TNT_breakpoint) {
                    $breakpoint_value = $TNT_breakpoint->totalOrderPrice;
                } else {
                    $breakpoint_value = 40;
                }
                $orders = $orders->where('totalOrderPrice', '>=', $breakpoint_value);
            }
        }
        return $orders;
    }

    public function get_BE_ParcelBreakpointValue()
    {
        $TNT_breakpoint = ShippingMethodBreakpoint::where('shipping_method_id', "TNT")->orderBy('totalOrderPrice', 'asc')->first();
        if ($TNT_breakpoint) {
            $breakpoint_value = $TNT_breakpoint->totalOrderPrice;
        } else {
            $breakpoint_value = 40;
        }

        return $breakpoint_value;
    }

    public function getPostNLShippingLabel($order)
    {
        $postNLShipment = $this->createPostNLShipmentForOrder($order);

        $label = $this->postnl->generateLabel(
            $postNLShipment,
            'GraphicFile|PDF',
            true
        );
        if(is_null($label)){
            return response()->json([
                'message' => 'Label for order '. $order->orderId . ' could not be made.',
                'error' => $order
            ])->setStatusCode(409);
        }

        return base64_decode($label->getResponseShipments()[0]->getLabels()[0]->getContent());
    }

    public function createPostNLShipmentForOrder($order)
    {
        $shipmentDetails = $order->shipmentDetails;
        $orderShipment = $order->shipments->last();
        if(is_null($orderShipment)) {
            OrderShipmentNotFound::dispatch($order);
            $order = $order->refresh();
            $orderShipment = $order->shipments->last();
        }
        $shipment = PostNLShipment::create([
            'Addresses' => [
                $this->createReceiverAddress($shipmentDetails)
            ],
            'Barcode' => $orderShipment->barcode,
            'Dimension' => (new Dimension('2000'))->setCurrentService('Shipping'),
            'ProductCodeDelivery' => $orderShipment->product_code,
            'Reference' => $orderShipment->reference,
            'DeliveryDate' => new \DateTime(((object)$order->orderItems[0]->fulfilment)->latestDeliveryDate),
        ])->setCurrentService('Shipping');

        return $shipment;
    }

    public function createPostNLReturnShipmentForOrder($order)
    {
        return PostNLShipment::create([
            'Addresses' => [
                $this->createReturnAddress($order),
                $this->createSenderAddress($order->shipmentDetails)
            ],
            'Barcode' => $this->postnl->generateBarcodeByCountryCode('NL'),
            'Dimension' => (new Dimension('2000'))->setCurrentService('Shipping'),
            'ProductCodeDelivery' => $order->productCodeDelivery,
            'Reference' => $order->orderId,
            'DeliveryDate' => new \DateTime('tomorrow'),
        ])->setCurrentService('Shipping');
    }

    private function createReceiverAddress($receiver) {
        return Address::create([
            'AddressType' => '01',
            'City' => $receiver->city,
            'CompanyName' => $this->formatCompanyNameForPostNL($receiver->company),
            'Countrycode' => $receiver->countryCode,
            'FirstName' => substr($receiver->firstName, 0, 32),
            'HouseNr' => $receiver->houseNumber,
            'HouseNrExt' => $receiver->houseNumberExtension,
            'Name' => substr($receiver->surName, 0, 32),
            'Street' => $receiver->streetName,
            'Zipcode' => $receiver->zipCode,
        ])->setCurrentService('Shipping');
    }

    private function createReturnAddress($order)
    {
        return Address::create([
            'AddressType' => '01',
            'City' => "ROTTERDAM",
            'CompanyName' => "DataIO retouren",
            'Countrycode' => "NL",
            'FirstName' => "t.a.v.",
            'HouseNr' => 747,
            'HouseNrExt' => null,
            'Name' => "Retournr ". $order->orderId,
            'Street' => "Postbus",
            'Zipcode' => "3000AS",
        ])->setCurrentService('Shipping');
    }

    private function createSenderAddress($sender)
    {
        return Address::create([
            'AddressType' => '02',
            'City' => $sender->city,
            'CompanyName' => $this->formatCompanyNameForPostNL($sender->company),
            'Countrycode' => $sender->countryCode,
            'FirstName' => substr($sender->firstName, 0, 32),
            'HouseNr' => $sender->houseNumber,
            'HouseNrExt' => $sender->houseNumberExtension,
            'Name' => substr($sender->surName, 0, 32),
            'Street' => $sender->streetName,
            'Zipcode' => $sender->zipCode,
        ])->setCurrentService('Shipping');
    }
}
