<?php


namespace App\Utils;


use App\Events\OrderShipmentNotFound;
use App\Order;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class SpreadsheetUtils
{
    private $offerUtils;
    private $jewelcaseOffers;

    public function __construct()
    {
        $this->offerUtils = new OfferUtils();
        $this->jewelcaseOffers = $this->offerUtils->getJewelcaseOffers();
    }

    public function prepareOrdersForSpreadsheet($orders)
    {
        $categorizedOrders = $this->getArrayWithOrderCategories();

        //split orders >= €100, >= €40, rest
        foreach ($orders as $index => $order) {
            $order->bolIndex = $index + 1; //used in spreadsheet for original bol index
            $categorizedOrders = $this->pre_sortOrders_by_category($categorizedOrders, $order);
            $order->addJewelcasesToOrderItems($this->jewelcaseOffers);
        }

        $orders = $this->sortOrders($categorizedOrders);
        return $orders;
    }

    private function pre_sortOrders_by_category($order_array, $order)
    {
        if ($order->hasReader() && $order->totalOrderPrice < 100) {
            $order_array['readers'][] = $order;
        } else if ($order->totalOrderPrice >= 100) {
            $order_array['above100'][] = $order;
        } else if ($order->totalOrderPrice >= 40) {
            $order_array['above40'][] = $order;
        } else {
            $order_array['others'][] = $order;
        }
        return $order_array;
    }

    private function getArrayWithOrderCategories(): array
    {
        return $orders_array = [
            'readers' => [],
            'above100' => [],
            'above40' => [],
            'others' => [],
        ];
    }

    public function sortOrders($orders)
    {
        //sort orders >= €100 by numOrderItems > totalOrderPrice(asc)
        if (!empty($orders['above100'])) {
            usort($orders['above100'], function ($a, $b) {
                $retval = count($b->orderItems) <=> count($a->orderItems);
                if ($retval === 0) {
                    $retval = $a->totalOrderPrice <=> $b->totalOrderPrice;
                    if ($retval === 0) {
                        $retval = $a->orderId <=> $b->orderId;
                    }
                }
                return $retval;
            });
        }
        //sort orders >= €40 by numOrderItems > totalOrderPrice(asc)
        if (!empty($orders['above40'])) {
            usort($orders['above40'], function ($a, $b) {
                $retval = count($b->orderItems) <=> count($a->orderItems);
                if ($retval === 0) {
                    $retval = $a->totalOrderPrice <=> $b->totalOrderPrice;
                    if ($retval === 0) {
                        $retval = $a->orderId <=> $b->orderId;
                    }
                }
                return $retval;
            });
        }
        //sort rest orders by numOrderItems > orderItem[0]->offer->reference > orderItem[0]->quantity
        if (!empty($orders['others'])) {
            usort($orders['others'], function ($a, $b) {
                $retval = count($b->orderItems) <=> count($a->orderItems);
                if ($retval === 0) {
                    $retval = $a->orderItems[0]->offer()->reference <=> $b->orderItems[0]->offer()->reference;
                    if ($retval === 0) {
                        $retval = $a->orderItems[0]->quantity <=> $b->orderItems[0]->quantity;
                        if ($retval === 0) {
                            $retval = $a->orderId <=> $b->orderId;
                        }
                    }
                }
                return $retval;
            });
        }
        return call_user_func_array('array_merge', $orders);
    }

    public function formatHouseNumberExtension($string)
    {
        if ($string == "") {
            return $string;
        }
        $string = preg_replace('/[^A-Za-z0-9\\\\\/]/', '', $string); // Removes special chars.
        $string = ' - ' . $string;
        $string = strtoupper($string);
        return $string;
    }

    public function createPostNLSheets($orders, $spreadsheet)
    {
        $chunk_orders = $this->getPostNLChunks($orders);
        $postNL_headers = [
            'YourReference', 'CompanyName', 'Surname', 'FirstName',
            'CountryCode', 'Street', 'HouseNo', 'HouseNoSuffix', 'Postcode',
            'City', 'Email', 'MobileNumber', 'ProductCode',
            'CODAmount', 'CODReference', 'InsuredValue'
        ];
        foreach ($chunk_orders as $chunk_index => $chunk) {
            $spreadsheet->createSheet();
            $sheet = $spreadsheet->getSheet($chunk_index + 1);
            $sheet->setTitle('PostNL chunk ' . ($chunk_index + 1));
            foreach ($postNL_headers as $header_index => $data) {
                $sheet->setCellValueByColumnAndRow($header_index + 1, 1, $data);
            }
            $row = 2;
            foreach ($chunk as $order_index => $order) {
                if(is_null($order->shipments->last())) {
                    OrderShipmentNotFound::dispatch($order);
                    $order = $order->refresh();
                }
                $sheet->setCellValueByColumnAndRow(1, $row, $order->orderId);
                $sheet->setCellValueByColumnAndRow(2, $row, substr($order->shipmentDetails->company, 0, 31));
                $sheet->setCellValueByColumnAndRow(3, $row, substr($order->shipmentDetails->surName, 0, 31));
                $sheet->setCellValueByColumnAndRow(4, $row, substr($order->shipmentDetails->firstName, 0, 31));
                $sheet->setCellValueByColumnAndRow(5, $row, $order->shipmentDetails->countryCode);
                $sheet->setCellValueByColumnAndRow(6, $row, $order->shipmentDetails->streetName);
                $sheet->setCellValueByColumnAndRow(7, $row, $order->shipmentDetails->houseNumber);
                $sheet->setCellValueByColumnAndRow(8, $row, (new SpreadsheetUtils())->formatHouseNumberExtension($order->shipmentDetails->houseNumberExtension));
                $sheet->setCellValueByColumnAndRow(9, $row, $order->shipmentDetails->zipCode);
                $sheet->setCellValueByColumnAndRow(10, $row, $order->shipmentDetails->city);
                $sheet->setCellValueByColumnAndRow(13, $row, $order->shipments->last()->product_code);

                $row++;
            }
        }

        return $spreadsheet;
    }

    /**
     * @param $orders
     * @return array
     */
    protected function getPostNLChunks($orders): array
    {
        $chunk_orders = [];

        //Chunk all order for NL
        if (env("BOL_API_REGION") === "NL") {
            $chunk_orders = array_chunk($orders, 100);
        } //Only chunk parcels for Belgium (€40 and above)
        elseif (env("BOL_API_REGION") === "BE") {
            $parcels = [];
            foreach ($orders as $order) {
                if ($order->totalOrderPrice >= 40 || $order->hasReader()) {
                    $parcels[] = $order;
                }
            }
            $chunk_orders = array_chunk($parcels, 100);
        }
        return $chunk_orders;
    }

    /**
     * @param $order
     * @return int
     */
    protected function getPostNLProductCode($order): int
    {
        $PARCEL_CODE_BE = 4944;
        $PARCEL_CODE_NL = 3089;
        $PARCEL_CODE_NL_READER = 3085;
        $LETTERBOX_PARCEL_CODE_NL = 2928;

        if (env("BOL_API_REGION") === "BE") {
            $productCode = $PARCEL_CODE_BE;
        } else {
            if ($order->totalOrderPrice >= 100) {
                $productCode = $PARCEL_CODE_NL;
            } else if ($order->hasReader()) {
                $productCode = $PARCEL_CODE_NL_READER;
            } else {
                $productCode = $LETTERBOX_PARCEL_CODE_NL;
            }
        }
        return $productCode;
    }

    /**
     * Styles the Header Row for the sheet pink-ish with outer borders:thin
     * @param $sheet
     * @param $lastHeaderIndex
     * @return mixed
     */
    public function setHeaderStyles($sheet, $lastHeaderIndex)
    {
        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [
                        'rgb' => '000000',
                    ],
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'rotation' => 0,
                'color' => [
                    'rgb' => 'f0b6c7'
                ],
            ],
        ];
        $sheet->getStyle('A1:'.$lastHeaderIndex.'1')->applyFromArray($styleArray);

        return $sheet;
    }

    /**
     * Styles the Order Row
     * Above €100: orange
     * Reader: brown
     * Above €40: yellow
     * Multiple OrderItems = font red and bold
     *
     * @param $sheet
     * @param $order
     * @param $row
     * @param $lastHeaderIndex
     */
    public function setOrderRowStyles($sheet, $order, $row, $lastHeaderIndex)
    {
        //Set Cell Styles
        $sheet->getStyle('A1:'.$lastHeaderIndex.'1')
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_TEXT);

        if($order->totalOrderPrice >= 100) {
            $sheet->getStyle('A'.$row.':'.$lastHeaderIndex.$row)
                ->getFill()
                ->setFillType(Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('FF9100'); //orange
        } else if($order->hasReader()) {
            $sheet->getStyle('A'.$row.':'.$lastHeaderIndex.$row)
                ->getFill()
                ->setFillType(Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('d4bc96'); //light-brown
        } else if ($order->totalOrderPrice >= 40) {
            $sheet->getStyle('A'.$row.':'.$lastHeaderIndex.$row)
                ->getFill()
                ->setFillType(Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('FFFF00'); //yellow
        }
        if(count($order->orderItems) >= 2) {
            $sheet->getStyle('A'.$row.':'.$lastHeaderIndex.$row)
                ->getFont()
                ->setBold(true)
                ->getColor()
                ->setRGB('FF0000'); //red
        }
    }
}
