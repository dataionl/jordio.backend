<?php


namespace App\Utils;

use App\BolShippingMethod;
use App\Events\OrderCreated;
use App\Http\BolRetailerApi\BolClient;
use App\Http\BolRetailerApi\DBHelper;
use App\Offer;
use App\Order;
use App\OrderFile;
use App\ShippingMethodBreakpoint;
use Carbon\Carbon;
use Firstred\PostNL\Entity\Label;
use Firstred\PostNL\PostNL;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;
use Jurosh\PDFMerge\PDFMerger;

class TaskUtils
{
    /**
     * @var BolClient
     */
    private $client;

    /**
     * @var PDFUtils
     */
    private $pdfClient;

    /**
     * @var ShipmentUtils
     */
    private $shipmentUtils;

    /**
     * @var FileUtils
     */
    private $fileUtils;

    public function __construct()
    {
        $this->client = new BolClient();
        $this->client->setDemoMode(env('BOL_API_DEMO_MODE'));
        $this->pdfClient = new PDFUtils();
    }

    public function syncOrders(?bool $fullSync = false, ?string $fulfilmentMethod = "FBR"): Builder
    {
        $reducedOrders = $fullSync === false ? $this->getNewReducedOrders($fulfilmentMethod) : $this->getReducedOrders($fulfilmentMethod);

        $detailedOrders = $this->getOrdersDetails($reducedOrders);
        $orderItems = $this->getOrderItems($detailedOrders);
        $this->storeOrdersWithItems($detailedOrders, $orderItems);

        $orders = Order::whereIn('orderId', array_column($detailedOrders, 'orderId'));
        $orders->each(function($order){
            OrderCreated::dispatch($order);
        });

        return $orders;
    }

    private function getReducedOrders(?string $fulfilmentMethod = "FBR"): array
    {
        $reducedOrders = [];
        $page = 1;
        $continue = true;
        while ($continue) {
            $orders = $this->client->getOrders($page, $fulfilmentMethod);
            $reducedOrders = array_merge($reducedOrders, $orders);
            $page++;
            if (empty($orders) || $this->client->isDemoMode()) {
                $continue = false;
            }
        }

        return $reducedOrders;
    }

    private function getNewReducedOrders(?string $fulfilmentMethod = "FBR"): array
    {
        $reducedOrders = [];
        $page = 1;
        $continue = true;
        while ($continue) {
            $bol_orders = $this->client->getOrders($page, $fulfilmentMethod);
            if (!empty($bol_orders)) {
                $bol_orders_ids = array_column($bol_orders, 'orderId');
                $known_orders = Order::whereIn('orderId', $bol_orders_ids)->get()->toArray();
                $known_orders_ids = array_column($known_orders, 'orderId');

                if ($known_orders_ids < $bol_orders_ids) {
                    foreach ($bol_orders as $order) {
                        if (!in_array($order->orderId, $known_orders_ids)) {
                            $reducedOrders[] = $order;
                        }
                    }
                    $page++;
                } else {
                    $continue = false;
                }
            }
            if (empty($bol_orders) || $this->client->isDemoMode()) {
                $continue = false;
            }
        }

        return $reducedOrders;
    }

    private function getOrdersDetails(array $newReducedOrders): array
    {
        $newOrders = [];
        foreach ($newReducedOrders as $o) {
            $order = $this->client->getOrder($o->orderId);
            //check if the order is found
            if (!empty($order)) {
                if($this->client->isDemoMode() && count($order->orderItems) > 1) {
                    $order->orderItems[0]->fulfilment->method = "FBB";
                }
                $order->totalOrderPrice = OrderUtils::getTotalOrderPrice($order);
                $newOrders[] = $order;
            }
        }

        return $newOrders;
    }

    private function getOrderItems(array $newOrders): array
    {
        $newOrderItems = [];
        foreach ($newOrders as $order) {
            foreach ($order->orderItems as $orderItem) {
                $orderItem->orderId = $order->orderId;
                $newOrderItems[] = $orderItem;
            }
        }

        return $newOrderItems;
    }

    private function storeOrdersWithItems(array $newOrders, array $newOrderItems)
    {
        $order_chunks = array_chunk($newOrders, 100);
        foreach ($order_chunks as $chunk) {
            DBHelper::storeOrdersUpsert($chunk);
            if (count($order_chunks) > 1) {
                sleep(3); //so we don't exceed PostNL API Limits
            }
        }
        DBHelper::storeOrderItemsUpsert($newOrderItems);
    }

    /**
     * @param Builder $orders orders
     * @param array $categories categories
     */
    public function generateOrderFiles(Builder $orders, array $categories)
    {
        $this->fileUtils = new FileUtils();
        foreach ($categories as $category) {
            switch ($category) {
                case "shipping_labels":
                    $this->fileUtils->generatePostNLShipmentsAndLabels($orders);
                    break;
                case "invoices":
                    $this->fileUtils->generateInvoices($orders);
                    break;
                case "packing_slips":
                    $this->fileUtils->generatePackingSlips($orders);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @todo: Deprecated
     * @param $orders
     * @return mixed
     */
    private function prepareShippingInformation($orders)
    {
        $this->shipmentUtils = new ShipmentUtils();
        $this->shipmentUtils->setShippingMethodFields();

        //Needed for BE productcode
        $BE_parcel_breakpoint_value = env('BOL_API_REGION') === "BE" ? $this->shipmentUtils->get_BE_ParcelBreakpointValue() : null;

        foreach ($orders->with('shipmentDetails')->get() as $order) {
            $shipping_method = $this->shipmentUtils->getOrderShippingMethod($order);
            $order->shipmentDetails->shipping_method_id = $shipping_method->transporter_code;

            $shipment_productcode = $this->shipmentUtils->getPostNLProductCode($order, $BE_parcel_breakpoint_value);
            $order->shipmentDetails->shipment_productcode = $shipment_productcode;

            $order->shipmentDetails->saveQuietly();
        }

        return $orders;
    }
}
