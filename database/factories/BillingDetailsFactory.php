<?php

namespace Database\Factories;

use App\BillingDetails;
use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Factories\Factory;

class BillingDetailsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BillingDetails::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'orderId' => substr($this->faker->uuid(), 0, 8),
            'salutation' => "MALE",
            'firstName' => $this->faker->firstName(),
            'surName' => $this->faker->lastName(),
            'streetName' => $this->faker->streetName(),
            'houseNumber' => $this->faker->buildingNumber(),
            'houseNumberExtension' => $this->faker->buildingNumber(),
            'extraAddressInformation' => null,
            'zipCode' => $this->faker->postcode(),
            'city' => $this->faker->city(),
            'countryCode' => $this->faker->countryCode(),
            'email' => $this->faker->email(),
            'company' => $this->faker->company(),
            'vatNumber' => $this->faker->numberBetween(1, 1000000),
            'kvkNumber' => $this->faker->numberBetween(1, 1000000),
            'orderReference' => $this->faker->word,
        ];
    }
}
