<?php

namespace Database\Factories;

use App\Order;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'orderId' => substr($this->faker->uuid(), 0, 8),
            'status' => "OPEN",
            'pickUpPoint' => 0,
            'orderPlacedDateTime' => $this->faker->dateTimebetween('-30 days', '+0 days'),
            'totalOrderPrice' => $this->faker->randomFloat(2, 0, 300),
        ];
    }
}
