<?php

namespace Database\Factories;

use App\OrderItem;
use DateTime;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'orderItemId' => substr($this->faker->uuid(), 0, 8),
            'orderId' => substr($this->faker->uuid(), 0, 8),
            'cancellationRequest' => false,
            'fulfilment' => (object)[
                "method" => "FBR",
                "expiryDate" => "2020-01-09",
                "distributionParty" => "RETAILER",
                "exactDeliveryDate" => "2020-01-08",
                "latestDeliveryDate" => (new DateTime('now'))->modify('+1 day')->format('Y-m-d')
            ],
            'offer' => (object)[
                "offerId"=> "8f6283e3-de98-c92f-e053-3598790a63b5",
                "reference"=> "TESTITEM",
            ],
            'product' => (object)[
                "ean" => "88888888888",
                "title" => "My Little Pony dekbed"
            ],
            'quantity' => $this->faker->randomNumber(1),
            'unitPrice' => $this->faker->randomFloat(2,0,50),
            'commission' => $this->faker->randomFloat(2,0,5),
            'additionalServices' => json_encode([])
        ];
    }
}
