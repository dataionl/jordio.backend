<?php

namespace Database\Factories;

use App\ShipmentDetails;
use Illuminate\Database\Eloquent\Factories\Factory;

class ShipmentDetailsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ShipmentDetails::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'orderId' => substr($this->faker->uuid(), 0, 8),
            'pickUpPointName' => null,
            'salutation' => "MALE",
            'firstName' => $this->faker->firstName(),
            'surName' => $this->faker->lastName(),
            'streetName' => $this->faker->streetName(),
            'houseNumber' => $this->faker->numberBetween(1,200),
            'houseNumberExtension' => $this->faker->randomLetter(),
            'extraAddressInformation' => null,
            'zipCode' => $this->faker->postcode(),
            'city' => $this->faker->city(),
            'countryCode' => env('BOL_API_REGION'),
            'email' => $this->faker->email(),
            'company' => $this->faker->company(),
            'deliveryPhoneNumber' => $this->faker->phoneNumber(),
            'language' => $this->faker->languageCode(),
        ];
    }
}
