<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->string('orderId', 30)->primary();
            $table->string('status')->default('OPEN');
            $table->boolean('pickUpPoint')->nullable();
            $table->dateTime('orderPlacedDateTime');
            $table->float('totalOrderPrice');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('shipment_details', function (Blueprint $table) {
            $table->id();
            $table->string('orderId', 30)->unique();
            $table->string('pickUpPointName')->nullable();
            $table->string('salutation')->nullable();
            $table->string('firstName')->nullable();
            $table->string('surName')->nullable();
            $table->string('streetName')->nullable();
            $table->string('houseNumber')->nullable();
            $table->string('houseNumberExtension')->nullable();
            $table->string('extraAddressInformation')->nullable();
            $table->string('zipCode')->nullable();
            $table->string('city')->nullable();
            $table->string('countryCode')->nullable();
            $table->string('email')->nullable();
            $table->string('company')->nullable();
            $table->string('deliveryPhoneNumber')->nullable();
            $table->string('language')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('orderId')->references('orderId')->on('orders')->cascadeOnDelete();
        });

        Schema::create('billing_details', function (Blueprint $table) {
            $table->id();
            $table->string('orderId', 30)->unique();
            $table->string('salutation')->nullable();
            $table->string('firstName')->nullable();
            $table->string('surName')->nullable();
            $table->string('streetName')->nullable();
            $table->string('houseNumber')->nullable();
            $table->string('houseNumberExtension')->nullable();
            $table->string('extraAddressInformation')->nullable();
            $table->string('zipCode')->nullable();
            $table->string('city')->nullable();
            $table->string('countryCode')->nullable();
            $table->string('email')->nullable();
            $table->string('company')->nullable();
            $table->string('vatNumber')->nullable();
            $table->string('kvkNumber')->nullable();
            $table->string('orderReference')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('orderId')->references('orderId')->on('orders')->cascadeOnDelete();
        });

        Schema::create('order_items', function (Blueprint $table) {
            $table->string('orderItemId', 30)->primary();
            $table->string('orderId', 30)->nullable();
            $table->boolean('cancellationRequest')->nullable();
            $table->json('fulfilment')->nullable();
            $table->json('offer')->nullable();
            $table->json('product')->nullable();
            $table->smallInteger('quantity')->nullable();
            $table->float('unitPrice')->nullable();
            $table->float('commission')->nullable();
            $table->json('additionalServices')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('orderId')->references('orderId')->on('orders')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment_details');
        Schema::dropIfExists('billing_details');
        Schema::dropIfExists('order_items');
        Schema::dropIfExists('orders');
    }
}
