<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->string('offerId')->primary();
            $table->string('ean')->nullable();
            $table->string('reference')->nullable();
            $table->boolean('onHoldByRetailer')->nullable();
            $table->string('unknownProductTitle')->nullable();
            $table->json('pricing');
            $table->json('stock');
            $table->json('fulfilment');
            $table->json('store');
            $table->json('condition');
            $table->json('notPublishableReasons');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
