<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJewelcaseToOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            if(!Schema::hasColumn('offers', 'jewelcase')) {
                $table->boolean('jewelcase')->default(false)->after('reference');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            if(Schema::hasColumn('offers', 'jewelcase')) {
                $table->dropColumn('jewelcase');
            }
        });
    }
}
