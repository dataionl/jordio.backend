<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPostnlBarcodeToShipmentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipment_details', function (Blueprint $table) {
            if(!Schema::hasColumn('shipment_details', 'barcode')) {
                $table->string('barcode', 50)->nullable()->after('orderId');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipment_details', function (Blueprint $table) {
            if(Schema::hasColumn('shipment_details', 'barcode')) {
                $table->dropColumn('barcode');
            }
        });
    }
}
