<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class CreateBolShippingMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bol_shipping_methods', function (Blueprint $table) {
            $table->string('transporter_code', 30)->primary();
            $table->string('transporter_name')->default("")->nullable();
            $table->boolean('track_and_trace')->default(false);
            $table->boolean('default')->default(false);
            $table->timestamps();
        });

        Artisan::call('db:seed', [
            '--class' => 'BolShippingMethodSeeder'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bol_shipping_methods');
    }
}
