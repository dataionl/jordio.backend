<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShippingMethodIdColumnToShipmentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipment_details', function (Blueprint $table) {
            if (!Schema::hasColumn('shipment_details', 'shipping_method_id')) {
                $table->string('shipping_method_id', 50)->nullable()->after('barcode');
                $table->foreign('shipping_method_id')
                    ->references('transporter_code')
                    ->on('bol_shipping_methods')
                    ->nullOnDelete();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipment_details', function (Blueprint $table) {
            if (Schema::hasColumn('shipment_details', 'shipping_method_id')) {
                $table->dropForeign(['shipping_method_id']);
                $table->dropColumn('shipping_method_id');
            }
        });
    }
}
