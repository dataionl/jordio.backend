<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingMethodBreakpointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_method_breakpoints', function (Blueprint $table) {
            $table->id();
            $table->float('totalOrderPrice');
            $table->string('shipping_method_id')->nullable();
            $table->foreign('shipping_method_id')
                ->references('transporter_code')
                ->on('bol_shipping_methods')
                ->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_method_breakpoints', function (Blueprint $table) {
            if (Schema::hasColumn('shipping_method_breakpoints', 'shipping_method_id')) {
                $table->dropForeign(['shipping_method_id']);
                $table->dropColumn('shipping_method_id');
            }
        });
        Schema::dropIfExists('shipping_method_breakpoints');
    }
}
