<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_files', function (Blueprint $table) {
            $table->id();
            $table->string('orderId', 30);
            $table->foreign('orderId')
                ->references('orderId')
                ->on('orders')
                ->cascadeOnDelete();
            $table->string('extension');
            $table->string('fileName');
            $table->string('filePath');
            $table->string('category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_files', function(Blueprint $table) {
            $table->dropForeign(['orderId']);
        });
        Schema::dropIfExists('order_files');
    }
}
