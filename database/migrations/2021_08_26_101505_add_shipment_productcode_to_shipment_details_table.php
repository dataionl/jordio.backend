<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShipmentProductcodeToShipmentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipment_details', function (Blueprint $table) {
            if(!Schema::hasColumn('shipment_details', 'shipment_productcode')) {
                $table->string('shipment_productcode', 50)->nullable()->after('shipping_method_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipment_details', function (Blueprint $table) {
            if(Schema::hasColumn('shipment_details', 'shipment_productcode')) {
                $table->dropColumn('shipment_productcode');
            }
        });
    }
}
