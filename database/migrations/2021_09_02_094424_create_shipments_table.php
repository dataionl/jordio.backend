<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->id();
            $table->string('transporter_code')->nullable();
            $table->string('product_code')->nullable();
            $table->string('barcode')->nullable();
            $table->string('reference')->nullable();
            $table->string('orderId')->nullable();
            $table->boolean('return')->default(false);
            $table->timestamps();

            $table->foreign('transporter_code')
                ->references('transporter_code')
                ->on('bol_shipping_methods')
                ->nullOnDelete();

            $table->foreign('orderId')
                ->references('orderId')
                ->on('orders')
                ->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipments', function (Blueprint $table) {
            $table->dropForeign(['transporter_code']);
        });
        Schema::dropIfExists('shipments');
    }
}
