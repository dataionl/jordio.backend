<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBolShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bol_shipments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shipment_id')->nullable()->constrained()->nullOnDelete();
            $table->string('orderId')->nullable();
            $table->string('orderItemId')->nullable();
            $table->string('status')->default("QUEUED")->nullable();
            $table->timestamps();

            $table->foreign('orderId')
                ->references('orderId')
                ->on('orders')
                ->nullOnDelete();

            $table->foreign('orderItemId')
                ->references('orderItemId')
                ->on('order_items')
                ->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bol_shipments', function (Blueprint $table) {
            $table->dropForeign(['shipment_id']);
        });
        Schema::dropIfExists('bol_shipments');
    }
}
