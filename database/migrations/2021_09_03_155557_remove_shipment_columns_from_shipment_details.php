<?php

use App\Shipment;
use App\ShipmentDetails;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveShipmentColumnsFromShipmentDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ShipmentDetails::chunk(1000, function($shipmentDetailsChunk){
                Shipment::upsert(collect($shipmentDetailsChunk)->map(function($s) {
                    $s = (object) $s;
                    return [
                        'transporter_code' => $s->shipping_method_id,
                        'barcode' => $s->barcode,
                        'reference' => $s->orderId,
                        'orderId' => $s->orderId,
                    ];
                })->toArray(), ['orderId', 'barcode']);
        });

        Schema::table('shipment_details', function (Blueprint $table) {
            if(Schema::hasColumn('shipment_details', 'shipping_method_id')) {
                $table->dropForeign(['shipping_method_id']);
                $table->dropColumn('shipping_method_id');
            }
            if(Schema::hasColumn('shipment_details', 'barcode')) {
                $table->dropColumn('barcode');
            }
            if(Schema::hasColumn('shipment_details', 'shipment_productcode')) {
                $table->dropColumn('shipment_productcode');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipment_details', function (Blueprint $table) {
            if(!Schema::hasColumn('shipment_details', 'barcode')) {
                $table->string('barcode', 50)->nullable()->after('orderId');
            }
            if (!Schema::hasColumn('shipment_details', 'shipping_method_id')) {
                $table->string('shipping_method_id', 50)->nullable()->after('barcode');
                $table->foreign('shipping_method_id')
                    ->references('transporter_code')
                    ->on('bol_shipping_methods')
                    ->nullOnDelete();
            }
            if(!Schema::hasColumn('shipment_details', 'shipment_productcode')) {
                $table->string('shipment_productcode', 50)->nullable()->after('shipping_method_id');
            }
        });

        Shipment::chunk(1000, function($shipmentsChunk){
            ShipmentDetails::upsert(collect($shipmentsChunk)->map(function($s) {
                $s = (object) $s;
                return [
                    'barcode' => $s->barcode,
                    'shipping_method_id' => $s->transporter_code,
                    'orderId' => $s->orderId,
                    'shipment_productcode' => $s->product_code
                ];
            })->toArray(), ['orderId']);
        });
    }
}
