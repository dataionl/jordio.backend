<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GiveOrderFilesColumnsADefaultValueOrNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_files', function (Blueprint $table) {
            if(Schema::hasColumn('order_files', 'extension')) {
                $table->string('fileName')->nullable()->change();
            }
            if(Schema::hasColumn('order_files', 'extension')) {
                $table->string('filePath')->nullable()->change();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_files', function (Blueprint $table) {
            if(Schema::hasColumn('order_files', 'extension')) {
                $table->string('fileName')->nullable(false)->change();
            }
            if(Schema::hasColumn('order_files', 'extension')) {
                $table->string('filePath')->nullable(false)->change();
            }
        });
    }
}
