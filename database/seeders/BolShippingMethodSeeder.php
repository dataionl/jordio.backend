<?php

namespace Database\Seeders;

use App\BolShippingMethod;
use Illuminate\Database\Seeder;

class BolShippingMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BolShippingMethod::insert([
            ['transporter_name'=> 'PostNL', 'transporter_code' => 'TNT',           'track_and_trace' => true, 'default' => true],
            ['transporter_name'=> 'PostNL Briefpost', 'transporter_code' => 'TNT_BRIEF',     'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'PostNL extra@home', 'transporter_code' => 'TNT-EXTRA',     'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'UPS', 'transporter_code' => 'UPS',           'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'TNT Express', 'transporter_code' => 'TNT-EXPRESS',   'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'Dynalogic','transporter_code' => 'DYL',           'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'DPD Nederland','transporter_code' => 'DPD-NL',        'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'DPD België','transporter_code' => 'DPD-BE',        'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'Bpost België','transporter_code' => 'BPOST_BE',      'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'Bpost Briefpost','transporter_code' => 'BPOST_BRIEF',   'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'DHLFORYOU','transporter_code' => 'DHLFORYOU',     'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'GLS','transporter_code' => 'GLS',           'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'FedEx Nederland','transporter_code' => 'FEDEX_NL',      'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'FedEx Belgie','transporter_code' => 'FEDEX_BE',      'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'Anders','transporter_code' => 'OTHER',         'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'DHL','transporter_code' => 'DHL',           'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'DHL Germany','transporter_code' => 'DHL_DE',        'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'DHL Global mail','transporter_code' => 'DHL-GLOBAL-MAIL', 'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'Transportservice Nederland','transporter_code' => 'TSN',           'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'Fiege','transporter_code' => 'FIEGE',         'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'TransMission','transporter_code' => 'TRANSMISSION',  'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'Parcel.nl','transporter_code' => 'PARCEL-NL',     'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'LogoiX','transporter_code' => 'LOGOIX',        'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'Packs','transporter_code' => 'PACKS',         'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'Bezorgafspraak','transporter_code' => 'COURIER',       'track_and_trace' => false, 'default' => false],
            ['transporter_name'=> 'NedWRK (Red je pakketje)','transporter_code' => 'RJP',           'track_and_trace' => false, 'default' => false],
        ]);
    }
}
