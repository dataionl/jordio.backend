<?php

namespace Database\Seeders;

use App\BillingDetails;
use App\Order;
use App\OrderItem;
use App\ShipmentDetails;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        Order::factory()
            ->count(50)
            ->has(ShipmentDetails::factory()->count(1))
            ->has(BillingDetails::factory()->count(1))
            ->has(OrderItem::factory()->count(rand(1,2)))
            ->create();
    }
}
