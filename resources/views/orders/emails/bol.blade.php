<body class="bol-body"
      style=" margin: 0 !important;
padding: 0 !important;
background: #ffffff;">
<div style="display: none;
font-size: 1px;
color: #fefefe;
line-height: 1px;
font-family: Arial, sans-serif;
max-height: 0px;
max-width: 0px;
opacity: 0;
overflow: hidden;">
    We hebben jouw pakket zojuist verzonden. Binnenkort worden je bestelde artikel(en) bezorgd.
</div>
<table border="0"
       cellpadding="0"
       cellspacing="0"
       width="100%"
       class="bol-outer-wrapper">
    <tr>
        <td bgcolor="#0000a4"
            align="center"
            class="bol-header"
            style="background-color: #0000a4;
padding: 0;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                <tr>
                    <td align="center" valign="top" width="700">
            <![endif]-->
            <table width="100%"
                   border="0"
                   cellspacing="0"
                   cellpadding="0"
                   align="center"
                   class="bol-wrapper"
                   style="max-width: 700px;">
                <tr>
                    <td height="56"
                        width="100%"
                        align="left"
                        valign="top"
                        class="bol-horizontal-spacing"
                        style="padding: 0 24px;">
                        <a href="https://www.bol.com?language=nl"
                           target="_blank">
                            <img src="https://s.s-bol.com/nl/upload/images/nb/servicemails/assets/v2/logo-bol-white.gif"
                                 width="125"
                                 height="56"
                                 border="0"
                                 alt="bol.com"
                                 style="font-family: 'Arial Black', Arial, Helvetica, sans-serif;
font-size: 30px;
line-height: 30px;
font-weight: bold;
color: #ffffff;
display: block;">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>


    <!-- start main content -->
    <tr>
        <td bgcolor="#ffffff"
            align="center"
            class="bol-row"
            style="padding: 16px 0;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                <tr>
                    <td align="center" valign="top" width="700">
            <![endif]-->
            <table width="100%"
                   border="0"
                   cellspacing="0"
                   cellpadding="0"
                   align="center"
                   class="bol-wrapper"
                   style="max-width: 700px;">
                <tr>
                    <td align="center"
                        class="bol-splitter bol-color-04"
                        style=" width: 100%;
font-size: 0;
background-color: #ffd2ad;">
                        <!--[if (gte mso 9)|(IE)]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                            <tr>
                                <td valign="top" width="444">
                        <![endif]-->
                        <div class="bol-two-third"
                             style=" display: inline-block;
margin: 0 -2px;
vertical-align: top;
width: 100%;
max-width: 444px;">
                            <table border="0"
                                   cellpadding="0"
                                   cellspacing="0"
                                   width="100%">
                                <tr>
                                    <td class="bol-top-banner-left"
                                        style="padding: 24px;">
                                        <table cellpadding="0"
                                               cellspacing="0"
                                               border="0"
                                               width="100%">
                                            <tr>
                                                <td class="bol-heading-1"
                                                    style=" font-family: 'Arial Black', 'Open Sans', Arial, 'Roboto', sans-serif;
font-weight: 900;
letter-spacing: -2px;
text-align: left;
font-size: 30px;
line-height: 32px;
color: #0000a4;
padding-bottom: 10px;">
                                                    Hallo Jordi,
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bol-intro"
                                                    style=" font-family: Arial, 'sans-serif';
letter-spacing: -1px;
text-align: left;
font-size: 28px;
line-height: 36px;
color: #0000a4;">

                                                    Je bestelde artikel is meegegeven aan de bezorgdienst.

                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="bol-button-container"
                                                    style="padding-top: 10px;">
                                                    <table border="0"
                                                           cellspacing="0"
                                                           cellpadding="0">
                                                        <tr>
                                                            <td align="center"
                                                                class="bol-button"
                                                                style="font-family: Arial, sans-serif;
font-size: 16px;
padding: 15px 24px;
background-color: #0000ff;">
                                                                <a href="
https://postnl.nl/tracktrace/?B=3SNOQJ166513530&P=3191BM&D=NL&T=C&L=NL
"
                                                                   target="_blank"
                                                                   class="bol-button-text"
                                                                   style="color:#ffffff;
text-decoration:none;
display:block;">
                                                                    Volg je pakket
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        <td valign="top" width="256">
                        <![endif]-->
                        <div class="bol-one-third"
                             style=" display: inline-block;
margin: 0 -2px;
vertical-align: top;
width: 100%;
max-width: 256px;">
                            <table align="center"
                                   border="0"
                                   cellpadding="0"
                                   cellspacing="0"
                                   width="100%">
                                <tr>
                                    <td class="bol-top-banner-right"
                                        style="padding: 24px">
                                        <table cellpadding="0"
                                               cellspacing="0"
                                               border="0"
                                               width="100%">
                                            <tr>
                                                <td align="center">

                                                    <img
                                                        src="https://s.s-bol.com/nl/upload/images/nb/servicemails/assets/v2/van/color-04/van-box.gif"
                                                        width="208"
                                                        height="208"
                                                        border="0"
                                                        alt="Illustratie bestelling meegegeven aan bezorger"
                                                        style=" font-family: sans-serif;
font-size: 12px;
text-align: center;
color: #0000a4;">

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff"
            align="center"
            class="bol-row"
            style="padding: 16px 0;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                <tr>
                    <td align="center" valign="top" width="700">
            <![endif]-->
            <table width="100%"
                   border="0"
                   cellspacing="0"
                   cellpadding="0"
                   align="center"
                   class="bol-wrapper"
                   style="max-width: 700px;">
                <tr>
                    <td width="100%"
                        align="left"
                        valign="top"
                        class="bol-horizontal-spacing"
                        style="padding: 0 24px;">
                        <table width="100%"
                               border="0"
                               cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td class="bol-large"
                                    style=" font-family: Arial, 'sans-serif';
text-align: left;
font-size: 18px;
line-height: 23px;
color: #0a0a0a;">


                                    We sturen je een e-mail als de bezorger begonnen is aan zijn bezorgronde.


                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff"
            align="center"
            class="bol-row"
            style="padding: 16px 0;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                <tr>
                    <td align="center" valign="top" width="700">
            <![endif]-->
            <table width="100%"
                   border="0"
                   cellspacing="0"
                   cellpadding="0"
                   align="center"
                   class="bol-wrapper"
                   style="max-width: 700px;">
                <tr>
                    <td width="100%"
                        align="left"
                        valign="top"
                        class="bol-horizontal-spacing"
                        style="padding: 0 24px;">
                        <table width="100%"
                               border="0"
                               cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td class="bol-heading-2"
                                    style=" font-family: 'Arial Black', 'Open Sans', Arial, 'Roboto', sans-serif;
font-weight: 900;
letter-spacing: -1px;
text-align: left;
font-size: 20px;
line-height: 28px;
color: #0a0a0a;
padding-bottom: 5px;">
                                    Contactloos bezorgen
                                </td>
                            </tr>
                            <tr>
                                <td class="bol-regular"
                                    style=" font-family: Arial, 'sans-serif';
text-align: left;
font-size: 14px;
line-height: 21px;
color: #0a0a0a;">
                                    We vinden het belangrijk dat we al onze klanten kunnen helpen met artikelen waar zij
                                    nu het meest behoefte aan hebben. Uiteraard willen wij ons, ook bij de bezorging van
                                    de pakketten en voor ieders veiligheid, altijd houden aan de voorgeschreven minimale
                                    afstand tussen twee mensen van tenminste anderhalve meter. Fijn als jij ons daarbij
                                    helpt.

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>


    <tr>
        <td bgcolor="#ffffff"
            align="center"
            class="bol-row"
            style="padding: 16px 0;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                <tr>
                    <td align="center" valign="top" width="700">
            <![endif]-->
            <table width="100%"
                   border="0"
                   cellspacing="0"
                   cellpadding="0"
                   align="center"
                   class="bol-wrapper"
                   style="max-width: 700px;">
                <tr>
                    <td width="100%"
                        align="left"
                        valign="top"
                        class="bol-horizontal-spacing"
                        style="padding: 0 24px;">
                        <table width="100%"
                               border="0"
                               cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td class="bol-heading-2"
                                    style=" font-family: 'Arial Black', 'Open Sans', Arial, 'Roboto', sans-serif;
font-weight: 900;
letter-spacing: -1px;
text-align: left;
font-size: 20px;
line-height: 28px;
color: #0a0a0a;
padding-bottom: 5px;">
                                    Dit heb je besteld
                                </td>
                            </tr>
                            <tr>
                                <td class="bol-tiles"
                                    style="font-size:0">

                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="left" border="0" cellspacing="0" cellpadding="0" width="153">
                                        <tr>
                                            <td align="center" valign="middle" width="157" height="120">
                                    <![endif]-->
                                    <a href="https://www.bol.com/nl/p/-/9200000107807216?Referrer=ENTcli_shipment_confirmation_standard2008093611&language=nl"
                                       target="_blank"
                                       class="bol-tile"
                                       style="display: inline-block;
width: 25%;
min-width: 156px;
min-height: 120px;
text-align: center;
background-color: #ffffff;">
                                        <img src="https://media.s-bol.com/KKOWLQW2Lpn/250x200.jpg"
                                             width="125"
                                             height="100"
                                             alt="Happy Socks Special Stripe sokken Giftbox - Maat 41-46"
                                             class="bol-tile-image"
                                             style=" display: inline-block;
width: 125px;
height: 100px;
padding: 10px 14px;
margin: 0 auto;
font-family: sans-serif;
font-size: 12px;
text-align: center;
color: #0000a4;">
                                    </a>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>


    <tr>
        <td bgcolor="#ffffff"
            align="center"
            class="bol-row"
            style="padding: 16px 0;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                <tr>
                    <td align="center" valign="top" width="700">
            <![endif]-->
            <table width="100%"
                   border="0"
                   cellspacing="0"
                   cellpadding="0"
                   align="center"
                   class="bol-wrapper"
                   style="max-width: 700px;">
                <tr>
                    <td width="100%"
                        align="left"
                        valign="top"
                        class="bol-horizontal-spacing"
                        style="padding: 0 24px;">
                        <table width="100%"
                               border="0"
                               cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td class="bol-heading-2"
                                    style=" font-family: 'Arial Black', 'Open Sans', Arial, 'Roboto', sans-serif;
font-weight: 900;
letter-spacing: -1px;
text-align: left;
font-size: 20px;
line-height: 28px;
color: #0a0a0a;
padding-bottom: 5px;">
                                    Details over je bestelling
                                </td>
                            </tr>


                            <tr>
                                <td class="bol-vertical-spacing"
                                    style="padding: 12px 0;">
                                    <table cellpadding="0"
                                           cellspacing="0"
                                           border="0"
                                           width="100%">
                                        <tr>
                                            <td valign="top"
                                                width="168"
                                                class="bol-icon-large"
                                                style="width: 156px;
padding-right: 16px;">
                                                <img
                                                    src="https://s.s-bol.com/nl/upload/images/nb/servicemails/assets/v2/illustrations/track-trace.gif"
                                                    width="152"
                                                    height="152"
                                                    border="0"
                                                    alt="Illustratie track and trace"
                                                    class="bol-icon-large-image"
                                                    style=" width: 152px;
height: 152px;
font-family: sans-serif;
font-size: 12px;
text-align: center;
color: #0000a4;">
                                            </td>
                                            <td valign="middle"
                                                height="152"
                                                class="bol-instruction"
                                                style="height: auto;
min-height: 152px;">
                                                <table cellpadding="0"
                                                       cellspacing="0"
                                                       border="0">
                                                    <tr>
                                                        <td class="bol-heading-3"
                                                            style=" font-family: Arial, 'sans-serif';
font-weight: bold;
letter-spacing: -0.6px;
text-align: left;
font-size: 18px;
line-height: 23px;
color: #0a0a0a;
padding-bottom: 2px;">
                                                            Track &amp; Trace nummer
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bol-regular"
                                                            style=" font-family: Arial, 'sans-serif';
text-align: left;
font-size: 14px;
line-height: 21px;
color: #0a0a0a;">

                                                            <a href="https://postnl.nl/tracktrace/?B=3SNOQJ166513530&amp;P=3191BM&amp;D=NL&amp;T=C&amp;L=NL"
                                                               target="_blank"
                                                               class="bol-link-text"
                                                               style="color:#0000ff;
text-decoration:none;">
                                                                3SNOQJ166513530
                                                            </a>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>


                            <tr>
                                <td class="bol-vertical-spacing"
                                    style="padding: 12px 0;">
                                    <table cellpadding="0"
                                           cellspacing="0"
                                           border="0"
                                           width="100%">
                                        <tr>
                                            <td valign="top"
                                                width="168"
                                                class="bol-icon-large"
                                                style="width: 156px;
padding-right: 16px;">
                                                <img
                                                    src="https://s.s-bol.com/nl/upload/images/nb/servicemails/assets/v2/illustrations/house.gif"
                                                    width="152"
                                                    height="152"
                                                    border="0"
                                                    alt="Illustratie bezorgadres"
                                                    class="bol-icon-large-image"
                                                    style=" width: 152px;
height: 152px;
font-family: sans-serif;
font-size: 12px;
text-align: center;
color: #0000a4;">
                                            </td>
                                            <td valign="middle"
                                                height="152"
                                                class="bol-instruction"
                                                style="height: auto;
min-height: 152px;">
                                                <table cellpadding="0"
                                                       cellspacing="0"
                                                       border="0">
                                                    <tr>
                                                        <td class="bol-heading-3"
                                                            style=" font-family: Arial, 'sans-serif';
font-weight: bold;
letter-spacing: -0.6px;
text-align: left;
font-size: 18px;
line-height: 23px;
color: #0a0a0a;
padding-bottom: 2px;">
                                                            Bezorgadres
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bol-regular"
                                                            style=" font-family: Arial, 'sans-serif';
text-align: left;
font-size: 14px;
line-height: 21px;
color: #0a0a0a;">

                                                            Jordi Holleman<br>

                                                            Jan Thijssenpad 10<br>

                                                            3191 BM HOOGVLIET ROTTERDAM<br>

                                                            Nederland<br>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>


    <tr>
        <td bgcolor="#ffffff"
            align="center"
            class="bol-row"
            style="padding: 16px 0;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                <tr>
                    <td align="center" valign="top" width="700">
            <![endif]-->
            <table width="100%"
                   border="0"
                   cellspacing="0"
                   cellpadding="0"
                   align="center"
                   class="bol-wrapper"
                   style="max-width: 700px;">
                <tr>
                    <td width="100%"
                        align="left"
                        valign="top"
                        class="bol-box-spacing bol-color-02"
                        style="padding: 24px;
background-color: #f0f6ff;">
                        <table width="100%"
                               border="0"
                               cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td class="bol-heading-2"
                                    style=" font-family: 'Arial Black', 'Open Sans', Arial, 'Roboto', sans-serif;
font-weight: 900;
letter-spacing: -1px;
text-align: left;
font-size: 20px;
line-height: 28px;
color: #0a0a0a;
padding-bottom: 5px;">
                                    Als je niet thuis bent...
                                </td>
                            </tr>
                            <tr>
                                <td class="bol-regular"
                                    style=" font-family: Arial, 'sans-serif';
text-align: left;
font-size: 14px;
line-height: 21px;
color: #0a0a0a;">

                                    ... dan proberen wij je pakket af te geven bij de buren of ontvang je het pakket in
                                    de brievenbus. De bezorger laat altijd een bericht achter, zodat je weet wat je kunt
                                    doen.

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>


    <tr>
        <td bgcolor="#ffffff"
            align="center"
            class="bol-row"
            style="padding: 16px 0;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                <tr>
                    <td align="center" valign="top" width="700">
            <![endif]-->
            <table width="100%"
                   border="0"
                   cellspacing="0"
                   cellpadding="0"
                   align="center"
                   class="bol-wrapper"
                   style="max-width: 700px;">
                <tr>
                    <td width="100%"
                        align="left"
                        valign="top"
                        class="bol-horizontal-spacing"
                        style="padding: 0 24px;">
                        <table width="100%"
                               border="0"
                               cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td class="bol-heading-2"
                                    style=" font-family: 'Arial Black', 'Open Sans', Arial, 'Roboto', sans-serif;
font-weight: 900;
letter-spacing: -1px;
text-align: left;
font-size: 20px;
line-height: 28px;
color: #0a0a0a;
padding-bottom: 5px;">
                                    Wanneer kan ik mijn pakket precies verwachten?
                                </td>
                            </tr>
                            <tr>
                                <td class="bol-regular"
                                    style=" font-family: Arial, 'sans-serif';
text-align: left;
font-size: 14px;
line-height: 21px;
color: #0a0a0a;">
                                    Voor dit pakket zijn we niet in staat een precieze bezorgdatum te bepalen. De meest
                                    actuele informatie van je bestelling vind je in je account.
                                </td>
                            </tr>
                            <tr>
                                <td class="bol-regular bol-link-container"
                                    style=" font-family: Arial, 'sans-serif';
text-align: left;
font-size: 14px;
line-height: 21px;
color: #0a0a0a;
padding-top: 16px;">
                                    <a href="https://www.bol.com/nl/account/bestellingen/details.html?placedOrderId=1157434108&Referrer=ENTcli_shipment_confirmation_standard2008093611&language=nl"
                                       target="_blank"
                                       class="bol-link-text"
                                       style="color:#0000ff;
text-decoration:none;">
                                        Naar je bestelling
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>


    <tr>
        <td bgcolor="#ffffff"
            align="center"
            class="bol-row"
            style="padding: 16px 0;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                <tr>
                    <td align="center" valign="top" width="700">
            <![endif]-->
            <table width="100%"
                   border="0"
                   cellspacing="0"
                   cellpadding="0"
                   align="center"
                   class="bol-wrapper"
                   style="max-width: 700px;">
                <tr>
                    <td width="100%"
                        align="left"
                        valign="top"
                        class="bol-horizontal-spacing"
                        style="padding: 0 24px;">
                        <table width="100%"
                               border="0"
                               cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td class="bol-heading-2"
                                    style=" font-family: 'Arial Black', 'Open Sans', Arial, 'Roboto', sans-serif;
font-weight: 900;
letter-spacing: -1px;
text-align: left;
font-size: 20px;
line-height: 28px;
color: #0a0a0a;
padding-bottom: 5px;">
                                    Kan ik het bezorgmoment of het bezorgadres nog wijzigen?
                                </td>
                            </tr>
                            <tr>
                                <td class="bol-regular"
                                    style=" font-family: Arial, 'sans-serif';
text-align: left;
font-size: 14px;
line-height: 21px;
color: #0a0a0a;">

                                    Je kunt de bezorgtijd of het bezorgadres niet veranderen. Lukt het de bezorger niet
                                    om het pakket bij jou af te geven, dan proberen we het bij je buren.
                                    Kijk op de site van de vervoerder voor de exacte bezorgopties. Dit kan gemakkelijk
                                    via de Track &amp; Trace link hierboven.

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>


    <!-- end main content -->
    <tr>
        <td bgcolor="#ffffff"
            align="center"
            class="bol-row"
            style="padding: 16px 0;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                <tr>
                    <td align="center" valign="top" width="700">
            <![endif]-->
            <table width="100%"
                   border="0"
                   cellspacing="0"
                   cellpadding="0"
                   align="center"
                   class="bol-wrapper"
                   style="max-width: 700px;">
                <tr>
                    <td align="center"
                        class="bol-splitter"
                        style=" width: 100%;
font-size: 0;">
                        <!--[if (gte mso 9)|(IE)]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                            <tr>
                                <td valign="top" width="444">
                        <![endif]-->
                        <div class="bol-two-third"
                             style=" display: inline-block;
margin: 0 -2px;
vertical-align: top;
width: 100%;
max-width: 444px;">
                            <table border="0"
                                   cellpadding="0"
                                   cellspacing="0"
                                   width="100%">
                                <tr>
                                    <td>
                                        <table cellpadding="0"
                                               cellspacing="0"
                                               border="0"
                                               width="100%">
                                            <tr>
                                                <td height="60"
                                                    class="bol-heading-3 bol-close-alert-text"
                                                    style=" font-family: Arial, 'sans-serif';
font-weight: bold;
letter-spacing: -0.6px;
text-align: left;
font-size: 18px;
line-height: 23px;
color: #0a0a0a;
padding-bottom: 2px;
height: 60px;
padding-left: 24px">
                                                    Hebben we je met deze mail goed ge&iuml;nformeerd?
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        <td valign="top" width="256">
                        <![endif]-->
                        <div class="bol-one-third"
                             style=" display: inline-block;
margin: 0 -2px;
vertical-align: top;
width: 100%;
max-width: 256px;">
                            <table align="center"
                                   border="0"
                                   cellpadding="0"
                                   cellspacing="0"
                                   width="100%">
                                <tr>
                                    <td>
                                        <table cellpadding="0"
                                               cellspacing="0"
                                               border="0"
                                               width="100%">
                                            <tr>
                                                <td>
                                                    <table border="0"
                                                           cellpadding="0"
                                                           cellspacing="0"
                                                           width="136"
                                                           class="bol-close-alert-icons"
                                                           style="width: 136px;">
                                                        <tr>
                                                            <td width="68"
                                                                height="60"
                                                                align="center"
                                                                valign="middle"
                                                                class="bol-close-alert"
                                                                style="width: 68px;
height: 60px;
align: center;
vertical-align: middle;
padding: 0 4px;">
                                                                <a href="https://klantreactie.bol.com/forms/wAXjW0/positive?verified=true&mailing=CLI_SHIPMENT_CONFIRMATION_052018&id=f8152846-b839-4257-849e-ea24ff9eccb9"
                                                                   target="_blank">
                                                                    <img
                                                                        src="https://s.s-bol.com/nl/upload/images/nb/servicemails/assets/v2/feedback-positive.gif"
                                                                        width="60"
                                                                        height="60"
                                                                        border="0"
                                                                        alt="&#128077;"
                                                                        class="bol-close-alert-image"
                                                                        style="width: 60px;
height: 60px;
display: block;
border: none;
font-family: sans-serif;
font-size: 24px;
text-align: center;
color: #0a0a0a;">
                                                                </a>
                                                            </td>
                                                            <td width="68"
                                                                height="60"
                                                                align="center"
                                                                valign="middle"
                                                                class="bol-close-alert"
                                                                style="width: 68px;
height: 60px;
align: center;
vertical-align: middle;
padding: 0 4px;">
                                                                <a href="https://klantreactie.bol.com/forms/wAXjW0/negative?verified=true&mailing=CLI_SHIPMENT_CONFIRMATION_052018&id=f8152846-b839-4257-849e-ea24ff9eccb9"
                                                                   target="_blank">
                                                                    <img
                                                                        src="https://s.s-bol.com/nl/upload/images/nb/servicemails/assets/v2/feedback-negative.gif"
                                                                        width="60"
                                                                        height="60"
                                                                        border="0"
                                                                        alt="&#128078;"
                                                                        class="bol-close-alert-image"
                                                                        style="width: 60px;
height: 60px;
display: block;
border: none;
font-family: sans-serif;
font-size: 24px;
text-align: center;
color: #0a0a0a;">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff"
            align="center"
            class="bol-footer"
            style="background-color: #ffffff;
padding: 16px 0 32px 0;">
            <table border="0"
                   cellpadding="0"
                   cellspacing="0"
                   width="100%">
                <tr>
                    <td bgcolor="#0000a4"
                        align="center"
                        class="bol-customer-service"
                        style="background-color: #0000a4;
padding: 16px 0;">
                        <!--[if (gte mso 9)|(IE)]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                            <tr>
                                <td align="center" valign="top" width="700">
                        <![endif]-->
                        <table border="0"
                               cellpadding="0"
                               cellspacing="0"
                               width="100%"
                               class="bol-wrapper"
                               style="max-width: 700px;">
                            <tr>
                                <td class="bol-heading-2-light bol-horizontal-spacing"
                                    style="font-family: 'Arial Black', 'Open Sans', Arial, 'Roboto', sans-serif;
font-weight: 900;
letter-spacing: -1px;
text-align: left;
font-size: 20px;
line-height: 28px;
color: #ffffff;
padding: 0 24px;">
                                    Service &amp; contact
                                </td>
                            </tr>
                            <tr>
                                <td align="center"
                                    class="bol-splitter"
                                    style=" width: 100%;
font-size: 0;">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="700">
                                        <tr>
                                            <td valign="top" width="350">
                                    <![endif]-->
                                    <div class="bol-half"
                                         style=" display: inline-block;
margin: 0 -2px;
vertical-align: top;
width: 100%;
max-width: 350px;">
                                        <table border="0"
                                               cellpadding="0"
                                               cellspacing="0"
                                               width="100%">
                                            <tr>
                                                <td class="bol-box-spacing"
                                                    style="padding: 24px;">
                                                    <table cellpadding="0"
                                                           cellspacing="0"
                                                           border="0"
                                                           width="100%">
                                                        <tr>
                                                            <td width="80"
                                                                valign="top"
                                                                class="bol-icon-medium"
                                                                style="width: 64px;
padding-right: 16px;
vertical-align: top;">
                                                                <img
                                                                    src="https://s.s-bol.com/nl/upload/images/nb/servicemails/assets/v2/icon-headset.gif"
                                                                    width="64"
                                                                    height="64"
                                                                    border="0"
                                                                    alt="Illustratie klantencontact"
                                                                    class="bol-icon-medium-image"
                                                                    style=" width: 64px;
height: 64px;
font-family: sans-serif;
font-size: 12px;
text-align: center;
color: #ffffff;">
                                                            </td>
                                                            <td class="bol-footer-text"
                                                                style=" font-family: Arial, 'sans-serif';
text-align: left;
font-size: 14px;
line-height: 21px;
color: #ffffff;">
                                                                <strong>Heb je ons nodig?</strong><br> Onze
                                                                klantenservice is dag en nacht open.

                                                                Je bestelnummer is 1&zwnj;1&zwnj;5&zwnj;7&zwnj;4&zwnj;3&zwnj;4&zwnj;1&zwnj;0&zwnj;8&zwnj;
                                                                <br><br>
                                                                <a href="https://www.bol.com/nl/klantenservice/index.html?language=nl&Referrer=ENTcli_shipment_confirmation_standard2008093611"
                                                                   target="_blank"
                                                                   class="bol-footer-link"
                                                                   style="color: #fff;
text-decoration: underline;">Naar de klantenservicepagina</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    <td valign="top" width="350">
                                    <![endif]-->
                                    <div class="bol-half"
                                         style=" display: inline-block;
margin: 0 -2px;
vertical-align: top;
width: 100%;
max-width: 350px;">
                                        <table border="0"
                                               cellpadding="0"
                                               cellspacing="0"
                                               width="100%">
                                            <tr>
                                                <td class="bol-box-spacing"
                                                    style="padding: 24px;">
                                                    <table cellpadding="0"
                                                           cellspacing="0"
                                                           border="0"
                                                           width="100%">
                                                        <tr>
                                                            <td width="80"
                                                                valign="top"
                                                                class="bol-icon-medium"
                                                                style="width: 64px;
padding-right: 16px;
vertical-align: top;">
                                                                <img
                                                                    src="https://s.s-bol.com/nl/upload/images/nb/servicemails/assets/v2/icon-letter.gif"
                                                                    width="64"
                                                                    height="64"
                                                                    border="0"
                                                                    alt="Illustratie zelf regelen"
                                                                    class="bol-icon-medium-image"
                                                                    style=" width: 64px;
height: 64px;
font-family: sans-serif;
font-size: 12px;
text-align: center;
color: #ffffff;">
                                                            </td>
                                                            <td class="bol-footer-text"
                                                                style=" font-family: Arial, 'sans-serif';
text-align: left;
font-size: 14px;
line-height: 21px;
color: #ffffff;">
                                                                <strong>Snel regelen in je account</strong><br> Volg je
                                                                bestelling, betaal facturen of retourneer een
                                                                artikel.<br><br>
                                                                <a href="https://www.bol.com/nl/rnwy/account/overzicht?language=nl&Referrer=ENTcli_shipment_confirmation_standard2008093611"
                                                                   target="_blank"
                                                                   class="bol-footer-link"
                                                                   style="color: #fff;
text-decoration: underline;">Naar je account</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
