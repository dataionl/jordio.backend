<!DOCTYPE html>
<html>
<head>
    <title>DataIO</title>

    {{--        <link href="{{ mix('css/app.css') }}" rel="stylesheet" media="all" type="text/css">--}}
    <link href="{{ public_path('css/app.css') }}" rel="stylesheet" media="all" type="text/css">
    <script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
</head>
<body class="dataio-order-shipped-body">
<!--[if mso]>
<style type="text/css">
    body, table, td {
        font-family: "Open Sans", serif !important;
    }
</style>
<![endif]-->

<table class="dataio-order-shipped-outer-wrapper" border="0"
       cellspacing="0"
       cellpadding="0"
       align="center"
>
    <tr>
        <td>
            <table class="dataio-header" border="0"
                   cellspacing="0"
                   cellpadding="0">
                <tr>
                    <td>
                        <table class="_wrapper">
                            <tr>
                                <td height="60">
                                    <img class="dataio-logo"
                                         height="60"
                                         src="https://www.dataio.nl/wp-content/uploads/2021/08/DataIO-logo-old.png"
                                         alt="dataio.nl">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="dataio-table-spacer">
            <table class="dataio-main" border="0" cellspacing="0"
                   cellpadding="0"
                   align="center"
            >

                <!-- start main content -->

                <tr class="dataio-top-header">
                    <td>
                        <table class="_intro-header">
                            <tr>
                                <td>Hallo {{$order->shipmentDetails->firstName}},</td>
                            </tr>
                        </table>
                        <table class="_intro-text">
                            <tr>
                                <td>
                                    Bedankt dat je ervoor gekozen hebt om via Bol.com een bestelling te plaatsen bij
                                    DataIO.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td class="dataio-table-spacer">
                        <table class="dataio-invoice-text">
                            <tr>
                                <td class="dataio-table-title">
                                    Je factuur / BTW-bon
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Je BTW-factuur vind je in de bijlage van dit bericht. Let op, je hoeft de factuur
                                    niet uit te betalen aan DataIO. De betaling van jouw bestelling verloopt via Bol.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td class="dataio-table-spacer">
                        <table class="dataio-delivery-info" border="0" cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td class="dataio-table-title">
                                    Wanneer kan ik mijn bestelling precies verwachten?
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Je bestelling met ordernummer {{$order->orderId}} wordt vandaag overgedragen aan
                                    PostNL en komt zo snel mogelijk naar je toe.
                                </td>
                            </tr>
                            <tr>
                                <td class="dataio-delivery-date">
                                    <span class="font-weight-bold">Bezorgdag: </span> {{$deliveryDate}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    We doen er alles aan om je bestelling op tijd te bezorgen. Het is echter in een
                                    enkel geval mogelijk dat door omstandigheden de bezorging vertraagd is.
                                </td>
                            </tr>
                            @if($order->bolShipments->last()->shipment->barcode)
                                <tr>
                                    <td class="dataio-postnl-tracktrace">
                                        <a class="_url" target="_blank"
                                           href="https://postnl.nl/tracktrace/?B={{$order->bolShipments->last()->shipment->barcode}}&P={{$order->shipmentDetails->zipCode}}&D={{$order->shipmentDetails->countryCode}}&T=C&L=NL">
                                            Volg je bestelling via Track & Trace
                                        </a>
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </td>
                </tr>

                <tr>
                    <td class="dataio-table-spacer">
                        <table class="dataio-delivery-not_home" border="0" cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td class="dataio-table-title">Als je niet thuis bent...</td>
                            </tr>
                            <tr>
                                <td>
                                    @if($order->isLetterboxParcel())
                                        ... geen probleem! Het pakketje past door de brievenbus. Indien je brievenbus
                                        vol is dan proberen wij je pakket af te geven bij de buren.
                                    @else
                                        ... dan proberen wij je pakket af te geven bij de buren of ontvang je het pakket
                                        in de brievenbus.
                                    @endif
                                    De bezorger laat altijd een bericht achter, zodat je weet wat je kunt doen.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td class="dataio-table-spacer">
                        <table class="dataio-footer" border="0" cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td class="_footer-content">
                                    Heb je nog vragen of opmerkingen neem dan via je Bol.com account contact met ons op.
                                    Uiteraard kun je ook telefonisch
                                    contact met onze klantenservice opnemen op 088 188 9888 of per e-mail naar <a
                                        href="mailto:bol@dataio.nl" class="_footer-url">bol@dataio.nl</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <!-- end main content -->
            </table>
        </td>
    </tr>
</table>

</body>
</html>
