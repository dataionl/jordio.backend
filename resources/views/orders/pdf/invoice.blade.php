<!DOCTYPE html>
<html>
<head>
    <title>DataIO</title>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet" media="all" type="text/css">
    <link href="{{ public_path('css/app.css') }}" rel="stylesheet" media="all" type="text/css">
    <script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
</head>
<body>
<main class="invoice" style="font-family: 'Open Sans', sans-serif;">
    <div class="container">
        <div class="row top-row">
            <div class="logos">
                <div class="col mr-4">
                    <img class="invoice-logo __dio"
                         src="{{public_path('/assets/DataIO-logo-old.png')}}" alt="DataIO-logo">
                </div>
                <div class="col">
                    <img class="invoice-logo __bol"
                         src="{{public_path('/assets/bol_partner_logo-400x100_grayscale.png')}}" alt="Bol-logo">
                </div>
            </div>
            @include('orders.pdf.partials.company-details')
        </div>

        <div class="invoice-title mb-0">
            <p>Factuur</p>
        </div>

        <div class="addresses">
            <div class="__table-wrapper">
                <table class="invoice-address _address-table">
                    <tr>
                        <td>{{$order->billingDetails->company}}</td>
                    </tr>
                    <tr>
                        <td>{{ $order->billingDetails->firstName }} {{$order->billingDetails->surName}}</td>
                    </tr>
                    <tr>
                        <td>{{$order->billingDetails->streetName}} {{$order->billingDetails->houseNumber}} {{$order->billingDetails->houseNumberExtension}}</td>
                    </tr>
                    <tr>
                        <td>{{$order->billingDetails->zipCode}}  {{$order->billingDetails->city}}</td>
                    </tr>
                </table>
            </div>
        </div>

        @include('orders.pdf.partials.invoice-details')

        @include('orders.pdf.partials.order-details_invoice')

        <div class="bottom-text">
            <div class="_wrapper">
                <p>
                    Heeft u nog vragen of opmerkingen neem dan via uw Bol.com account contact met ons op.
                    Uiteraard kunt u ook telefonisch contact met onze klantenservice opnemen op 088 188 9888 of per
                    e-mail naar bol@dataio.nl.
                </p>
            </div>
        </div>
    </div>
</main>
</body>
</html>
