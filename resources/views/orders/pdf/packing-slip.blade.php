<!DOCTYPE html>
<html>
<head>
    <title>DataIO</title>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet" media="all" type="text/css">
    <link href="{{ public_path('css/app.css') }}" rel="stylesheet" media="all" type="text/css">
    <script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
</head>
<body>
<main class="packing-slip" style="font-family: 'Open Sans', sans-serif;">
    <div class="container">
        <div class="row top-row">
            <div class="logos">
                <div class="col mr-4">
                    <img class="packing-slip-logo __dio"
                         src="{{public_path('/assets/DataIO-logo-old.png')}}" alt="DataIO-logo">
                </div>
                <div class="col">
                    <img class="packing-slip-logo __bol"
                         src="{{public_path('/assets/bol_partner_logo-400x100_grayscale.png')}}" alt="Bol-logo">
                </div>
            </div>
            @include('orders.pdf.partials.company-details')
        </div>

        <div class="packing-slip-title mb-0">
            <p>Factuur</p>
        </div>

        <div class="addresses">
            <div class="__table-wrapper">
                <table class="shipment-address _address-table">
                    <tr class="pb-1">
                        <td class="__title">Verzendadres</td>
                    </tr>
                    <tr>
                        <td>{{$order->shipmentDetails->company}}</td>
                    </tr>
                    <tr>
                        <td>{{ $order->shipmentDetails->firstName }} {{$order->shipmentDetails->surName}}</td>
                    </tr>
                    <tr>
                        <td>{{$order->shipmentDetails->streetName}} {{$order->shipmentDetails->houseNumber}} {{$order->shipmentDetails->houseNumberExtension}}</td>
                    </tr>
                    <tr>
                        <td class="__zipCode">{{$order->shipmentDetails->zipCode}}  {{$order->shipmentDetails->city}}</td>
                    </tr>
                </table>
            </div>
            <div class="__table-wrapper">
                <table class="invoice-address _address-table">
                    <tr class="pb-1">
                        <td class="__title">Factuuradres</td>
                    </tr>
                    <tr>
                        <td>{{$order->billingDetails->company}}</td>
                    </tr>
                    <tr>
                        <td>{{ $order->billingDetails->firstName }} {{$order->billingDetails->surName}}</td>
                    </tr>
                    <tr>
                        <td>{{$order->billingDetails->streetName}} {{$order->billingDetails->houseNumber}} {{$order->billingDetails->houseNumberExtension}}</td>
                    </tr>
                    <tr>
                        <td>{{$order->billingDetails->zipCode}}  {{$order->billingDetails->city}}</td>
                    </tr>
                </table>
            </div>
        </div>

        @include('orders.pdf.partials.invoice-details')

        @include('orders.pdf.partials.order-details_packing-slip')

        <div class="return-terms">
            <div class="_text-wrapper">
                <h5 class="__title">LET OP!</h5>
                <p>
                    Voor datadragers (geheugenkaarten, USB Flash drives, SSD, HDD) gelden afwijkende
                    retourvoorwaarden. Geopende verpakkingen van deze artikelen kunnen wij dan ook helaas NIET
                    retour accepteren. Raadpleeg voor meer informatie onze website.
                </p>
                <p>
                        <span class="_underline">Alle relevante informatie is
                        terug te vinden op de verpakking.</span>
                </p>
            </div>
        </div>

        <div class="bottom-text">
            <table class="__table">
                <tr>
                    <td>
                        <p>
                            <span class="font-weight-bold">Een artikel retour sturen? </span>Dat kan binnen 30 dagen
                            na
                            ontvangst. Het werkt als volgt:
                        </p>
                        <p>
                            * Log in op je account en zoek je bestelling.
                        </p>
                        <p>
                            * Bekijk de details van je bestelling en klik op de link ‘Retour melden’.
                        </p>
                        <p>
                            * Doorloop de retouraanvraag en ontvang je retourinstructies.
                        </p>
                        <p>
                            Indien de verkoper DataIO aangeeft dat het artikel in goede staat is ontvangen, vervalt
                            je
                            betalingsverplichting. Heb je al betaald? Dan betalen wij je terug.
                        </p>

                    </td>
                    <td>
                        <p>
                            <span class="font-weight-bold">Heb je vragen?</span><br/>
                        </p>
                        <p>
                            Neem via je account contact op met de verkoper als je vragen hebt over het ontvangen
                            artikel.
                        </p>
                        <p>
                            Ga voor vragen over je betaling naar bol.com/klantenservice. Daar helpen we je graag
                            verder.
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</main>
</body>
</html>
