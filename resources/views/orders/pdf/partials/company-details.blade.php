<div class="company-details">
    <div class="company-details-address">
        <p class="company-name">{{env('DATAIO_INVOICE_COMPANY_NAME')}}</p>
        <p>{{env('DATAIO_INVOICE_COMPANY_STREETNAME')}} {{env('DATAIO_INVOICE_COMPANY_HOUSENUMBER')}}</p>
        <p>{{env('DATAIO_INVOICE_COMPANY_ZIPCODE')}} {{env('DATAIO_INVOICE_COMPANY_CITY')}}</p>
        <p>{{env('DATAIO_INVOICE_COMPANY_COUNTRY')}}</p>
    </div>
    <div class="company-details-contact">
        <p>{{env('DATAIO_INVOICE_COMPANY_PHONE')}}</p>
        <p class="_url"><a href="https://{{env('DATAIO_INVOICE_COMPANY_WEBSITE')}}">{{env('DATAIO_INVOICE_COMPANY_WEBSITE')}}</a></p>
        <p class="_url"><a href="{{env('DATAIO_INVOICE_COMPANY_EMAIL')}}">{{env('DATAIO_INVOICE_COMPANY_EMAIL')}}</a></p>
    </div>
    <div class="company-details-organization">
        <p>KvK: {{env('DATAIO_INVOICE_COMPANY_KVK')}}</p>
        <p>BTW {{env('DATAIO_INVOICE_COMPANY_BTW_NL')}}</p>
        <p>BTW {{env('DATAIO_INVOICE_COMPANY_BTW_BE')}}</p>
    </div>
</div>
