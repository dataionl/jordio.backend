<div class="invoice-details">
    <table class="invoice-details-table ml-0">
        <tr>
            <td>Ordernummer:</td>
            <td>{{$order->orderId}}</td>
            @if($order->billingDetails->vatNumber)
                <td class="__padding-left">Uw BTW nummer:</td>
                <td>{{$order->billingDetails->vatNumber}}</td>
            @endif
        </tr>
        <tr>
            <td>Factuurnummer:</td>
            <td>{{$order->orderId}}</td>
            @if($order->billingDetails->kvkNumber)
                <td class="__padding-left">Uw kvk nummer:</td>
                <td>{{$order->billingDetails->kvkNumber}}</td>
            @endif
        </tr>
        <tr>
            <td>Factuurdatum:</td>
            <td>{{\Carbon\Carbon::parse($order->orderPlacedDateTime)->translatedFormat('d F Y')}}</td>
            @if($order->billingDetails->orderReference)
                <td class="__padding-left">Referentie:</td>
                <td>{{$order->billingDetails->orderReference}}</td>
            @endif
        </tr>
    </table>
</div>
