<div class="order-details mt-8">
    <div class="__order-items-table-wrapper">
        <table class="order-details-table">
            <tr>
                <th class="orderItem-reference">Artikelnummer</th>
                <th class="orderItem-title">Omschrijving</th>
                <th class="orderItem-quantity">Aantal</th>
                <th class="orderItem-unitPrice text-right">Prijs per stuk</th>
                <th class="orderItem-VAT text-right">BTW</th>
                <th class="orderItem-totalPrice text-right">Prijs incl. BTW</th>
            </tr>
            @foreach ($order->orderItems as $orderItem)
                @if(((object)$orderItem->fulfilment)->method == "FBR")
                    <tr>
                        <td class="{{$orderItem->isMultiPack() ? 'multi-pack' : ''}}">{{((object)$orderItem->offer)->reference}}</td>
                        <td class="_product-title">{{((object)$orderItem->product)->title}}
                            @if($orderItem->hasJewelcase())
                                <span class="_jewelcase">Inclusief gratis jewelcase / opbergdoosje</span>
                            @endif
                        </td>
                        <td class="_quantity @if($orderItem->quantity > 1) more_than_one @endif">{{$orderItem->quantity}}</td>
                        <td class="text-right">&euro; {{number_format($orderItem->unitPrice, 2, ",", ".")}}</td>
                        <td class="text-right">21%</td>
                        <td class="text-right">
                            &euro; {{number_format(($orderItem->unitPrice)*($orderItem->quantity), 2, ",", ".")}}</td>
                    </tr>
                @endif
            @endforeach
        </table>
    </div>
    <div class="__order-price-table-wrapper">
        <table class="vat-calculations-table">
            <tr>
                <td class="_height ">Verzendkosten</td>
                <td class="_height _price">&euro; {{number_format(0, 2, ",", ".")}}</td>
            </tr>
            <tr>
                <td class="_height">Exclusief BTW</td>
                <td class="_height _price">
                    &euro; {{number_format($order->subTotalPrice('FBR'), 2, ",", ".")}}</td>
            </tr>
            <tr>
                <td class="_height _last-before-total">BTW 21%</td>
                <td class="_height _price _last-before-total">
                    &euro; {{number_format($order->totalVAT('FBR'), 2, ",", ".")}}</td>
            </tr>
            <tr class="font-weight-bold">
                <td class="_border-top">Totaal</td>
                <td class="_border-top _price">
                    &euro; {{number_format($order->grandTotalPrice('FBR'), 2, ",", ".")}}</td>
            </tr>
        </table>
    </div>
    <div class="bol-payment-info">
        <p>Alle betalingen verlopen via Bol.com. Informatie hierover vind je in je Bol.com account</p>
    </div>
</div>
