<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| BolRetailerApi Routes
|--------------------------------------------------------------------------
|
| Here is where you can register BolRetailerApi routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your BolRetailerApi!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
