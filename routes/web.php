<?php

use app\Http\BolRetailerApi\Client;
use App\Http\BolRetailerApi\OpenAPI\ModelGenerator;
use app\Http\BolRetailerApi\Order as OrderAPI;
use App\Http\Controllers\BareShippingLabelController;
use App\Http\Controllers\BolClientController;
use App\Http\Controllers\BolShippingMethodController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\OrderBatchController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderFileController;
use App\Http\Controllers\PackingSlipController;
use App\Http\Controllers\ReducedOrderController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\ShipmentController;
use App\Http\Controllers\ShippingMethodBreakpointController;
use App\Http\Controllers\SpreadsheetController;
use App\Http\Controllers\StorageController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\TokenController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test1', [TestController::class, 'test1']);
Route::get('/test2', [TestController::class, 'test2']);
Route::get('/test3', [TestController::class, 'test3']);

Route::get('/orders', [OrderController::class, 'all']);
Route::get('/orders/refresh', [OrderController::class, 'refresh']);
Route::post('/orders/spreadsheet', [SpreadsheetController::class, 'get']);
Route::post('/orders/invoices', [InvoiceController::class, 'get']);
Route::post('/orders/packing-slips', [PackingSlipController::class, 'get']);
Route::get('/orders/ean/{ean}', [OrderController::class, 'productEan']);
Route::get('/orders/cancellations', [OrderController::class, 'getOrderItemsWithCancelRequest']);
Route::get('/orders/cancellations/go', [OrderController::class, 'handleCancellationRequests']);
Route::post('/orders/files', [OrderFileController::class, 'regenerate']);
Route::get('/orders/files/{id}', [OrderFileController::class, 'get']);
Route::get('/orders/{id}', [OrderController::class, 'get']);
Route::delete('/orders/{order}', [OrderController::class, 'delete']);
Route::post('/orders/orderItem/{orderItem}/delete', [OrderController::class, 'deleteOrderItem']);
Route::post('/orders/batch', [OrderBatchController::class, 'newBatch']);
Route::post('/orders/{order}/change-batch', [OrderController::class, 'changeOrderBatch']);
Route::post('/orders/{id}', [OrderController::class, 'updateOrderDetails']);
Route::post('/orders/{id}/shipments/new', [OrderController::class, 'newShipment']);

Route::post('/shipments/orders', [ShipmentController::class, 'shipOrdersByOrderId']);
Route::post('/shipments/batch/{id}', [ShipmentController::class, 'shipOrdersByBatchId']);
Route::post('/shipments/manual', [ShipmentController::class, 'shipOrdersManually']);
Route::post('/shipments/labels/postnl', [ShipmentController::class, 'getPostNLShippingLabels']);
Route::get('/shipments/labels/postnl/return/{id}', [ShipmentController::class, 'createPostNLReturnLabel']);
Route::get('/shipments', [ShipmentController::class, 'all']);

Route::get('/offers', [OfferController::class, 'all']);
Route::get('/offers/refresh', [OfferController::class, 'syncOffers']);
Route::post('/offers/update/fulfilment-deliverycode', [OfferController::class, 'updateOfferFulfilmentDeliveryCodes']);
Route::post('/offers/update/fulfilment-shippingmethod', [OfferController::class, 'updateOfferShippingMethods']);
Route::get('/offers/{id}', [OfferController::class, 'get']);
Route::post('/offers/{id}', [OfferController::class, 'updateOffer']);

Route::get('/order-batches', [OrderBatchController::class, 'index']);
Route::get('/order-batches/all-batch-ids', [OrderBatchController::class, 'getAllIds']);
Route::get('/order-batches/{id}', [OrderBatchController::class, 'get']);
Route::get('/orders/batch/{id}', [OrderBatchController::class, 'getOrdersInBatch']);

Route::post('/search-orders', [SearchController::class, 'search_orders']);

Route::post('/storage', [StorageController::class, 'get']);
Route::get('/storage/exports', [StorageController::class, 'exports']);
Route::get('/storage/shipping_labels', [StorageController::class, 'shipping_labels']);

Route::get('/shipping-methods', [BolShippingMethodController::class, 'all']);
Route::get('/shipping-methods/breakpoints', [ShippingMethodBreakpointController::class, 'all']);
Route::post('/shipping-methods/breakpoints', [ShippingMethodBreakpointController::class, 'new']);
Route::get('/shipping-methods/breakpoints/{id}', [ShippingMethodBreakpointController::class, 'get']);
Route::put('/shipping-methods/breakpoints/{id}', [ShippingMethodBreakpointController::class, 'update']);
Route::delete('/shipping-methods/breakpoints/{id}', [ShippingMethodBreakpointController::class, 'delete']);
Route::get('/shipping-methods/{id}', [BolShippingMethodController::class, 'get']);
Route::post('/shipping-methods/{id}', [BolShippingMethodController::class, 'update']);

Route::get('/invoice/{id}', [InvoiceController::class, 'index']);
Route::get('/packing-slip/{id}', [PackingSlipController::class, 'index']);

Route::post('/bare-shipping-label', [BareShippingLabelController::class, 'create']);

Route::get('/install/client', [BolClientController::class, 'generateClient']);
Route::get('/install/client/original', [BolClientController::class, 'generateOriginalClient']);
Route::get('/install/models', [BolClientController::class, 'generateModels']);
Route::get('/install/models/original', [BolClientController::class, 'generateOriginalModels']);

Route::get('/', function () {
    return view('welcome');
});
